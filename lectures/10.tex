\begin{lem}
  Let $K\in \sset$.
  \begin{enumerate}
    \item The simplicial set $K$ is a colimit of the diagram:
    \[
    F\mc \IDelta/K\to\sset,~\left(\npcat,\Delta^n\to K\right)\mapsto \Delta^n
    \]
    \item The topological space $\abs{K}$ is a colimit of the diagram:
    \[
      \abs{F}\mc \IDelta/K\to\topcat,~\left(\npcat,\Delta^n\to K\right)\mapsto \abs{\Delta^n}
    \]
  \end{enumerate}
\end{lem}

\begin{proof}
\leavevmode
  \begin{enumerate}
    \item By the Yoneda Lemma, we have:
    \[
    \sset(\Delta^n,K)=K_n
    \]
    Under this identification, specifing a cone under $F$ with tip $S\in \sset$ is equivalent to specifying a morphism $K\xrightarrow{g}S$. So a cone under $F$ with tip $K$ corresponding to $\id\mc K\to K$ is a colimit cone, as claimed.
    \item This follows from the explicit construction of the colimit: We can express the colimit of any functor $G\mc I\to \ccat$\itodo{put this somewhere else} as the coequalizer of
    \[
    \begin{tikzcd}
    \amalg_{f\mc i\to j}G(i)
    \ar[shift left = .75ex]{r}[above]{\id}
    \ar[shift right = .75ex]{r}[below]{G(f)}
    &
    \amalg_{i\in I}G(i)
    \end{tikzcd}
    \]
  \end{enumerate}
\end{proof}


\subsection{Kan fibrations}
\begin{defn}
  A simplicial set $K$ is called a \emph{Kan complex} if it satisfies the following \emph{horn filling condition}: For every $n\geq 0$ and $0\leq i\neq n$ every map $f\mc \Lambda_i^n\to K$ can be extended to a diagram of the form
  \[
  \begin{tikzcd}
    \Lambda_i^n
    \ar[hookrightarrow]{d}
    \ar{r}[above]{f}
    &
    K
    \\
    \Delta^n\ar[dashed]{ur}
    &
  \end{tikzcd}
  \]
\end{defn}

\begin{prop}
  Let $X$ be a topological space. Then the simplicial set $\sing(X)$ is a Kan complex.
\end{prop}
\begin{proof}
  For every $n>0$ and $0\leq i\leq n$, the map $j\mc \abs{\Delta_i^n}\to \abs{\Delta^n}$ admits a retraction, i.e. a map $p\mc \abs{\Delta^n}\to\abs{\Delta_i^n}$ such that $p\circ j=\id$:
  This is given by the projection parallel to a vector from the barycenter of the $i$-th face of $\abs{\Delta^n}$ to the $i$-th vertex of $\abs{\Delta^n}$. For example for $\Delta_1^2$ this looks like
  \[
  \begin{tikzcd}
    {}
    &
    1
    \ar{dr}
    &
    {}
    \\
    0
    \ar[dashed]{u}
    \ar{ur}
    \ar{rr}
    &
    {}
    \ar[dashed]{u}
    &
    2
    \ar[dashed]{u}
  \end{tikzcd}
  \]
  \itodo{this pic should be better}
  Using the adjunction \todo{which adjunction}, the extension problem
  \[
  \begin{tikzcd}
    \Lambda_i^n
    \ar{r}
    \ar[hookrightarrow]{d}
    &
    \sing(X)
    \\
    \Delta^n
    \ar[dashed]{ur}
    &
  \end{tikzcd}
  \]
  is equivalent to the extension problem
  \[
  \begin{tikzcd}
  \abs{\Lambda_i^n}
  \ar[shift left= .75ex]{d}[right]{j}
  \ar[leftarrow, shift right = .75ex]{d}[left]{p}
  \ar{r}[above]{f}
  &
  X
  \\
  \abs{\Delta^n}
  \ar[dashed]{ur}[below right]{\tilde{f}}
  &
  \end{tikzcd}
  \]
  which can be solved by setting $\tilde{f}\defined f\circ p$.
\end{proof}
\lec


\subsection{The homotopy category of a simplical set}
\begin{numtext}
We want to understand the above construction in the case of the functor
\[
\chi\mc \IDelta\to \catcat
\]
where we regegard the partially ordered set $\npcat$ as a category with objects $0,\ldots,n$ and a unique morphism $i\to j$ if and only if $i\leq j$. In that way, we obtain a functor
\[
\nerve\defined\nerve_{\chi}\mc\catcat\to\setcat;
\]
the simplicial set $\nerve(\ccat)$ is called the \emph{nerve of $\ccat$}.
We can explicitly describe the simplices of $\nerve(\ccat)$:
  \begin{itemize}
    \item The vertices of $\nerve(\ccat)$ are precisley the objects of $\ccat$;
    \item The edges of $\nerve(\ccat)$ are the morphisms in $\ccat$;
    \item The two-simplices of $\nerve(\ccat)$ are given by commutative diagrams of the form
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      x_1\ar{rd}
      \ar[phantom]{d}{\scriptscriptstyle///}
      &
      \\
      x_0\ar{rr}\ar{ur}
      &
      {}
      &
      x_2
    \end{tikzcd}
    \]
    which is the same as the set of all composable morphisms
    \[
    x_0\to x_1\to x_2
    \]
    in $\ccat$.
    \item For a general $n$, the $n$-simplices are given by composable $n$-chains of morphisms in $\ccat$:
    \[
      0\to 1 \to \ldots \to n
      \]
    For $n = 0$, we interpret this by saying that the identity at $x$ is the zero-chain.
  \end{itemize}
\end{numtext}
\begin{mdefn}
  Let $X$ be a simplicial set, and $f,g\in X_1$ two edges, connecting vertices $x$ and $y$. We say $f$ and $g$ are equivalent if there is a two-simplex $\sigma\in X_2$ such that
  \[
  d_2(\sigma)=f,~d_1(\sigma)=g\text{ and }d_0(\sigma) = \id_y,
  \]
  which we visualize as:
  \[
  \begin{tikzcd}
    &
    Y
    \ar{rd}[above right]{\id_y}
    \ar[phantom]{d}{\sigma}
    &
    \\
    x
    \ar{ur}[above left]{f}
    \ar{rr}[below]{g}
    &
    {}
    &
    y
  \end{tikzcd}
  \]
\end{mdefn}
Being equivalent is in general not an equivalence relation on the sets of edges -- it is however if $X$ is an $\infty$-category, which we will show later.


\begin{construction}
  Let $X$ be a simplicial set. We define a category $\hoh X$ as follows:
  \begin{itemize}
    \item The objects of $\hoh X$ are given by the vertices of $X$, i.e. $\obj(\hoh) = X_0$;
    \item The morphisms are generated by $X_1$, i.e. for every $1$-simplex $f\in X_1$, there is a morphism $d_1(f)\xrightarrow{f} d_0(f)$ in $\hoh X$. We denote the free composite by $f\ast g$;
  \end{itemize}
  subject to the following relations
  \begin{enumerate}[label = (\normalfont \arabic*)]
    \item The $1$-simplex $s_0(x)$ is the identity at $x$;
    \item For every $2$-simplex $\sigma\mc \Delta^2\to X$ with boundary given by a triple $(f,g,h)$ we impose the relation $h=g\ast f$, i.e. we force every diagram
    \[
    \begin{tikzcd}[column sep = small]
      &
      x_1
      \ar{rd}[above right]{g}
      &
      \\
      x_0
      \ar{rr}[below]{h}
      \ar{ur}[above left]{f}
      &
      &
      x_2
    \end{tikzcd}
    \]
    to be commutative in $\hoh X$.
    \item If $f\sim f'$, then $f\ast g\sim f'\ast g$ and $g'\ast f\sim g'\ast f'$ for every composable morphisms $g,g'$.
  \end{enumerate}
  This construction is functorial in $X$ and we obtain a functor \[\hoh\mc \sset\to\catcat,\] which we colloquially refere to as \emph{taking homotopy}, and the categegory $\hoh X$ is called the \emph{homotopy category} of $X$. Note that this construction is well-defined for every simplicial set, since we take free composites of morphsism, so we don't need to think about well-definedness of composition, in contrast to the construction from Sheet 5 -- more on that later!
\end{construction}

\begin{prop}
  The pair of functors
  \[
  \hoh\mc \setcat \leftrightarrows \catcat \mcc \nerve
  \]
  is an adjunction.
\end{prop}
\begin{proof}
  We already know \emph{that there is a right adjoint}, just not how it explicitly looks. There are two ways to go from here -- either show that an explicit construction of $\abs{-}^Q$ actually gives the homotopy category, or use universal properties to show the equivalence from a more handwavy perspective. We shall use the latter approach:\par
   We first observe that we have natural maps of simplicial sets $u_1\mc X\to \nerve(\abs{X}^Q)$ and $u_2\mc X\to \nerve(\hoh X)$. The map $u_1$ is the counit of the adjunction and the map $u_2$ can be constructed as follows: We know that $n$-simplices in $\nerve(\hoh X)$ correspond to composable $n$-chains of morphisms in $\hoh X$. Now for every $n$-simplex $\sigma$ in $S$, we obtain a composable chain of morphisms
   \[
    d_0(\sigma)\to d_1(\sigma)\to \ldots \to d_n(\sigma)
    \]
    in $\nerve(\hoh S)$, were all possible compositions agree precisley because of the equivalence relation imposed on $\hoh X$.
    Note now that the map $u_2$ makes the composition
    \[
    \fun(\ccat,\dcat)\to \sset(\nerve(\ccat),\nerve(\dcat))\to \sset(X,\nerve(D))
    \]
    bijective. But $u_1$ does so too, and this is precisley the Kan extension. So $\hoh X\cong \abs{X}^Q$ follows.
\end{proof}
We now want to comparte the notion of homotopy category with the notion of homotopy category defined on Sheet 5.
\begin{mprop}
  If $X$ is an $\infty$-category, then the relation ``being equivalent'' is in fact an equivalence relation on the set of edges of $X$.
\end{mprop}
\begin{prop}
  The nerve functor $\nerve\ccat\to\sset$ is fully faithfull.
\end{prop}

\begin{proof}
  We first note that the nerve functor is faithful, since a functor $F\mc \ccat\to\dcat$ is determined on its behaviour on objects and vertices, which corresponds to the maps $\nerve(F)_{0,1}\mc \nerve(\ccat)_{0,1}\to\nerve(\dcat)_{0,1}$.\par
  Let now $f\mc \nerve(\ccat)\to \nerve(\dcat)$ be a morphism of simplicial sets. On zero-simplices, we obtain a map $F\mc \obj(\ccat)\to\obj(\dcat)$ which assigns to each object of $\ccat$ an object of $\dcat$ -- this already looks promising. Similarly for $1$-simplices: The map $f$ sends a vertex $u\mc d_0(u)\to d_1(u)$ to a morphism $f(u)\mc d_0(F(u))\to d_1(F(u))$, which is just a morphism in $\dcat$. We need to show now that this is indeed a functor, which is the image of the nerve:
  \begin{itemize}
    \item The compatibility of $F$ with degeneracy maps implies $F(\id_x) = \id_{F(x)}$ for every $x\in \ccat$. Assume now that we are given composable morphisms $x\xrightarrow{u}y\xrightarrow{v}z$ in $\ccat$. Then they can be identified with the 2-simplex $\sigma$ of the form
    \[
    \begin{tikzcd}[column sep = small]
    &
    y\ar{dr}[above right]{F(g)}
    &
    \\
    F(x)
    \ar{rr}[below]{F(g\circ f)}
    \ar{ur}[above left]{F(f)}
    &
    &
    z
    \end{tikzcd}
    \]
    and since $f$ is a map of simplicial sets, $f(\sigma)$ corresponds to the $2$-simplex
    \[
    \begin{tikzcd}[column sep = small]
    &
    y\ar{dr}[above right]{g}
    &
    \\
    x
    \ar{rr}[below]{g\circ f}
    \ar{ur}[above left]{f}
    &
    &
    z
    \end{tikzcd}
    \]
    and thus $F$ is indeed compatible with composition.
    \item That $f$ is indeed the map $\nerve(F)$ follows directly from the fact that every $n$-simplex in $\nerve(\dcat)$ is determined by its $1$-simplices.
  \end{itemize}
\end{proof}
\begin{cor}
  For every category $\ccat$, the counit of the Nerve adjunction induces an isomorphism $\hoh \nerve(\ccat)\xrightarrow{\cong}\ccat$.
\end{cor}
