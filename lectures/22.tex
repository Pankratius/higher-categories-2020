\begin{prop}
  Let $\ccat$ be a Kan-enriched category. Then $\hnerve(\ccat)$ is an $\infty$-category.
\end{prop}

\subsection{$\infty$-groupoids, functors and diagrams}
\begin{numtext}
  Let $\ccat$ be an $\infty$-category. We call the vertices of $\ccat$ the \emph{objects of $\ccat$} and the edges of $\ccat$ the \emph{morphisms} of $\ccat$. A morphism in $\ccat$ is called an \emph{equivalence} if the corresponding morphism in $\hoh \ccat$ is an isomorphism. We call $\ccat$ an \emph{$\infty$-groupoid}, or \emph{anima}, if every morphism in $\ccat$ is an equivalence, i.e. if $\hoh \ccat$ is an equivalence.
\end{numtext}
\begin{example}
  Every Kan complex is an $\infty$-groupoid.
\end{example}
\begin{prop}
  Let $\ccat$ be an $\infty$-category. Then $\ccat$ is an anima if and only if it is a Kan complex.
\end{prop}
\begin{proof}
  Later.
\end{proof}
\begin{construction}
  Let $\ccat$ be an $\infty$-category. We denote by $C^{\simeq}\sse \ccat$ the simplicial subset consisting of all edges which are equivalences. By the above proposition, this in an anima, which is called the \emph{maximal anima in $\ccat$}.
\end{construction}
\begin{defn}
  Let $K$ be a simplicial set and $\ccat$ an $\infty$-category. A \emph{$K$-diagram in $\ccat$} is a morphism of simplicial sets $f\mc K\to \ccat$. In the case that $K=\dcat$ is an $\infty$-category, we call such diagrams \emph{functors}.
\end{defn}
\begin{prop}
  Let $K$ be a simplicial set and $\ccat$ an $\infty$-category. Then the mapping complex
  \[
  \fun(K,\ccat)\defined \maps(K,\ccat)
  \]
  is an $\infty$-category.
\end{prop}
\begin{example}
  Let $\tcat$ be a topological category and $I$ an ordinary category. Let
  \[
  \ccat \defined \nerve_{\topcat}(\tcat)=\hnerve(\sing(\tcat)),
  \]
  which is an $\infty$-category. A \emph{homotopy coherent $I$-diagram in $\tcat$} is a functor
  \[
  \pphi\mc N(I)\to \ccat
  \]
  of $\infty$-categories. To any such diagram, we can associate a functor of ordinary categories:
  \[
  \hoh \pphi\mc \hoh \nerve(I)\to \hoh \ccat
  \]
  We illustrate the difference between $\pphi$ and $\hoh \pphi$ in the concrete example $I = [1]\times [1]$ and $\tcat = \topcat$.
  A homotopy-coherent $I$-diagram in $\topcat$ amounts to giving a diagram of topological spaces which commutes up to \emph{specified} homotopies:
  \[
  \begin{tikzcd}[row sep = huge, column sep = huge]
      X_{00}
      \ar{r}[above]{f}
      \ar{d}[left]{g}
      \arrow[rd, "h"{near start}, ""{name = h, above right}, ""{name = hb, below left}]
      &
      |[alias = f]|
      X_{10}
      \ar{d}[right]{g'}
      \\
      |[alias = g]|
      X_{01}
      \ar{r}[below]{f'}
      &
      X_{11}
      \arrow[Rightarrow, from = h, to = f, "H"]
      \arrow[Rightarrow, from = hb, to = g, "H'"]
  \end{tikzcd}
  \]
  where $H\mc h \simeq g'\circ f$ and $H'\mc h\simeq f'\circ g$. Passing to $\hoh\pphi$ amounts to discarding the specific homotopies $H$ and $H'$ and only remebering that the square commutes up to \emph{some} homotopy.\par
  To illustrate this more concretely, let $(X,\ast)$ be a pointed topological space and let $\Omega X$ be the associated loop space. Then we have a homotopy-coherent square
  \begin{equation}\label{22:square1}\tag{I}
  \begin{tikzcd}[column sep = large, row sep = large]
    \Omega X
    \ar{r}[above]{\ast}
    \ar{d}[left]{\ast}
    \arrow[rd, "\ast", ""{name = H, above right}, ""{name = dot, below left}]
    &
    |[alias = uast]|
    \ast
    \ar{d}[right]{\ast}
    \\
    |[alias = last]|
    \ast
    \ar{r}[below]{\ast}
    &
    X
    \arrow[phantom, from = H, to = uast, "H"]
    \arrow[phantom, from = dot, to = last, "\ast"]
  \end{tikzcd},
  \end{equation}
  where
  \[
  H\mc [0,1]\times \Omega X \to X,~(t,\gamma)\mapsto \gamma(t).
  \]
  As a homotopy-coherent square, this is different from the homotopy coherent square
  \begin{equation}\label{22:square2}\tag{II}
  \begin{tikzcd}[column sep = large, row sep = large]
    \Omega X
    \ar{r}[above]{\ast}
    \ar{d}[left]{\ast}
    \arrow[rd, "\ast", ""{name = H, above right}, ""{name = dot, below left}]
    &
    |[alias = uast]|
    \ast
    \ar{d}[right]{\ast}
    \\
    |[alias = last]|
    \ast
    \ar{r}[below]{\ast}
    &
    X
    \arrow[phantom, from = H, to = uast, "\ast"]
    \arrow[phantom, from = dot, to = last, "\ast"]
  \end{tikzcd},
  \end{equation}
  with constant homotopies. Later, we will see that \eqref{22:square1} is a pullback-square in the $\infty$-category $\nerve_{\topcat}(\topcat)$, while \eqref{22:square2} is not.
\end{example}

\lec
