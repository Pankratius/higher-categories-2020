\section{Simplicial Homotopy Theory}
\subsection{Simplicial Sets}

\begin{defn}
\leavevmode
\begin{enumerate}
\item
 The \emph{simplex category} $\IDelta$ has objects given by the standard linearly ordered sets
    \[
    \npcat \defined \lset 0\leq 1\leq \ldots\leq n\rset
    \]
    for $n\geq 0$ and morphisms given by weakly monotonous maps
    $f\mc \mpcat\to \npcat$, i.e. for $i\leq j$ it holds that $f(i)\leq f(j)$.\par
  A \emph{simplicial set} is a functor $\op{\IDelta}\to \setcat$. We denote the functor category of simplicial sets by
  \[
  \sset\defined \fun(\op{\IDelta},\setcat).
  \]
  \item Let $X\in \sset$ be a simplicial set. An element of $X_n\defined X(\npcat)$ is called an \emph{$n$-simplex of $X$}. For $n=0$ this is also called a \emph{vertex} of $X$, and for $n=1$ a \emph{edge}.
  \item An $n$-simplex $x\in X_n$ is called \emph{degenerate} if there is a $m\leq n$ and a $y\in X_m$ as well as a map $f\mc \npcat\to \mpcat$ such that $X(f)(y) = x$. Otherwise, $x$ is called \emph{non-degenerate}.
  \end{enumerate}
\end{defn}
\begin{numtext}
  Let $n\geq 0$. There are two important sets of order-preserving maps starting or ending in $\npcat$. The first is given by the collection
  \[
  \partial_i\mc\left[n-1\right]\to\npcat,~j\mapsto
  \begin{cases}
  j&j<i\\
  j+1&j\geq i
  \end{cases}
  \]
  for $0\leq i \leq n$;
  the second is given by the collection
  \[
  \sigma_i\mc \npcat\to \left[n-1\right],~j\mapsto
  \begin{cases}
    j&j\leq i\\
    j-i&j>i
  \end{cases}
  \]
  for $0\leq i <n$.
  For $X$ a simplicial set, the induced maps
  \[
  d_i\defined X(\partial_i)\mc X_n\to X_{n-1} \text{ respectivley }s_i\defined X(\sigma_i)\mc X_{n-1}\to X_n
  \]
  are called the \emph{$i$-th face respectivley $i$-th degeneracy map}.\par
  These maps satisfy the following \emph{simplicial identities}:
  \begin{equation}\tag{SI}\label{9:SI}
  \left\{
  \begin{array}{cccc}
    d_id_j &=& d_{j-1}d_i&\text{if }i<j\\
    d_is_j &=& s_{j-1}d_i&\text{if }i<j\\
    d_js_j &=& \id &\\
    d_{j+1}s_j&=&\id&\\
    d_is_j &=& s_jd_{i-1}&\text{if }i>j+1\\
    s_is_j &=& S_{j+1}s_i&\text{if }i\leq j
  \end{array}
  \right.
  \end{equation}
\end{numtext}
\begin{prop}
  The datum of a simplicial set $X\mc\op{\IDelta}\to\setcat$ is equvialent to the following data:
  \begin{itemize}
    \item A collection of sets $\lset X_n\rset_{n\in \nn}$;
    \item Collections of maps
    \[
    d_i\mc X_n\to X_{n-1}\text{ and }s_i\mc X_{n-1}\to X_n
    \]
    for all $n$ and $0\leq i \leq n$ respectivley $0\leq i<n$, satisfying \eqref{9:SI}.
  \end{itemize}
\end{prop}
\begin{mremdef}
  Let $X$ be a simplicial set. A simplicial set $Y$ is called a \emph{simplicial subset of $X$} if there is a monomorphisms of simplicial sets $Y\hookrightarrow X$. In light of the previous proposition, this is equivalent to giving a collection of subsets $(Y_n\sse X_n)_n$ such that $d^i(Y_n)\sse Y_{n-1}$ and $s^i(Y_n)\sse Y_{n+1}$ holds for all $i$ and $n$.
\end{mremdef}
\begin{example}
  Let $n\geq 0$. Then the morphism-functor
  \[
  \Delta^n\mc \op{\IDelta}\to \setcat,~\mpcat\mapsto \IDelta(\mpcat,\npcat)
  \]
  provides a simplicial set which is called the \emph{standard $n$-simplex}. By the Yoneda Lemma, the set of morphisms $\Delta^n\to X$ is in bijective correspondence with the $n$-simplices of $X$.
\end{example}
\begin{rem}
  We want to visualize the low-dimensional simplices of $\Delta^2$:
  \begin{itemize}
    \item The $0$-simplices of $\Delta^2$ are given by order-preserving maps $\left[0\right]\to\left[2\right]$, i.e. by
    \[
    \lset 0\mapsto 0,~0\mapsto 1,~0\mapsto 2\rset
    \]
    We visualize this as three points $0$, $1$, $2$:
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1
      &
      \\
      0
      &
      &
      2
    \end{tikzcd}
    \]
    \item The one-simplices are given by maps $\left[1\right]\to \left[2\right]$. There are six such maps
    \[
    \arraycolsep=1.4pt
    \lset
    \begin{array}{ccc}
    0&\mapsto& 0\\
    1&\mapsto&1
    \end{array},~
    \begin{array}{ccc}
      0&\mapsto&2\\
      1&\mapsto&2
    \end{array},~
    \begin{array}{ccc}
      0&\mapsto&1\\
      1&\mapsto&2
    \end{array},~
    \begin{array}{ccc}
      0&\mapsto 0\\
      1&\mapsto 0
    \end{array},~
    \begin{array}{ccc}
      0&\mapsto&1\\
      1&\mapsto&1
    \end{array},~
    \begin{array}{ccc}
      0&\mapsto&2\\
      1&\mapsto&2
    \end{array}
    \rset
    \]
    The last three are degenerate: If we denote by $c_i\mc\left[1\right]\to\left[2\right]$ the map $0,1\mapsto i$ for $0\leq i\leq 2$, then $c_i$ can be factored as:
    \[
    \left[1\right]\to \left[0\right]\xrightarrow{d_i}\left[2\right]
    \]
     \par
    We visualize the $1$-simpleces of $\Delta^2$ as edges and vertices of a triangle (the names are already suggestive) --  the degenerate $1$-simplices, which correspond exactly to the $0$-simplices are given by the vertices of the triangle, and the non-degenerate $1$-simplices to the edges, connecting the edges $d_i(...)$ for $i=0,1$:
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1\ar{rd}
      &
      \\
      0\ar{rr}\ar{ur}
      &
      &
      2
    \end{tikzcd}
    \]
    \item There is only one non-degenerate 2-simplex, given by
    \[
    \arraycolsep=1.4pt
    \begin{array}{ccc}
      0&\mapsto&0\\
      1&\mapsto&1\\
      2&\mapsto&2
    \end{array},
    \]
    which we visualize as the area of the triangle given by the $0$- and $1$-simplices:
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1\ar{rd}
      \ar[phantom]{d}{\scriptscriptstyle///}
      &
      \\
      0\ar{rr}\ar{ur}
      &
      {}
      &
      2
    \end{tikzcd}
    \]
  \end{itemize}
\end{rem}

\begin{example}
  Let $n\geq 0$ and $\mathcal{J}\sse \powset\left(\lset 0,1\ldots,n\rset\right)$. We define the simplicial set $\Delta^\mathcal{J}$ to have $m$-simplices given by
  \[
  \Delta^{\mathcal{J}}\left(\mpcat\right) \defined
  \lset \mpcat\xrightarrow{f}\npcat\ssp \text{ there is a }J\in\mathcal{J}\text{ such that }\im(f)\sse J\rset,
  \]
  which is a subset of $\Delta^n(\mpcat)$. On morphisms, we choose the restriction of the morphisms of $\Delta^n$, i.e. for a map $\rho\mc\left[m'\right]\to\mpcat$, we define:
  \begin{align*}
  \Delta^{\mathcal{J}}(f)\mc \Delta^{\mathcal{J}}\left(\mpcat\right)&\to \Delta^{\mathcal{J}}\left(\left[m'\right]\right)\\
  \left(\mpcat\xrightarrow{f}\npcat\right)&\mapsto
  \left(\left[m'\right]\xrightarrow{\rho}\mpcat\xrightarrow{f}\npcat\right)
  \end{align*}
  This provides a range of important examples:
  \begin{itemize}
    \item For $\mathcal{J} = \powset\left(\lset 0,1\ldots,n\rset\right)$, we recover the standard $n$-simplex.
    \item For $\mathcal{J} = \powset\left(\lset 0,1\ldots,n\rset\right) \setminus \lset\lset 0,1\ldots,n\rset\rset$, we call $\Delta^\mathcal{J}$ the \emph{boundary} of $\Delta^n$ and denote it by $\partial \Delta^n$. For $n = 2$, it can be visualized as
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1\ar{rd}
      &
      \\
      0\ar{rr}\ar{ur}
      &
      &
      2
    \end{tikzcd}
    \]
    Note that the filling of the $1$-simplices is missing, as this simplex corresponds exactly to the map $\left[2\right]\to\left[2\right]$ that hits all of $\left[2\right]$.
    \item For $0<i<n$ and
    \[\mathcal{J} = \powset\left(\lset 0,\ldots,n\rset\right) \setminus \lset\lset 0,1\ldots,n\rset,\lset 0,\ldots, i-1,i+1,\ldots,n\rset\rset,
    \]
    we call $\Delta^{\mathcal{J}}$ the \emph{$i$-th horn of $\Delta^n$} and denote it by $\Lambda^{n}_i$. For $i=0$ or $i=n$, we disregard the undefined term.
    The three horns of $\Delta^2$ look like
    \[
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1
      \ar[phantom]{d}{\scriptscriptstyle \Lambda_0^2}
      &
      \\
      0\ar{rr}\ar{ur}
      &
      {}
      &
      2
    \end{tikzcd}
    ,~
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1\ar{rd}
      \ar[phantom]{d}{\scriptscriptstyle\Lambda_1^2}
      &
      \\
      0\ar{ur}
      &
      {}
      &
      2
    \end{tikzcd}
    ,~
    \begin{tikzcd}[row sep = small, column sep = small]
      &
      1\ar{rd}
      \ar[phantom]{d}{\scriptscriptstyle \Lambda^2_2}
      &
      \\
      0\ar{rr}
      &
      {}
      &
      2
    \end{tikzcd}
    \]
  \end{itemize}
\end{example}
There is the following characterisation for maps out of the horns and the boundrary of $\Delta^n$.
\begin{mprop}\label{10:gjmapsfrom}
Let $X$ be a simplicial set.
\begin{enumerate}
\item
  The boundrary of $\Delta^n$ is the coequalizer of the following diagram:
  \[
  \begin{tikzcd}[column sep = huge]
  \displaystyle\coprod_{0\leq i < j \leq n}\Delta^{n-2}
  \ar[shift left = .75ex]{rr}[above]{(\Delta^{n-2})_{ij}\xrightarrow{d^{j-1}}(\Delta^{n-1})_{i}}
  \ar[shift right = .75ex]{rr}[below]{(\Delta^{n-2})_{ij}\xrightarrow{d^i}(\Delta^{n-1})_{j}}
  &
  &
  \displaystyle\coprod_{0\leq i \leq n}\Delta^{n-1}
  \end{tikzcd}
  \]
  In particular, there is a bijective correspondence between the set of maps $\partial\Delta^n\to X$ and the set of tuples $(y_0,\ldots,y_n)$ of $(n-1)$-simplices of $X$ such that $d_j(y_k) = d_{k-1}(y_j)$ for $0\leq j <k\leq n$.
  \item
  Analagously, the horn $\horn{k}{n}$ is the coequalizer of the following diagram:
  \[
  \begin{tikzcd}[column sep = huge]
  \displaystyle
  \coprod_{\substack{i,j\in \npcat\setminus\lset k\rset\\i<j}}\Delta^{n-2}
  \ar[shift left = .75ex]{rr}[above]{(\Delta^{n-2})_{ij}\xrightarrow{d^{j-1}}(\Delta^{n-1})_{i}}
  \ar[shift right = .75ex]{rr}[below]{(\Delta^{n-2})_{ij}\xrightarrow{d^i}(\Delta^{n-1})_{j}}
  &
  &
  \displaystyle
  \coprod_{i\in \npcat\setminus \lset k\rset }
  \Delta^{n-1}
  \end{tikzcd}
  \]
  In particular, there is a bijective correspondence between the set of maps $\horn{i}{n}\to X$ with the set of $n$-tuples $(y_0,\ldots,\hat{y}_k,\ldots,y_n)$ of $(n-1)$-simplices $X$ such that $d_i(y_j) = d_{j-1}(y_i)$ for $i,j\in \npcat\setminus\lset k\rset$ with $i<j$ holds.
\end{enumerate}
\end{mprop}
\begin{example}
  A collection $\mathcal{K}\sse \powset\left(\lset 0,1,\ldots,n\rset\right)$ of non-empty subsets satisfying
  \[
  \left(J\in \mathcal{K}\text{ and }\emptyset\neq I\sse J\right)\implies
  I\in \mathcal{K}
  \]
  is called an \emph{abtract simplicial complex}. Using the construction of the above example, every abstract simplicial complex gives rise to a simplicial set $\Delta^{\mathcal{K}}$. Note that not every simplicial set arrises in this way, for example the \emph{simplicial circle}:
  \begin{align*}
    \Delta^1/\partial \Delta^1\mc\op{\IDelta}&\to\setcat\\
    \mpcat&\mapsto \Delta^1(\mpcat)/\partial \Delta^1(\mpcat)
  \end{align*}
\end{example}

Later on, we will be interested in two certain kinds of simplicial sets, which have certain lifting properties against the horn inclusions:
\begin{defn}
\leavevmode
\begin{enumerate}
\item A simplicial set $X$ is called a \emph{quasi-category} or an \emph{$\infty$-category}, if every map from an inner $n$-horn can be lifted to an $n$-simplex, i.e. for every $n\geq 0$ and $0\leq i \leq n$ the lifting problem
    \[
    \begin{tikzcd}
      \Lambda_i^n
      \ar[hookrightarrow]{d}
      \ar{r}[above]{f}
      &
      X
      \\
      \Delta^n\ar[dashed]{ur}[below right]{?}
      &
    \end{tikzcd}
    \]
  admits a (non-necessarily unique) solution.
  \item A simplicial set $X$ is called a \emph{Kan complex} if every map from a $n$-horn can be lifted to an $n$-simplex, i.e. the above lifting problem also admits solutions in the cases $i=0,n$.
  \end{enumerate}
\end{defn}


\begin{numtext}
  Before we can actually produce examples of $\infty$-categories and Kan complexes, we recall some facts about adjunctions and apply them to simplicial sets. We see that by \cref{7:colimitofrepresentables}, every simplicial set can be described as a colimit of representables, i.e. the standard simplices. In this special case, we can formulate it as follows: Let $\Delta/X$ be the overcategory of $X$ induced by the Yoneda embedding, then
  \[
  X\cong \colim_{\npcat\in \Delta/X}\Delta^n.
  \]
  Furthermore, \cref{7:kanadjunction} gives us a way to understand adjunctions involving simplicial sets -- given a functor $F\mc \IDelta\to \ccat$ into a cocomplete category $\ccat$, there is an adjunction
  \[
  \abs{-}^F\mc \sset\leftrightarrows\ccat\mcc\nerve_F
  \]
  where
  \[\nerve_F\mc \ccat\to \sset,~c\mapsto \ccat(F(-),c)
  \]
  is called the \emph{nerve of $F$} and $\abs{-}^F$ is the left Kan extension of $F$ along the Yoneda embedding.
\end{numtext}

\begin{example}
  For $n\geq 0$, we define the \emph{geometric $n$-simplex} by:
  \[
  \abs{\Delta^n}\defined
  \lset
  \left(t_0,t_1,\ldots,t_n\right)\in \rr^{n+1}\ssp
  \sum_{i}t_i = 1, t_i\geq 0
  \rset
  \]
  Given an order-preserving map $f\mc\mpcat\to\npcat$, we define a continous map $f_{\ast}\mc \abs{\Delta^m}\to\abs{\Delta^n}$ via:
  \[
  (t_0,\ldots,t_m)\mapsto (s_0,\ldots,s_n)\text{ where }s_i\defined
  \begin{cases}
      0&\text{if }f^{-1}(i)=\emptyset \\
      \sum_{j\in f^{-1}(i)}t_j&\text{else}
  \end{cases}
  \]
  This construction then defines a functor
  \[
  \rho\mc \IDelta\to \topcat
  \]
  Let $Y$ be a small topological space. We restict the functor $\topcat(-,Y)$ along $\rho$ to obtain a functor
  \[
  \sing(Y)\mc \op{\IDelta}\to \setcat,~\npcat\mapsto\topcat(\abs{\Delta^n},Y),
  \]
  which is called the \emph{singular simplicial set of $Y$}.
  In low dimensions, we have:
  \begin{itemize}
    \item Vertices are given by points of $Y$.
    \item Edges are given by continous paths $\abs{\Delta^1} = [0,1]\to Y$.
    \item Triangles are given by maps from the standard triangle into $Y$.
  \end{itemize}
\end{example}


\subsection{Geometric Realization}
\begin{numtext}
  Let $K\in \sset$ be a simplicial set. We define the \emph{geometric realization of $K$} to be the topological space
  \[
  \abs{K}\defined \left(\coprod_{n\geq 0}K_n\times \abs{\Delta^n}\right)
  \]
  modulo the equivalence relation generated by
  \[
  \left(f^{\ast}(\sigma),y\right)\sim \left(\sigma,f_{\ast}(y)\right),
  \]
  where $(f,\sigma,y)$ runs over all morphisms $f\mc \mpcat\to\npcat$ in $\IDelta$, $\sigma \in K_n$ and $y\in \abs{\Delta^m}$. This construction is functorial in $K$ in that it defines a functor
  \[
  \abs{-}\mc \sset\to \topcat
  \].
\end{numtext}

\subsection{Adjunctions involving simplicial sets}\label{9:adjunctionsubsection}
The following proposition explains how to construct adjunctions involving simplicial sets:
\begin{prop}
  Let $\ccat$ be any cocomplete category and $F\mc \IDelta\to \ccat$ a functor. Then the functor
  \[
  \nerve_F\mc \ccat\to \sset,~c\mapsto\ccat\left(F(-),c\right)
  \]
  admits a left-adjoint, which we denote by $\abs{-}^F$.
\end{prop}
\begin{proof}
  By \todo{how exactly}, a left adjoint is given by left Kan extending $F$ along the Yoneda embedding
  \[
  \begin{tikzcd}
    &
    \sset
    \ar[dashed]{dr}
    &
    \\
    \IDelta
    \ar{ur}[above left]{y}
    \ar{rr}[below]{F}
    &
    {}
    \ar[Rightarrow, shorten = .75ex]{u}
    &
    \ccat
  \end{tikzcd}
  \]
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Denote by $\iota\mc \IDelta\to \catcat$ the functor that sends each totally ordered set $\npcat$ to the corresponding category. Then the left adjoint is denoted by $\tau_1$.
  \end{enumerate}
\end{example}
\lec
