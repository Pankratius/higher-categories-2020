\begin{defn}
  A partially ordered set is called \emph{filtered}of every finite subset has an upper bound.
\end{defn}
\begin{mexample}
  Any totally ordered set is filtered.
\end{mexample}

\begin{defn}
  An object $X\in\ccat$ is called \emph{compact} if, for every diagram
  \[
  I\to\ccat,~i\mapsto Y_i
  \]
  where $I$ is a filtered partially ordered small set with colimit $Y_{\infty}$ and colimit cone
  \[
  \lset \eta_i\mc Y_i\to Y_{\infty}\rset_i
  \]
  and for every morphism $f\mc X\to Y_{\infty}$ the following conditions are satisfied:
  \begin{enumerate}
    \item There exists $i\in I$ and $f_i\mc X\to Y_i$ such that
    \[
    \begin{tikzcd}
      X
      \ar{r}[above]{f_i}
      \ar{dr}[below left]{f}
      &
      Y_i
      \ar{d}[right]{\eta_i}
      \\
      &
      Y_{\infty}
    \end{tikzcd}
    \]
    commutes.
    \item Given $i,j\in I$ and $f_i\mc X\to Y_i$, $f_j\mc X\to Y_j$ as above (i.e. $f = \eta_i \circ f_i = \eta_j\circ f_j$ holds)
    then there exists $i,j\leq k$ such that
    \[
    \begin{tikzcd}
    X
    \ar{r}[above]{f_i}
    \ar{dr}[above right]{f_k}
    \ar{d}[left]{f_j}
    &
    Y_i
    \ar{d}
    \\
    Y_j
    \ar{r}
    &
    Y_{\infty}
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
\end{defn}

\begin{rem}
  An object $X\in \ccat$ is compact if and only if the functor $\ccat(X,-)\mc \ccat\to \setcat$ commutes with filtered colimits.
\end{rem}

\begin{example}
  Let $X$ be a simplicial set. We claim that $X$ is compact if and only if it is compact.\par
  Suppose $X$ is compact. Consider the diagram
  \[
  \gamma\mc \mathcal{P}^{\mathrm{Fin}}(X)\to\setcat,~Y\sse X\mapsto X,
  \]
  then $\mathcal{P}^{\mathrm{Fin}}(X)$ is filtered and $X\cong \colim \gamma$. Since $X$ is compact, there exists a factorisation of the identity over a finite subset $Y$:
  \[
  \begin{tikzcd}
    X
    \ar{r}
    \ar[equal]{rd}
    &
    Y
    \ar[hookrightarrow]{d}
    \\
    &
    X
  \end{tikzcd}
  \]
  and thus $X=Y$ which is finite.\par
  Assume now that $X$ is finite. Given a diagram
  \[
  \gamma\mc I\to \setcat,~i\mapsto Y_i
  \]
  with $I$ filtered, a colimit is given by
  \[
  Y_{\infty}\defined \left(\coprod_{i\in I}Y_i\right)/\sim
  \]
  where $x\in Y_i$ is equivalent to $y\in Y_j$ if theres a $i,j\leq k$ such that $x$ and $y$ agree in $Y_k$. Suppose further that we are given a map $f\mc X\to Y_{\infty}$ then for every $x\in X$ there exists and $i_x\in I$ such that $f(x)$ lies in the image of $Y_{i_x}\to Y_{\infty}$. Let $i$ be an upper bound of the finite set $\lset i_x\rset_{x\in X}$. Then we obtain a factorisation of the form
  \[
  \begin{tikzcd}
    X
    \ar{r}
    \ar{rd}[below left]{f}
    &
    Y_i
    \ar{d}
    \\
    &
    Y_{\infty}
  \end{tikzcd}
  \]
  which establishes property i).
  Now suppose we are given two such factorisations
  \[
  \begin{tikzcd}
    X
    \ar{r}[above]{f_i}
    \ar{rd}[below left]{f}
    &
    Y_i
    \ar{d}[right]{\eta_i}
    \\
    &
    Y_{\infty}
  \end{tikzcd}
  \text{ and }
  \begin{tikzcd}
    X
    \ar{r}[above]{f_j}
    \ar{rd}[below left]{f}
    &
    Y_i
    \ar{d}[right]{\eta_j}
    \\
    &
    Y_{\infty}
  \end{tikzcd}
  \]
  Then, again by the explicit description of the colimit, for every $x\in X$ there exists $i,j\leq k_x$ such that $f_i(x)$ and $f_j(x)$ coincide in $Y_{k_x}$. Let $k$ be a an upper bound of the finite set $\lset k_x\rset_{x\in X}$ then we get the diagram
  \[
  \begin{tikzcd}
    X
    \ar{r}[above]{f_i}
    \ar{d}[left]{f_j}
    &
    Y_i
    \ar{d}
    \\
    Y_j
    \ar{r}
    &
    Y_k
  \end{tikzcd}
  \]
  commutes for all $i,j$.
\end{example}


\begin{lem}[Small Object Argument]
  Let $\ccat$ be a category with small colimits. Suppose that $M$ is a small set of morphisms such that, for every $A\xrightarrow{i}B$ in $M$, the object $A$ is compact. Then every morphism $f\mc X\to Y$ in $\ccat$ admits a factorisation
  \[
  \begin{tikzcd}
    X
    \ar{rr}[above]{f}
    \ar{rd}[below left]{h}
    &
    &
    Y
    \\
    &
    Z
    \ar{ur}[below right]{g}
    &
  \end{tikzcd}
  \]
  such that $h\in \overline{M}$ and $g\in M\rlp$.
\end{lem}
\begin{cor}
  In the circumstances of the Small Object Argument, we have
  \[
  \overline{M} = \llp (M\rlp).
  \]
\end{cor}
\begin{proof}
  Let $f\in \llp (M\rlp)$ and consider the factorisation from the small object argument
  \[
  f = g\circ h,~\text{with }g\in M\rlp\text{ and }h\in \overline{M}
  \]
  Then the factorisation determines a lifting problem
  \[
  \begin{tikzcd}
  X
  \ar{r}[above]{h}
  \ar{d}[left]{f}
  &
  Z
  \ar{d}[right]{g}
  \\
  Y
  \ar[equal]{r}
  &
  Y
  \end{tikzcd},
  \]
  which has a solution $r$ since $g\in M\rlp$ and $f\in \llp (M\rlp)$. Then the diagram
  \[
  \begin{tikzcd}
    X
    \ar[equal]{r}
    \ar{d}[left]{f}
    &
    X
    \ar{d}[right]{h}
    \ar[equal]{r}
    &
    X
    \ar{d}[right]{f}
    \\
    Y
    \ar{r}[below]{r}
    &
    Z
    \ar{r}[below]{g}
    &
    Y
  \end{tikzcd}
  \]
  exhibits $f$ as a retract of $h$, which is an element of the saturated set $\overline{M}$. So $f$ itself is a member of $\overline{M}$.
\end{proof}

\begin{proof}[Proof of the Small Object Argument]
Let $f\mc X\to Y$ be any morphism in $\ccat$. Consider the small set of all commutative diagrams of the form
\[
\lset
\begin{tikzcd}
  A_j
  \ar{r}
  \ar{d}[left]{i_j}
  &
  X
  \ar{d}[right]{f}
  \\
  B_j
  \ar{r}
  &
  Y
\end{tikzcd}
\rset_{j\in J};
  \]
  with $i_j\in M$. We form the ``big'' commutative square
  \[
  \begin{tikzcd}
    \coprod_{j\in J} A_j
    \ar{r}
    \ar{d}[left]{\coprod i_j}
    &
    X
    \ar{d}
    \\
    \coprod{j\in J}B_j
    \ar{r}
    &
    Y
  \end{tikzcd}
  \]
  and further augment this to the pushout:
  \[
  \begin{tikzcd}
    \coprod_{j\in J} A_j
    \ar{r}
    \ar{d}
    \ar[phantom]{dr}{\lrcorner}
    &
    X
    \ar{d}[right]{h_1}
    \ar[bend left = 20]{rdd}[right]{f}
    &
    \\
    \coprod_{j\in J}B_j
    \ar{r}
    \ar[bend right = 20]{rrd}
    &
    X_1
    \ar[dashed]{rd}[right]{g_1}
    &
    \\
    &
    &
    Y
  \end{tikzcd}
  \]
  Note that the morphism $h_1$ is contained in $\overline{M}$ and we already arrive at the start of our factorisation $f = g_1\circ h_1$. \par
  We now repeat this construction with $f$ replaced by $g$, i.e. we consider the small set of commutative diagrams of the form
  \[
  \lset
  \begin{tikzcd}
    A_j
    \ar{r}
    \ar{d}[left]{i_j}
    &
    X_1
    \ar{d}[right]{f}
    \\
    B_j
    \ar{r}
    &
    Y
  \end{tikzcd}
  \rset_{j\in J_1};
    \]
    with $i_j\in M$ and form the pushout
    \[
    \begin{tikzcd}
      \coprod_{j\in J} A_j
      \ar{r}
      \ar{d}
      \ar[phantom]{dr}{\lrcorner}
      &
      X
      \ar{d}[right]{h_2}
      \ar[bend left = 20]{rdd}[right]{g_1}
      &
      \\
      \coprod_{j\in J}B_j
      \ar{r}
      \ar[bend right = 20]{rrd}
      &
      X_2
      \ar[dashed]{rd}[right]{g_2}
      &
      \\
      &
      &
      Y
    \end{tikzcd};
    \]
  where we again have $h_2\in \overline{M}$. Iterating this, we obtain a sequence
  \[
  \begin{tikzcd}[column sep = small]
    X
    \ar{rr}[above]{h_1}
    \ar{rrrd}[below left]{f}
    &
    &
    X_1
    \ar{rr}[above]{h_2}
    \ar{rd}[above right]{g_1}
    &
    &
    X_2
    \ar{rr}
    \ar{ld}[above left]{g_2}
    &
    &
    \ldots
    \\
    &
    &
    &
    Y
    &
    &
    &
  \end{tikzcd}
  \]
  of which we form the colimit
  \[
  \begin{tikzcd}[column sep = small]
    X
    \ar{rr}[above]{h_1}
    \ar{rrrd}[below left]{f}
    &
    &
    X_1
    \ar{rr}[above]{h_2}
    \ar{rd}[above right]{g_1}
    &
    &
    X_2
    \ar{rr}
    \ar{ld}[above left]{g_2}
    &
    &
    \ldots
    \ar{rr}
    &
    &
    X_{\infty}
    \ar[dashed]{dlllll}[below]{g}
    \\
    &
    &
    &
    Y
    &
    &
    &
    &
    &
    &
  \end{tikzcd}
  \]
  Set now $h$ to be the canonical map $X\to X_{\infty}$, then in particular $h\in \in \overline{M}$. Finally, we claim $g\in M\rlp$:
  Given a lifting problem
  \[
  \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i}
    &
    X_{\infty}
    \ar{d}[right]{g}
    \\
    B
    \ar[dashed]{ur}
    \ar{r}
    &
    Y
  \end{tikzcd}
  \]
  with $i\in M$, the compactness of $A$ implies that there is a commutative factorisation of the form:
  \[
  \begin{tikzcd}[row sep = small, column sep = small]
    A
    \ar{rr}
    \ar{rd}
    \ar{dd}[left]{i_j}
    &
    &
    X_{\infty}
    \ar{dd}
    \\
    &
    X_k
    \ar{ur}
    \ar{dr}
    &
    \\
    B
    \ar{rr}
    &
    &
    Y
  \end{tikzcd}
  \]
  The left-most square
  \[
  \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i_j}
    &
    X_k
    \ar{d}
    \\
    B
    \ar{r}
    &
    Y
  \end{tikzcd}
  \]
  is one of the squares parametrized by $J_k$, and from the definition of $X_{k+1}$ we have maps $X_k\xrightarrow{h_{k+1}}X_{k+1}$ and $B\to X_{k+1}$ that lead to the big commutative refinement
  \[
  \begin{tikzcd}[column sep = small]
    A
    \ar{rr}
    \ar{rd}
    \ar{ddd}[left]{i_j}
    &
    &
    X_{\infty}
    \ar{ddd}
    \\
    &
    X_k
    \ar{ur}
    \ar{d}[left]{h_{k+1}}
    &
    \\
    &
    X_{k+1}
    \ar{dr}
    \ar{uur}
    &
    \\
    B
    \ar{ur}
    \ar{rr}
    &
    &
    Y
  \end{tikzcd}
  \]
  Now a solution to the lifting problem is given by the composition
  \[
  B\to X_{k+1}\to X_{\infty}
  \]
\end{proof}
\lec
