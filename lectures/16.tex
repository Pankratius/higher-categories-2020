\section{Model Categories}
\subsection{Localization of categories}
\begin{defn}
Let $\ccat$ be a category and let $\weake$ be a set of morphisms in $\ccat$. A \emph{Localization of $\ccat$ along $\weake$} is category $\ccat[\weake^{-1}]$ equipped with a functor
\[
\pi\mc \ccat\to \ccat[\weake^{-1}]
\]
satisfying the following universal property: For every category $\dcat$, the functor
\[
\pi^{\ast}\mc \fun(\ccat[\weake^{-1}],\dcat)\to \fun(\ccat,\dcat)
\]
is fully-faithfu with essential image given by those functors $F\mc \ccat\to\dcat$ that send all morphisms in $\weake$ to isomorphisms in $\dcat$.
\end{defn}
\begin{example}
  Denote by $B(\nn)$ the category associated to the monoid $(\nn,+)$. Then the groupoid $B\zz$ is the Localization of $B\nn$ along $\weake=\lset 1\rset$.
\end{example}

\begin{prop}
  Let $\ccat$ be a category and $\weake$ a set of morphisms in $\ccat$. Then a localization of $\ccat$ along $\weake$ exists.
\end{prop}

\begin{proof}[Sketch of a Proof]
We construct $\ccat[\weake^{-1}]$ as a colimit in $\catcat_{\kappa}$, the category of $\kappa$-small categories where $\kappa$ is chosen such that $\ccat$ is $\kappa$-small. \par
Consider now the category
\[
[1] \defined \begin{tikzcd}
  0
  \ar{r}[above]{f}
  &
  1
\end{tikzcd}
\]
and
\[
\overline{[1]}\defined
\begin{tikzcd}
0
\ar[bend left = 20]{r}[above]{f}
&
1
\ar[bend left = 20]{l}[below]{g}
\end{tikzcd}
\]
where $g\circ f = \id_0$ and $f\circ g = \id_1$ holds. Then the inclusion $[1]\to\overline{[1]}$ is a localization along $\lset f\rset$. We now form the pushout
\[
\begin{tikzcd}
  \displaystyle \coprod_{a\in \weake}[1]
  \ar{r}
  \ar{d}
  \ar[phantom]{rd}{\ulcorner}
  &
  \ccat
  \ar{d}[right]{\pi}
  \\
  \displaystyle \coprod_{a\in \weake}\overline{[1]}
  \ar{r}
  &
  \ccat
\end{tikzcd}
\]
Then from the universal property of the pushout, we immediately get that for any category $\dcat$, functors $\overline{\ccat}\to \dcat$ are in bijection with functors from $\ccat$ to $\dcat$ that invert all elements in $\weake$.
\end{proof}
\begin{numtext}
  Based on the concept of localization, we can now state a result which provides a precise relation between $\sset$ and $\topcat$: We call a morphisms $f\mc X\to Y$ of topological spaces a \emph{weak homotopy equivalence} if, for every $n\geq 0$ and every $x\in X$, the induced map
  \[
  f_{\ast}\mc \pi_n(X,x)\to\pi_n(Y,f(x))
  \]
  is an isomorphism; this set is denoted by $\weake$.\par
  Consequently, a map $g\mc K\to S$ of simplicial sets is called a \emph{weak homotopy equivalence} if the induced map on geometric realizations
  \[
  \left|f\right|\mc \left|K\right|\to\left|S\right|
  \]
  is a weak homotopy equivalence of topological spaces. We also refer to this set as $\weake$. By construction we obtain a functor $\left|-\right|$ such that the diagram
  \[
  \begin{tikzcd}
    \sset
    \ar{d}
    \ar{r}[above]{\left|-\right|}
    &
    \topcat
    \ar{d}
    \\
    \sset[\weake^{-1}]
    \ar[dashed]{r}[below]{\left|-\right|}
    &
    \topcat[\weake^{-1}]
  \end{tikzcd}
  \]
  In this chapter, we will develop language to prove:
  \begin{theorem}
    The functor
    \[
    \left|-\right|\mc  \sset[\weake^{-1}]\to
    \topcat[\weake^{-1}]
    \]
    is an equivalence of categories.
  \end{theorem}
\end{numtext}

\subsection{Model category axioms}

\begin{defn}
  A \emph{model category} is a category $\ccat$ equipped with three distinguished sets of moprhisms:
  \begin{itemize}
    \item \emph{weak equivalences} $\weake$;
    \item \emph{cofibrations} $\cofib$;
    \item \emph{fibrations} $\fib$;
  \end{itemize}
  satisfying the following axioms:
  \begin{enumerate}
    \item[(M1)] The category $\ccat$ has all small limits and colimts.
    \item[(M2)] $\weake$ satisfies the two-out-three property: $f\mc X\to Y$ and $g\mc Y\to Z$ in $\ccat$ then if any two of the morphisms $f$, $g$ or $g\circ f$ are in $\weake$ then so is the third.
    \item[(M3)] All three sets $\fib$, $\cofib$ and $\weake$ are stable under retracts.
    \item[(M4)] The lifting problem
    \[
    \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i}
    &
    X
    \ar{d}[right]{p}
    \\
    B
    \ar[dashed]{ur}
    \ar{r}
    &
    Y
    \end{tikzcd}
    \]
    has a solution, provided that either
    \[
    i\in \cofib\cap \weake\text{ and }p\in \fib
    \]
    or
    \[
    i \in \cofib \text{ and }p\in \fib\cap\weake.
    \]
    A morphism in $\cofib\cap \weake$ is called a \emph{trivial cofibration} and a morphism in $\fib\cap \weake$ is called a \emph{trivial fibration}.
    \item[(M5)] Any map $X\to Z$ in $\ccat$ admits factorization
    \[
    X\xrightarrow{f}Y\xrightarrow{g}Z\text{ and }X\xrightarrow{f'}Y\xrightarrow{g'}Z
    \]
    with $f\in \cofib\cap\weake$, $g\in \fib$, $f'\in \cofib$ and $g'\in \fib\cap\weake$.
  \end{enumerate}
  We say that the tuple $(\weake,\cofib,\fib)$ forms a \emph{model structure on $\ccat$}.
\end{defn}
\begin{example}
  Any complete and cocomplete category admits a model structure, where the weak equivalences are given by the set of isomorphisms in $\ccat$, and all morphisms are bot fibrations and cofibrations.
\end{example}
\begin{lem}
  Let $\ccat$ be a model cateogy. Then we have
  \begin{enumerate}
    \item $\cofib\rlp = \fib\cap\weake$;
    \item $\cofib = \llp(\fib\cap\weake)$;
    \item $(\cofib\cap\weake)\rlp = \fib$;
    \item $\cofib\cap\weake = \llp \fib$.
  \end{enumerate}
\end{lem}
\begin{cor}
  In any model category, the sets of cofibrations and trivial cofibrations are saturated.
\end{cor}
\begin{example}
  These are further examples for model categories -- proofs can be found in \cite{hovey}.
  \begin{enumerate}
    \item The category $\topcat$ of topological spaces has a model structure, called the \emph{Quillen model structure}, with
    \begin{itemize}
      \item weak equivalences given by the weak homotopy equivalences;
      \item fibrations given by Serre fibrations;
      \item cofibrations given by the set $\llp)\fib\cap\weake)$.
    \end{itemize}
    \item The category $\sset$ of simplicial sets has a model structure, called the \emph{Kan model structure}, with
    \begin{itemize}
      \item weak equivalences given by the weak homotopy equivalences;
      \item fibrations given by the Kan fibrations;
      \item cofibrations given by the monomorphisms.
    \end{itemize}
    \item Let $R$ be a small ring. Then the category of chain complexes over $R$ of left $R$-modules has a model structure, the \emph{projective model structure}, with
    \begin{itemize}
      \item weak equivalences given by the quasi-isomorphisms;
      \item fibrations given by degree-wise surjective maps;
      \item cofibrations given by $\llp(\fib\cap\weake)$.
    \end{itemize}
  \end{enumerate}
\end{example}
\lec
