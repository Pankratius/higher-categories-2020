\begin{proof}
\leavevmode
\begin{enumerate}
  \item We first spell out the assumption: For every $j\in J$, the pair
  \[
  (G(j),~\xi^{(j)}\mc \Delta(G(j))\nta F|(j/\pphi))
  \]
  is a limit cone over $F|(j/\pphi)$.
  So in particular, for every other extension $(G',\eta')$ of $F$ along $\pphi$ and every $j\in J$ there is a unique morphism $\gamma_j\mc G'(j)\to G(j)$ such that the diagram
  \begin{equation}\label{7:conediagram}
    \begin{tikzcd}
      \Delta(G'(j))\ar[Rightarrow]{rd}[above right]{\xi'^{(j)}}\ar[Rightarrow, dashed]{d}[left]{\exists !\Delta(\gamma_j)}&\\
      \Delta(G(j))\ar[Rightarrow]{r}[below]{\xi^{(j)}}&F|(j'/\pphi)
    \end{tikzcd}
  \end{equation}
  commutes.
  We claim that the collection $\gamma\defined \lset \gamma_j\rset_{j\in J}$ defines a natural transformation $G'\nta G$ -- so we have to verify that for every morphism $g\mc j\to j'$ in $J$ the diagram
  \begin{equation}\label{7:natdiagram}
  \begin{tikzcd}
    G'(j)\ar{r}[above]{G'(g)}\ar{d}[left]{\gamma_j}
    &
    G'(j')\ar{d}[right]{\gamma_{j'}}\\
    G(j)
    \ar{r}[below]{G(g)}&
    G(j')
  \end{tikzcd}
  \end{equation}
  commutes.\par
  For that, we first apply $\Delta$ to \eqref{7:natdiagram} to obtain
  \[
  \begin{tikzcd}[column sep = huge, row sep = large]
  \Delta(G'(j))
  \ar[Rightarrow]{r}[above]{\Delta(G'(g))}
  \ar[Rightarrow]{d}[left]{\Delta(\gamma_j)}
  &
  G'(j')
  \ar[Rightarrow]{d}[right]{\Delta(\gamma_{j'})}\\
  G(j)
  \ar[Rightarrow]{r}[below]{G(g)}&
  G(j')
  \end{tikzcd},
  \]
  which we extented to the right by \eqref{7:conediagram} to obtain :
  \begin{equation}\label{7:bigone}
  \begin{tikzcd}[column sep = huge, row sep = large]
  \Delta(G'(j))
  \ar[Rightarrow]{r}[above]{\Delta(G'(g))}
  \ar[Rightarrow]{d}[left]{\Delta(\gamma_j)}
  \mcom{dr}
  &
  \Delta(G'(j'))
  \ar[Rightarrow]{d}[right]{\Delta(\gamma_{j'})}
  \ar[Rightarrow, bend left = 25]{rd}[above right, pos = 0.3]{\xi'}
  \com{dr}{1}
  &
  \\
  \Delta(G(j))
  \ar[Rightarrow]{r}[below]{\Delta(G(g))}
  &
  \Delta(G(j'))
  \ar[Rightarrow]{r}[below]{\xi}
  &
  F|(j'/\pphi)
  \end{tikzcd}
  \end{equation}
Now the natural transformation $\xi\mc \Delta(G(j'))\nta F|(j/\pphi)$ is a limit cone over $F|(J/\pphi)$, so there is a \emph{unique} natural transformation $\Delta(G'(j))\nta \Delta(G(j'))$ such that the diagram
\begin{equation}\label{7:uselimitcone}
\begin{tikzcd}[column sep = huge, row sep = large]
\Delta(G'(j))
\ar[Rightarrow, bend left = 35]{rrd}[above, pos = .3]{\xi'\circ\Delta(G'(g))}
\ar[Rightarrow]{rd}[below left]{\exists !}
&
\Delta(G'(j'))
%\ar[Rightarrow]{d}[right]{\Delta(\gamma_{j'})}
%\ar[Rightarrow, bend left = 25]{rd}[above right, pos = 0.3]{\xi'}
%\com{dr}
&
\\
\Delta(G(j))
%\ar[Rightarrow]{r}[below]{\Delta(G(g))}
&
\Delta(G(j'))
\ar[Rightarrow]{r}[below]{\xi}
&
F|(j'/\pphi)
\end{tikzcd}
\end{equation}
commutes.
It surely commutes for the \qq{upper-horizontal-then-down}-choice of the natural transfromation, i.e.
\begin{align*}
  \xi \circ \left(\Delta(\gamma_{j'}) \circ \Delta(G(g))\right)&=
  \left(\xi \circ \Delta(\gamma_{j'})\right)\circ \Delta(G'(g))\\
  &= \xi'\circ \Delta(G'(g)).
\end{align*}
where the second equality holds by the construction of $\Delta(\gamma_{j'})$ (the triangle $///^1$ in \eqref{7:bigone} commutes, since $\xi$ is a limit cone). So it suffices that in the diagram \eqref{7:uselimitcone} the \qq{down-then-lower-horizontal} choice of natural transformations also leads to commutativity, i.e. that the diagram
\begin{equation}\label{7:conetriangle}
\begin{tikzcd}[column sep = huge, row sep = large]
\Delta(G'(j))
\ar[Rightarrow, bend left = 35]{rrd}[above, pos = .3]{\xi'\circ\Delta(G'(g))}
\ar[Rightarrow]{d}[left]{\Delta(\gamma_j)}
&
\Delta(G'(j'))
%\ar[Rightarrow]{d}[right]{\Delta(\gamma_{j'})}
%\ar[Rightarrow, bend left = 25]{rd}[above right, pos = 0.3]{\xi'}
%\com{dr}
&
\\
\Delta(G(j))
\ar[Rightarrow]{r}[below]{\Delta(G(g))}
&
\Delta(G(j'))
\ar[Rightarrow]{r}[below]{\xi}
&
F|(j'/\pphi)
\end{tikzcd}
\end{equation}
commutes. For that, we consider the functor $g^{\pre}:$
\begin{align*}
  j'/\pphi&
  \to j/\pphi\\
  \left(i,~j'\mapsto \pphi(i)\right)&
  \mapsto
  \left(i,~j\xrightarrow{g}j'\to\pphi(i)\right)
\end{align*}
Now $g^\pre$ induces a functor $g^{\ast}\mc \ccat^{j/\pphi}\to \ccat^{j'/\pphi}$, which is given on objects by
\[
\left(j/\pphi\to \ccat\right)\mapsto
\left(j'/\pphi\xrightarrow{g}j/\pphi\to \ccat\right)
\]
and on a natural transformation $\nu\mc H_1\nta H_2$ by
\[
\begin{tikzcd}
\lset H_1((i,f)) \xrightarrow{\nu_{(i,f)}} H_2(i,f)\rset_{(i,f)\in j/\pphi}
  \ar[mapsto]{d}\\
\lset H_1((i,f\circ g)) \xrightarrow{\nu_{(i,f\circ g)}} H_2(i,f\circ g)\rset_{(i,f)\in j'/\pphi}
\end{tikzcd}
\]
In particular, we get that the diagram \eqref{7:conetriangle} is the image of \eqref{7:conediagram} under $g^{\ast}$, and hence commutes. \par
Next, we verify that $\gamma\mc G'\nta G$ is a morphism of right extension, i.e. that for every $i\in I$, the diagram
\[
\begin{tikzcd}
  G'(\pphi(i))
  \ar{rd}[above right]{\eta'_i}
  \ar{d}[left]{\gamma_{\pphi(i)}}
  &
  \\
  G(\pphi(i))
  \ar{r}[below]{\eta_i}
  &
  F(i)
  \end{tikzcd}
\]
commutes. But this diagram is nothing but \cref{7:conetriangle} in the case $j = \pphi(i)$, evaluated at $\left(i,\pphi(i)\xrightarrow{\id}\pphi(i)\right)$, and hence commutes.\par
Finally, we need to show that $\gamma$ is another morphism of extensions. So let $\gamma'\mc G'\nta G$ be another morphism of extensions. Then for every $i\in I$, the diagram
\[
\begin{tikzcd}
  G'(\pphi(i))
  \ar{rd}[above right]{\eta'_i}
  \ar{d}[left]{\gamma'_{\pphi(i)}}
  &
  \\
  G(\pphi(i))
  \ar{r}[below]{\eta_i}
  &
  F(i)
  \end{tikzcd}
  \]
  commutes; and for every morphism $f\mc j\to \pphi(i)$ in $J$ the diagram
  \[
  \begin{tikzcd}
  G'(j)
  \ar{r}[above]{G'(f)}
  \ar{d}[left]{\gamma'_j}
  &
  G'(\pphi(i))
  \ar{d}[right]{\gamma'_{\pphi(i)}}\\
  G(j)
  \ar{r}[below]{G(f)}&
  G(\pphi(i))
  \end{tikzcd}
  \]
  does too, since $\gamma'$ is natural. Putting these two together, we obtain that for every object $\left(i,f\mc j\to \pphi(i)\right)$ in $j/\pphi$ the diagram
  \[
  \begin{tikzcd}
    G'(j)
    \ar{rd}[above right]{\xi'_{i,f}}
    \ar{d}[left]{\gamma'_j}
    &
    \\
    G(j)
    \ar{r}[below]{\xi_{i,f}}
    &
    F(i)
  \end{tikzcd}
  \]
  commutes. thus the diagram of natural transformations
  \[
  \begin{tikzcd}
    \Delta(G'(j))
    \ar{rd}[above right]{\xi'}
    \ar{d}[left]{\Delta(\gamma'_j)}
    &
    \\
    \Delta(G(j))
    \ar{r}[below]{\xi}
    &
    F|(j/\pphi)
  \end{tikzcd}
  \]
  commutes. Now, a long time ago (at the very begining of this proof), we defined $\xi\mc \Delta(G(j))\nta F|(j/\pphi)$ to be a limit cone. So finally, by uniqueness, $\gamma_j = \gamma_j'$ follows for all $j\in J$, and we are done!
  \item Recall that we need to find a terminal extension of $F$ along $\pphi$, i.e. a functor $G\mc J\to \ccat$ and a natural transformation $\eta\mc \pphi^{\ast}G \to F$, such that for every other extension $(G',\eta')$ there is a unique natural transformation $\gamma\mc G'\to G$ such that $\eta\circ \pphi^\ast(\gamma)= \eta'$ holds.\par
  We begin by choosing, for every $j\in J$, a limit cone
  \[
  \left(G(j),~\alpha^{(j)}\mc \Delta(G(j))\nta F|(j/\pphi)\right).
  \]
  Then we define the extension $G\mc J\to \ccat$ on objects to be the assignment $j\mapsto G(j)$ in the above sense. Moving on to morphisms, let $g\mc j\to j'$ be a morphism in $J$. Similarly as in the proof of i), we get the induced functor $g^{\pre}\mc j'/\pphi\to j/\pphi$, which induces maps
  \[
  \begin{tikzcd}
    \Delta(G(j))
    \ar[Rightarrow]{rd}[below left]{\alpha^{(j)}\circ g^\pre}
    &
    &
    \Delta(G(j'))
    \ar[Rightarrow]{ld}[below right]{\alpha^{(j')}}\\
    &
    F|(j'/\pphi)
  \end{tikzcd}
  \]
  Now since $\alpha^{(j')}$ is a limit cone, there is a unique morphism
\[G(g)\mc G(j)\to G(j')\] such that $\Delta(G(g))$ makes the above diagram commute. This defines $G$ on morphisms. Similiar to propositions we have seen before (for example in the proof of \cref{6-limitexists}), the uniqueness of $G(g)$ implies the functoriality of $G$. \par
We already see that $G$ satisfies the pointwise formular from the proposition, so what's left to verify is that it is indeed a right Kan extension of $F$ along $\pphi$ -- so we need to construct a natural transformation $\eta\mc \pphi^\ast G\nta F$ which exhibits $G$ as an extension of $F$ and show that it is final with that property. For that, we note that the limit cones chosen in the beginig of the proof of this part supply in particular maps
\[
\alpha^{(\pphi(i))}\mc \Delta(G(\pphi(i)))\to F|(j/\pphi)
\]
which we can evaluate at $(i,i\xrightarrow{\id}i)$ to get maps
\[
\eta_i\defined G(\pphi(i))\xrightarrow{\alpha^{(\pphi(i))}_{(i,\id_i)}} F(i),
\]
which we set as the $i$-th component of the desired natural transformation $\eta\mc \pphi^{\ast}G\nta F$. We are not done yet -- we still need to show that $\eta$ is indeed  natural and that $(G,\eta)$ is in fact a Kan extension. So let $f\mc i\to i'$ be a morphism in $I$ and consider the diagram:
\[
\begin{tikzcd}
G(\pphi(i))
\ar{d}[left]{G(\pphi(f))}
\ar{r}[above]{\eta_i}
\mcom{rd}
&
F(i)
\ar{d}[right]{F(f)}
\\
G(\pphi(i'))
\ar{r}[below]{\eta_{i'}}
&
F(i')
\end{tikzcd}
\]
For that, we note that the diagram
\[
\begin{tikzcd}
  \Delta(G(\pphi(i)))
  \ar[Rightarrow]{rd}[below left]{\alpha^{(i)}\circ (\pphi(f))^\pre}
  \ar[Rightarrow]{rr}[above]{G(\pphi(f))}
  &
  &
  \Delta(G(\pphi(i')))
  \ar[Rightarrow]{ld}[below right]{\alpha^{(j)}\circ g^\pre}\\
  &
  F|(\pphi(i')/\pphi)
\end{tikzcd}
\]
commutes by construction of the $\alpha^{(j)}$, and evaluating at $\pphi(i')$ gives the desired commutativity for $\eta$ to be a natural transformation.\par
Finally, we need to show that $(G,\eta)$ is indeed a Kan extension. For that, we use part i)! Namley, we observe that the $\alpha^{(j)}$ agree with the maps $\xi$ from \eqref{6:conedef} in \cref{6:coneconst}, and hence is indeed a Kan extension.
\end{enumerate}
\end{proof}


\begin{cor}
  Let $\pphi\mc I\to J$ be a functor of small categories and suppose that $\ccat$ has all limits, then there is an adjucntion
  \[
  \pphi^{\ast}\mc \ccat^J\leftrightarrows\ccat^I\mcc \pphi_{\ast},
  \]
  where $\pphi_{\ast}$ assigns to a given $I$-diagram in $\ccat$ its right Kan extension along $\pphi$.
  \par
  Dually, assume that $\ccat$ has all colimits. Then there is an adjunction
  \[
  \pphi_{!}\mc \ccat^I\leftrightarrows\ccat^J\mcc \pphi^{\ast},
  \]
  where $\pphi_{!}$ assigns to a given $I$-diagram in $\ccat$ its left Kan extension along $\pphi$.
\end{cor}
\begin{proof}
  By \cref{6:bigkan}, a Kan extension $G$ of $F$ along $\pphi$ exists (since it can be computed as a limit in $\ccat$, which exists by assumption). The claim now follows directly from \cref{6:smallkan}.
\end{proof}

The following two statements will be important once we want to understand adjunctions involving simplicial sets:

\begin{mfact}\label{7:colimitofrepresentables}
  Let $\ccat$ be a category, then any presheaf on $\ccat$ is a colimit of representables: Denote by $\pphi\mc \ccat\hookrightarrow\fun(\op{\ccat},\setcat)$ the Yoneda embedding and by $\pphi/F$ the category of elements over $F$, which is defined dually to \cref{6:slicecategorydef}. Then $F$ can be written as the colimit:
  \[
  \colim_{\pphi(X)\nta F}\ccat(-,X)
  \]
\end{mfact}
\begin{proof}
  We use the Yoneda Lemma to prove this assertion about the Yoneda Embedding! Let $G$ be any other presheaf, then we have
  \begin{align*}
    \nattraf\left(\colim_{\pphi(X)\nta F}\ccat(-,X),G\right)
    &=
    \lim_{\pphi(X)\nta F}\left(\nattraf(\ccat(-,X),G)\right)
    \\
    &=
    \lim_{\pphi(X)\nta F} G(X),
  \end{align*}
  which is precisley the set of natural transformations from $F$ to $G$.
\end{proof}
\begin{mcor}\label{7:kanadjunction}
  Let $\ccat$ and $\dcat$ be categories, such that $\ccat$ is cocomplete. Let
  \[
  \pphi\mc \ccat\hookrightarrow\setcat_{\ccat}
  \]
  be the Yoneda embedding.
  \begin{enumerate}
   \item For any $F\mc I\to \ccat$, the functor
  \begin{align*}
    R_F\mc \ccat&\to \fun(\op{I},\setcat)\\
    c&\mapsto \left(\ccat(F(-),c)\mc \op{I}\to\setcat,~i\mapsto \ccat(F(i),c)\right)
  \end{align*}
  fits into an adjunction:
  \[
  \pphi_{!}(F)\mc \fun(\op{I},\setcat)\leftrightarrows\ccat\mcc R_F
  \]
  \item In fact, restricting along the Yoneda embedding induces an equivalence of categories
  \[
  \fun^{\mathrm{colim}}(\setcat_{\ccat},\dcat)\xrightarrow{\cong}\fun(\ccat,\dcat)
  \]
  \end{enumerate}
\end{mcor}
\begin{proof}
\leavevmode
\begin{enumerate}
\item
  By the dual version of \cref{6:bigkan}, we can explicitly describe the left Kan extension as a colimit
  \[
  \pphi_{!}(F) = \colim F|(\pphi/j).
  \]
  Now
  \begin{align*}
  \dcat(\pphi_{!}F(H),d)
  &
  \cong \lim_{\pphi(X)\nta H} \dcat(F(X),d)
  \\
  &
  \cong \lim_{\pphi(X)\nta H}G(d)(X)
  \\
  &
  \cong \nattraf(H,G(d))
  \end{align*}
  \item We basically know all of this from the discussions on Kan extensions -- all that is left to show is that for $F\mc \ccat\to \dcat$ the Kan extension $\pphi_{!}F$ is colimit-preserving. But this is immediate, since $\pphi_{!}F$ is left-adjoint.
  \end{enumerate}
\end{proof}
\lec
