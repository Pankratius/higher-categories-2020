%some examples are still missing

\begin{prop}\label{6-limitexists}
  Let $I,\ccat$ be categories and assume that every $I$-diagram in $\ccat$ has a limit. Then there is an adjunction
  \[
  \Delta \mc \ccat \leftrightarrow \ccat^I\mcc \lim
  \]
  where the functor $\lim$ assigns to every $I$-diagram its limit.
\end{prop}

\begin{proof}
  We first construct the functor $\lim$: For every $I$-diagram $F$, we choose a limit cone $(\lim F,\eta_F\mc \Delta(\lim F)\nta F)$. Then the assignement $F\mapsto \lim F$ defines $\lim$ on objects (up to natural isomorphism). Given a natural transformation $\gamma \mc F\nta G$ in $\ccat^I$, we now want to construct a morphism $\lim \gamma\mc \lim F\to \lim G$ in $\ccat$. For that, consider the diagram
  \[
  \begin{tikzcd}
    \Delta(\lim F)
    \ar[Rightarrow]{r}[above]{\eta_F}
    &
    F
    \ar[Rightarrow]{d}[right]{\gamma}
    \\
    \Delta(\lim G)\ar[Rightarrow]{r}[below]{\eta_G}
    &
    G
  \end{tikzcd}
  \]
  Now the composition $\gamma\circ \eta_F$ is a cone over $G$, and since $\Delta(\lim G)$ is a limit cone over $G$, there is a unique natural transformation $\Delta(\lim F)\nta \Delta(\lim G)$ such that the whole diagram commutes:
  \[
  \begin{tikzcd}
    \Delta(\lim F)
    \ar[Rightarrow]{r}[above]{\eta_F}
    \ar[Rightarrow]{rd}[above right]{\gamma \circ \eta_F}
    \ar[dashed, Rightarrow]{d}[left]{\exists !}
    &
    F
    \ar[Rightarrow]{d}[right]{\gamma}
    \\
    \Delta(\lim G)\ar[Rightarrow]{r}[below]{\eta_G}
    &
    G
  \end{tikzcd}
  \]
  But by construction of $\Delta$, this gives in particular a morphism \[\lim \gamma\mc \lim F\to \lim G.\]
  Now consider natural transformations $\gamma\mc F\nta G$ and $\gamma'\mc G\nta H$. We need to show that $\lim(\gamma'\circ \gamma) = \lim(\gamma')\circ \lim(\gamma)$ -- this follows from the universal property of $\lim H$ in the diagram:
  \[
  \begin{tikzcd}
    \Delta(\lim F)
    \ar[Rightarrow]{r}[above]{\eta_F}
    \ar[Rightarrow]{rdd}[below left]{(\gamma'\circ \gamma)\circ \eta_F}
    &
    F
    \ar[Rightarrow]{d}[right]{\gamma}
    \\
    &
    G
    \ar[Rightarrow]{d}[right]{\gamma'}
    \\
    \Delta(\lim H)\ar[Rightarrow]{r}[below]{\eta_H}
    &
    G
  \end{tikzcd}
  \]
  Now by universal property of $\Delta(\lim H)$, there is a unique natural transformation
  \[\Delta(\lim (\gamma'\circ \gamma))\Delta (\lim F)\nta \Delta(\lim H)\]
  that makes the diagram commute -- but we already know that such a natural transformation is given by the composition
  \[
  \Delta(\lim \gamma')\circ \Delta(\lim \gamma)
  \]
  which were constructed above:
  \[
  \begin{tikzcd}
    \Delta(\lim F)
    \ar[Rightarrow]{r}[above]{\eta_F}
    \ar[Rightarrow]{rdd}
    \ar[bend right = 50, Rightarrow,dashed]{dd}[left]{\exists !}
    \ar[Rightarrow]{d}
    &
    F
    \ar[Rightarrow]{d}[right]{\gamma}
    \\
    \Delta(\lim G)
    \ar[Rightarrow]{d}
    &
    G
    \ar[Rightarrow]{d}[right]{\gamma'}
    \\
    \Delta(\lim H)\ar[Rightarrow]{r}[below]{\eta_H}
    &
    G
  \end{tikzcd}
  \]
  After we defined the functor $\lim$, we show that it is indeed part of an adjunction, by constructing a unit and a counit: The collection
  \[
  \eta \defined
  \lset
  \Delta(\lim F)\mc\xRightarrow{\eta_F}F
  \rset_{F\in \ccat^I}
  \]
  gives a natural transformation
  \[\eta\mc \Delta\circ \lim \nta \id_{\ccat^I}\]
  For the counit, we need to specify maps $\eepsilon_x\mc x\to \lim(\Delta(x))$. But by the universal property of the limit cone, this is the same as specifying a cone $\Delta(x)\nta\Delta(x)$ over the constant diagram -- for that we choose the identity. \par
  It remains to show that these natural transformations satisfy the triangle identites.

\end{proof}

In light of the above proposition, we consider the following more general context: Let $I,J,\ccat$ be categories and $\pphi\mc I\to J$ a functor. We then obtain a functor:
\[
\pphi^{\ast}\mc \ccat^J\to \ccat^{I},~F\mapsto F\circ \pphi,
\]
which reduces to $\Delta$ in the case that $J$ is the trivial category. But in general -- is there an adjoint $\pphi_{\ast}\mc \ccat^I\to \ccat^J$?
\begin{defn}
  Let $F\in \ccat^I$. A \emph{right extension of $F$ along $\pphi$} consists of a diagram $G\in \ccat^J$ and a natural transformation $\eta\mc \pphi^{\ast}G\nta F$:
  \[
  \begin{tikzcd}
    I\ar{r}[above]{\pphi}\ar{rd}[below left]{F}[name = U]{}&
    J\ar{d}[right]{G}
    \ar[to = U, Rightarrow]
    \\
    &
    \ccat
  \end{tikzcd}
  \]
  Such a right extension is called a \emph{right Kan extension} if it has the following universal property: For every right extension $(G',\eta')$ of $F$ there is a unique natural transformation $\gamma\mc G'\nta G$ such that the following diagram of natural transformations commutes:
  \[
  \begin{tikzcd}
    \pphi^{\ast}(G')
    \ar[Rightarrow]{rr}[above]{\pphi^{\ast}(\gamma)}
    \ar[Rightarrow]{rd}[below left]{\eta'}&&
    \pphi^{\ast}(G)\ar[Rightarrow]{ld}[below right]{\eta}\\
    &
    F&
  \end{tikzcd}
  \]
  \par A \emph{left extension of $F$ along $\pphi$} is defined in the same way, just with the direction of the natural transformation reversed: It consists of a diagram $G\in \ccat^J$ and a natural transformation $\eta\mc F\nta \pphi^{\ast}G$:
  \[
  \begin{tikzcd}
    I\ar{r}[above]{\pphi}\ar{rd}[below left]{F}[name = U]{}&
    J\ar{d}[right]{G}
    \ar[from = U, Rightarrow]
    \\
    &
    \ccat
  \end{tikzcd}
  \]
  Such a left left extension is called an \emph{left Kan extension} if it has the following universal property: For every left extension $(G',\eta')$ of $F$ there is a unique natural transformation $\gamma\mc G'\to G$ such that the following diagram of natural transformations commutes:
  \[
  \begin{tikzcd}
    \pphi^{\ast}(G')
    \ar[Rightarrow]{rr}[above]{\pphi^{\ast}(\gamma)}
    \ar[Leftarrow]{rd}[below left]{\eta'}&&
    \pphi^{\ast}(G)\ar[Leftarrow]{ld}[below right]{\eta}\\
    &
    F&
  \end{tikzcd}
  \]
\end{defn}
\begin{prop}\label{6:smallkan}
  Let $I,J,\ccat$ be categories and $\pphi\mc I\to J$ be a functor. Assume that every $I$-diagram in $\ccat$ admits a right Kan extension along $\pphi$. Then there is an adjunction
  \[
    \pphi^{\ast}\mc \ccat^J\leftrightarrow \ccat^I\mcc \pphi_{\ast}
  \]
  where $\pphi_{\ast}$ associates to an $I$-diagram its right Kan extension along $\pphi$.
\end{prop}
\begin{proof}
  This goes along the same lines as \cref{6-limitexists} (note that \cref{6-limitexists} is a special case of this proposition for $J = \lset \ast\rset$).
\end{proof}

\begin{defn}\label{6:slicecategorydef}
  Given a functor $\pphi\mc I\to J$ and an object $j\in J$, the \emph{slice category} $j/\pphi$ is defined as follows:\index{slice!category}\index{category!slice}
  \begin{itemize}
    \item Objects are given by pairs $(i,f)$ with $i\in I$ and $f\mc j\to \pphi(i)$ a morphism in $J$.
    \item For two such pairs $(i,f)$ and $(i',f')$, a morphism in $j/\pphi$ is given by a moprhism $g\mc i\to i'$ in $I$ such that the diagram
    \[
    \begin{tikzcd}
      &
      j
      \ar{rd}[above right]{f'}
      \ar{ld}[above left]{f}
      \\
      \pphi(i)\ar{rr}[below]{\pphi(g)}
      &&
      \pphi(i')
    \end{tikzcd}
    \] commutes.
  \end{itemize}
\end{defn}

\begin{numtext}\label{6:coneconst}
Let $\pphi\mc I\to J$ be a functor and $F\in \ccat^I$ be an $I$-diagram. For a given $j\in J$, we denote the pullback of $F$ along the forgetful functor
\[
j/\pphi\to I,~(i,f)\mapsto i
\]
by $F|(j/\pphi)$ -- so $F|(j/\pphi)$ is the $j/\pphi$-diagram in $\ccat$ given by
\[
\begin{tikzcd}[column sep = small, row sep = small]
  j/\pphi
  \ar{r}
  &
  I
  \ar{r}[above]{F}
  &
  \ccat
  \\
  (i,f)
  \ar[mapsto]{r}
  &
  i
  \ar[mapsto]{r}
  &
  F(i)
\end{tikzcd}
\]
Now suppose that $G\in \ccat^J$ is a right extension of $F$ along $\pphi$. Then we want to construct, for every $j\in J$, a cone
\[
\left(G(j),~\xi^{(j)}\mc \Delta(G(j))\nta F|(j/\pphi)\right)
\]
over $F|(j/\pphi)$ in $\ccat$.\par
For that, we need to provide, for every $(i,f)\in j/\pphi$ a morphism
\[
\xi_{i,f}^{(j)}\mc G(j)\to F(i)
\]
in $\ccat$, such that the diagrams
\begin{equation}\tag{$\ast$}\label{6:conedef}
\begin{tikzcd}
  &
  G(j)
  \ar{rd}[above right]{\xi_{i',f}^{(j)}}
  \ar{ld}[above left]{\xi_{i,f}^{(j)}}
  &
  \\
  F(i)\ar{rr}[below]{F(g)}
  &&
  F(i')
\end{tikzcd}
\end{equation}
commute for every morphism $g\mc (i,f)\to (i',f')$ in $j/\pphi$. This is done in two steps:
\begin{enumerate}
  \item The right extension $G$ comes equipped with a natural transformation
  \[
  \eta\mc \pphi^{\ast}G\nta F
  \]
  which we may evaluate at $i\in I$ to obtain a morphism
  $
  \eta_i\mc G(\pphi(i))\to F(i)$.
  \item The morphism $f\mc j\to \pphi(i)$ gives rise to a map
  \[
  G(f)\mc G(j)\to G(\pphi(i)).
  \]
\end{enumerate}
We now define $\xi_{i,f}^{(j)}$ to be the composition
 \[
 G(j)\xrightarrow{G(f)}G(\pphi(i))\xrightarrow{\eta_i}F(i)
 \]
 Now for the commutativity of \eqref{6:conedef}, let $g\mc i \to i'$ be any morphism in $I$. We first note that in the following picture, the left diagram commutes by the definition of $j/\pphi$, and hence the right one does too:
 \[
 \begin{tikzcd}
   &
   j\com{d}
   \ar{rd}[above right]{f}
   \ar{ld}[above left]{f'}
   &
   \\
   \pphi(i)\ar{rr}[below]{\pphi(g)}
   &
   {}
   &
   \pphi(i')
 \end{tikzcd}
 \stackrel{G}{\leadsto}
 \begin{tikzcd}
   &
   G(j)
   \ar{rd}[above right]{G(f)}
   \ar{ld}[above left]{G(f')}
   &
   \\
   G(\pphi(i))\ar{rr}[below]{G(\pphi(g))}
   &&
   G(\pphi(i'))
 \end{tikzcd}
 \]
 Since $\eta$ is a natural transformation $G(\pphi(-))\nta F$, we obtain that in the big diagram, the square $///^2$ commutes too
 \[
 \begin{tikzcd}
   &
   G(j)
   \ar{rd}[above right]{G(f)}
   \ar{ld}[above left]{G(f')}
   &
   \\
   G(\pphi(i))\ar{rr}[below]{G(\pphi(g))}
   \ar{d}[left]{\eta_i}
   \com{rrd}{^2}
   &
   &
   G(\pphi(i'))
   \ar{d}[right]{\eta_{i'}}
   \\
   F(i)
   \ar{rr}[below]{F(g)}
   &
   &
   F(i')
 \end{tikzcd},
 \]
 and by composing the outer morphisms we obtain the commutativity of \eqref{6:conedef}.
\end{numtext}

\begin{theorem}\label{6:bigkan}
  Let $\pphi\mc I\to J$ be a functor, $\ccat$ a category and $F\in \ccat^I$ a diagram.
  \begin{enumerate}
    \item Let $(G,\eta)$ be a right extension of $F$ along $\pphi$ and suppose that, for every $j\in J$, the associated cone from \cref{6:coneconst} is a limit cone. Then $G$ is a right Kan extension of $F$ along $\pphi$.
    \item Vice versa, assume that, for every $j\in J$, the diagram $F|(j/\pphi)$ has a limit in $\ccat$. Then there exists a right Kan extension $G$ along $\pphi$ which admits the \qq{pointwise} formula
    \[
    G(j)\cong \lim F|(j/\pphi).
    \]
  \end{enumerate}
\end{theorem}
\lec
