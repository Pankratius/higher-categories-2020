
\begin{theorem}
  Let $\ccat$ be a model category, with $\weake$ the class of weak equivalences. Then:
  \begin{enumerate}
    \item The association
    \[
    \pi\mc \ccat\to \ho(\ccat),~X\mapsto \repr \repq X
    \]
    yields a well-defined functor.
    \item The functor $\pi$ exhibits $\ho(\ccat)$ as a localization of $\ccat$ along $\weake$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  To show i), we show that the homotopy class $[\repr\repq f]$ depends only on $f$ and not on the choices of lifts in \eqref{18:lift1} and \eqref{18:lifting2}. Suppose $f_1,f_2\mc \repq X\to\repq Y$ are both lifts for \eqref{18:lift1}. Let $C$ be a cylinder for $\repq X$ and consider the lifting problem:
  \[
  \begin{tikzcd}
  \repq X \amalg \repq X
  \ar{d}
  \ar{r}[above]{(f_1,f_2)}
  &
  \repq Y
  \ar{d}[right]{p_Y}
  \\
  C
  \ar{r}[below]{f\circ p_X\circ t}
  &
  Y
  \end{tikzcd}
  \]
  Since the vertical-left map is a cofibration and the vertical-right map a trivial fibration, we get a solution $H\mc C\to \repq Y$, which defines a left homotopy between $f_1$ and $f_2$.
  Thus the composition $i_YH\mc C\to \replace Y$ defines a left homotopy between $i_Yf_1$ and $i_Yf_2$, and so there is a also right homotopy between $i_Yf_1$ and $f_Yf_2$
  \[
  \wtilde{H}\mc\repq X\to P,
  \]
  where $P$ is a path object for $\replace Y$.\par
  Let further $g_1$, $g_2$ be two extensions of $f_1$ and $f_2$, solving the lifting problem \eqref{18:lifting2} for $\repq f= f_1$ and $\repq f = f_2$ respectivley.
  Consider the lifting problem:
  \[
  \begin{tikzcd}
  \repq X
  \ar{r}[above]{\wtilde{H}}
  \ar{d}
  &
  P
  \ar{d}
  \\
  \replace X
  \ar{r}[below]{(g_1,g_2)}
  \ar[dashed]{ur}
  &
  \replace Y\times \replace Y
  \end{tikzcd}
  \]
  Its solution
  \[
  H'\mc \replace X\to P
  \]
  provides a homotopy between $g_1$ and $g_2$. This shows i).\par
  For ii), we first introduce an auxiliary category $\overline{\ccat}$, with objects given by the objects of $\ccat$ and morphisms given by:
  \[
  \overline{\ccat}(X,Y)\defined
  \ho(\ccat)(\replace X,\replace Y)
  \]
  Then the functor $\pi\mc \ccat\to \ho(\ccat)$ factors as
  \[
  \begin{tikzcd}[column sep = small]
  \ccat
  \ar{r}[above]{\overline{\pi}}
  &
  \overline{\ccat}
  \ar{r}[above]{\xi}
  &
  \ho(\ccat),
  \end{tikzcd}
  \]
  where $\overline{\pi}$ is the identity on objects and on morphisms given by $f\mapsto [\replace f]$; the functor $\xi$ is given on objects by $X \mapsto \replace X$ and is the identity on morphisms.
  It is thus surjective on objects and fully faithful and hence an equivalence.
  So it suffices to see that $\overline{\pi}$ is a localization of $\ccat$ along $\weake$.\par
  To this end, we wil show that $\overline{\pi}$ satisfies the universal property of the localization, i.e. that given a functor $F\mc \ccat\to\dcat$ such that $F$ maps weak equivalences to isomorphisms there exists a unique functor $\overline{F}\mc \overline{C}\to\overline{D}$ such that the following diagram commutes:
  \[
  \begin{tikzcd}
    \ccat
    \ar{r}[above]{F}
    \ar{d}[left]{\overline{\pi}}
    &
    \dcat
    \\
    \overline{C}
    \ar[dashed]{ur}{\exists !\overline{F}}
    &
  \end{tikzcd}
  \]
  Since $\overline{\pi}$ is the identity on objects, we are forced to set $\overline{F}(X)\defined F(X)$. We next show that $F$ identifies (left and right) homotopic morphisms in $\ccat$: Suppose we are given a right homotopy
  \[
  \begin{tikzcd}
    &
    Y
    &
    \\
    X
    \ar{ur}[above left]{f}
    \ar{r}[above]{H}
    \ar{rd}[below left]{g}
    &
    P
    \ar{u}[left]{p_0}
    \ar{d}[left]{p_1}
    &
    Y
    \ar{l}[above]{s}
    \ar[equal]{ul}
    \ar[equal]{ld}
    \\
    &
    Y
    &
  \end{tikzcd}
  \]
  Since $F(s)$ is an isomorphism and
  \[
  id_{F(y)} = F(\id_y) = F(p_0)\circ F(s)
  \]
  we see that $F(p_0)$ is the inverse of $F(s)$. But the same holds for $F(p_1)$ and thus $F(p_0) = F(p_1)$. This also implies that
  \begin{align*}
    F(f)  &= F(p_0)\circ F(H)\\
          &= F(p_1)\circ F(H)\\
          &= F(g).
  \end{align*}
  We then define, for $[g]\in \overline{C}(X,Y)$:
  \[
  \overline{F}([g])\defined
  F(p_Y)\circ F(i_Y)^{-1}\circ F(g)\circ F(i_X)\circ F(p_X)^{-1}
  \]
  This satisfies $F(f) = \overline{F}([\replace f])$, i.e. $F = \overline{F}\circ \overline{\pi}$.
  This choice of $\overline{F}$ is indeed unique.
\end{proof}

\subsection{Quillen Adjunctions}
\begin{prop}
  Let $\ccat$, $\dcat$ be model categories and let
  \[
  F\mc \ccat \leftrightarrows \dcat\mcc G
  \]
  be an adjunction. Then the following are equivalent:
  \begin{enumerate}
    \item The functor $F$ preserves cofibrations and trivial cofibrations.
    \item The functor $G$ preseves fibrations and trivial fibrations.
  \end{enumerate}
\end{prop}
\begin{defn}
  Such an adjunction of model categories is called a \emph{Quillen adjunction}.
\end{defn}
\lec
