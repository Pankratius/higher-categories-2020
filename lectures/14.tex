\begin{cor}
  Let $f$ be an anodyne morphism and $g$ a monomorphism. Then $f\wedge g$ is anodyne.
\end{cor}

\begin{proof}
  We already know that for a fixed morphism $g$, the set of morphisms
  \[
  \lset f'\ssp f'\wedge g\text{ is anodyne}\rset
  \]
  is saturated. It thus suffices to verify that for and $f'\in M_3$ and any monomorphism $g$, the morphism $f'\wedge g$ is anodyne. We
  \begin{align*}
    &\left( \left( \lset v\rset \to \Delta^1\right)\wedge \left(K\hookrightarrow S\right)\right)\wedge g\\
    \cong & \left(\lset v\rset\to \Delta 1\right)\wedge \left(\left(K\hookrightarrow S\right)\wedge g\right)
  \end{align*}
  which is an element of $M_3$.
\end{proof}

\subsection{Mapping spaces}
\begin{construction}
  Let $K$ and $S$ be simplicial sets. We introduce the \emph{simplicial set $\maps(K,S)$ of maps from $K$ to $S$} by setting
  \[
  \maps(K,S)_n\defined \sset\left(K\times \Delta^n,S\right)
  \]
  The functoriality in $\npcat$ is obtained via the Yoneda embedding.\par
  The simplicial set $\maps(K,S)$ comes equipped with an \emph{evaluation morphism}
  \[
  \ev\mc K\times \maps(K,S)\to S
  \]
  which associates to a pair $(\sigma,f)$ with $\sigma \in K_n$ and $f\mc K\times \Delta^n\to S$ the $n$-simplex:
  \[
  f(\sigma,\id\mc \npcat\to\npcat)\in S_n
  \]
\end{construction}

\begin{prop}\label{14:mapsadjunction}
  Let $K$ be a simplicial set. Then there is an adjunction:
  \[
  K\times-\mc\sset \leftrightarrows\sset\mc \maps(K,-)
  \]
\end{prop}
\begin{prop}
  Let $i\mc A\hookrightarrow B$ be a monomorphism of simplicial sets and $p\mc K\to S$ a Kan fibration. Then the morphism
  \[
  \maps(B,K)\to\maps(A,K)\times_{\maps(A,S)}\maps(B,S)
  \]
  induced by the commutative square
  \[
  \begin{tikzcd}
    \maps(B,K)
    \ar{r}
    \ar{d}
    &
    \maps(A,K)
    \ar{d}
    \\
    \maps(B,S)
    \ar{r}
    &
    \maps(A,s)
  \end{tikzcd}
  \]
  is a Kan fibration.
\end{prop}
\begin{proof}
  We verify that any lifting problem of the form
  \[
  \begin{tikzcd}
    A'
    \ar{d}[left]{j}
    \ar{r}
    &
    \maps(B,K)
    \ar{d}
    \\
    B'
    \ar[dashed]{ur}[below right]{?}
    \ar{r}
    &
    \maps(A,K)\times_{\maps(A,S)}\maps(B,S)
  \end{tikzcd}
  \]
  with $j\mc A'\to B'$ anodyne
  has a solution. By \cref{14:mapsadjunction}, we may pas to the adjoint morphisms and equivalently solve the lifting problem:
  \[
  \begin{tikzcd}
  A'\times B\displaystyle \coprod_{A'\times A} B'\times A
  \ar{r}
  \ar{d}[left]{j\wedge i}
  &
  K
  \ar{d}[right]{p}
  \\
  B'\times B
  \ar{r}
  &
  S
  \end{tikzcd}
  \]
  Now the map $j\wedge i$ is anodyne too (\cref{addreference})
  and so we indeed get a solution.
\end{proof}

\begin{cor}\label{14:kanfibexamples}
  Let $K$ be a Kan complex.
\begin{enumerate}
  \item Let $B$ be any simplicial set. Then the simplicial set $\maps(B,K)$ is a Kan complex.
  \item Let $A\hookrightarrow B$ be a monomorphism of simplicial sets, the the restriction map
  \[
  \maps(B,K)\to \maps(A,K)
  \]
  is a Kan fibration.
\end{enumerate}
\end{cor}

\subsection{Simplical homotopy}
\begin{defn}
  Let $f,g\mc B\to K$ be morphisms of simplicial sets.
  \begin{enumerate}
    \item A \emph{homotopy from $f$ to $g$} is a morphism
    \[
    H\mc \Delta^1\times B\to K
    \]
    such that $\restrict{H}{\lset 0\rset \times B} = f$ and $\restrict{H}{\lset 1\rset \times B} = g$.
    We say \emph{$f$ is homotopic to $g$} if there exists a homotopy $H$ from $f$ to $g$ and write $f\overset{H}{\simeq}g$ or simpliy $f\overset{H}{\simeq}g$.
    \item Suppose in addition we are given monomorphisms $i\mc A\to B$ and that $\restrict{f}{A}=\restrict{g}{A} = n$. We say that $H$ is a \emph{homotopy relative $A$} if the diagram
    \[
    \begin{tikzcd}
      \Delta^1\times A
      \ar[twoheadrightarrow]{r}
      \ar{d}[left]{\id\times i}
      &
      A
      \ar{d}[right]{n}
      \\
      \Delta^1\times B
      \ar{r}[below]{H}
      &
      K
    \end{tikzcd}
    \]
    commutes.
    We say \emph{$f$ is homotopic to $g$ relative to $A$} if there is a homotopy from $f$ to $g$ relative $A$; in this case, we write:
    \[
    f \overset{H}{\simeq}g~(\text{rel }A)
    \]
  \end{enumerate}
\end{defn}
\begin{prop}
  Let $i\mc A\hookrightarrow B$ be a monomorphism of simplicial sets, $K$ a Kan complex and $n\mc A\to K$ a fixed morphism. Then the relation of homotopy relative to $A$ defines an equivalence relation on the set of all morphisms $f\mc B\to K$ with $\restrict{f}{A}=n$.
\end{prop}
\begin{proof}
  We first note that Kan fibrations are stable under pullback, i.e. given a cartesian diagram
  \[
  \begin{tikzcd}
    X'
    \ar{d}[left]{p'}
    \ar{r}
    \ar[phantom]{rd}{\lrcorner}
    &
    X
    \ar{d}[right]{p}
    \\
    Y'
    \ar{r}
    &
    Y
  \end{tikzcd}
  \]
  with $p$ a Kan fibration, then $p'$ is a Kan fibration too.
  We apply this to the cartesian square
  \[
  \begin{tikzcd}
    \maps(B,K)^n
    \ar{r}
    \ar{d}
    \ar[phantom]{dr}{\lrcorner}
    &
    \maps(B,K)
    \ar{d}[right]{i^\ast}
    \\
    \lset n\rset
    \ar{r}
    &
    \maps(A,K)
  \end{tikzcd}
  \]
  Now the map $i^\ast$ is a Kan fibration (\cref{14:kanfibexamples}, ii)), and so $\maps(B,K)^n\to \lset n\rset$ is too.\par
  The set of vertices of $\maps(B,K)^n$ is precisley the set of morphisms $f\mc B\to K$ such that $\restrict{f}{A}=n$. Furthere, such morphisms $f,g\mc B\to K$ are homotopic relative $A$ if and only if the corresponding vertices
  \[
  \Delta^0\to \maps(B,K)^n
  \]
  are homotopic in the absolute sense. We have thus reduced the problem to showing tha the relation of homotopy defines an equvialence relation on the set of vertices of a Kan complex, i.e. we may assume $A = \emptyset$ and $B = \Delta^0$. We verify this explicitly:
  \begin{itemize}
    \item reflexive: Every vertex $x$ of $K$ is homotopic to itself via the degenerate edge $s_0(x)$.
    \item symmetric: Let $x,y\in K_0$ with $x\overset{e}{\simeq}y$:
    \[
    \begin{tikzcd}
      &
      y
      &
      \\
      x
      \ar{ur}[above left]{e}
      \ar{rr}[below]{s_0(x)}
      &
      &
      x
    \end{tikzcd}
    \]
    We fill the corresponding horn $\horn{0}{2}\to K$ to obtain a $2$-simplex:
    \[
    \begin{tikzcd}
      &
      y
      \ar{dr}[above right]{e'}
      \com{d}
      &
      \\
      x
      \ar{ur}[above left]{e}
      \ar{rr}[below]{s_0(x)}
      &
      {}
      &
      x
    \end{tikzcd}
    \]
    Then $y\overset{e'}{\simeq}x$.
    \item transitive: Assume $x\overset{e}{y}$ and $y\overset{e'}{\simeq}z$. We fill the horn
    \[
    \begin{tikzcd}
      &
      y
      \ar{dr}[above right]{e'}
      &
      \\
      x
      \ar{ur}[above left]{e}
      &
      {}
      &
      z
    \end{tikzcd}
    \]
    to obtain the two-simplex
    \[
    \begin{tikzcd}
      &
      y
      \ar{dr}[above right]{e'}
      \com{d}
      &
      \\
      x
      \ar{ur}[above left]{e}
      \ar{rr}[below]{e''}
      &
      {}
      &
      z
    \end{tikzcd}
    \]
    Then $\overset{e''}{\simeq}z$.

  \end{itemize}
\end{proof}

\begin{defn}
  Let $K$ be a Kan complex.
  \begin{enumerate}
    \item We define $\pi_0(K)$ to be the set of homotopy classes of vertices of $K$.
    \item For $n\geq 1$ and a vertex $x\in K_0$, we define $\pi_n(K,x)$ to be the set of homtopy classes relative $\partial \Delta^n$, of morphisms $f\mc \Delta^n\to K$ such that $\restrict{f}{\partial \Delta^n}$ is the constant morphism:
    \[
    \partial \Delta^n\to\Delta^n\xrightarrow{x}K
    \]
  \end{enumerate}
\end{defn}
