\begin{example}
  That a map $\ccat\un{x}\to \ccat$ is a trivial Kan fibrations means that for all $n\geq 0$, every lifting problem of the form
  \[
  \begin{tikzcd}
  \partial \Delta^n
  \ar{r}
  \ar{d}
  &
  \ccat\un{x}
  \ar{d}[right]{\pi}
  \\
  \Delta^n
  \ar[dashed]{ur}
  \ar{r}
  &
  \ccat
  \end{tikzcd}
  \]
  has a solution. We explicitly anazly the conditions for the existence of a solution for this problem in low dimension:
  \begin{itemize}
    \item In the case $n = 0$, we have $\partial \Delta^0 = \emptyset$ and a morphism $\Delta^0\to \ccat\un{x}$ corresponds to the choice of an object in $\ccat\un{x}$, and similarly for $\ccat$. A solution in the diagram
    \[
    \begin{tikzcd}
      &
      \ccat\un{x}
      \ar{d}[right]{\pi}
      \\
      \Delta^0
      \ar[dashed]{ur}
      \ar{r}
      &
      \ccat
    \end{tikzcd}
    \]
    means that for every object $y\in \ccat 0$, there exists a vertex
    \[
    f\in (\ccat\un{x})_0 =
    \lset
    f\mc \underbrace{\Delta^0\ast\Delta^0}_{=\Delta^1}\to \ccat \ssp \restrict{f}{\Delta^0} = x
    \rset,
    \]
    such that $\pi(f) = y$. This means nothing more than that there exists a morphism $f\mc x\to y$ in $\ccat$.
    \item For $n=1$, suppose we are given edges $f\mc x\to y$ and $g\mc x\to y$ in $\ccat$. The existence of a solution to the lifting problem
    \[
    \begin{tikzcd}
      \Delta^0\amalg \Delta^0
      \ar{rr}[above]{(f,g)}
      \ar[hookrightarrow]{d}
      &
      &
      \ccat\un{x}
      \ar{d}[right]{\pi}
      \\
      \Delta^1
      \ar[dashed]{urr}
      \ar{r}
      &
      \Delta^0
      \ar{r}[below]{y}
      &
      \ccat
    \end{tikzcd}
    \]
    Now $H\mc \Delta^1\to \ccat\un{x}$ determines an edge in $\ccat\un{x}$, which we will also denote $H$, which determines a map
     \[
     \Delta^0\ast\Delta^1\to \ccat,
     \]
     i.e. a two-simplex in $\ccat$. The condition that the diagram commutes ammounts to saying that this two-simplex is of the form
     \[
     \begin{tikzcd}
       &
       1
       \ar{rd}[above right]{s_0(y)}
       &
       \\
       0
       \ar{rr}[below]{g}
       \ar{ur}[above left]{f}
       &
       &
       2
     \end{tikzcd}
     \]
     Thus $H$ defines a homotopy between $f$ and $g$, so in particular $[f] = [g]$ and $x$ is an initial object in $\hoh \ccat$.
  \end{itemize}
\end{example}
\begin{prop}\label{24:initialcontractible}
  Let $\ccat$ be an $\infty$-category and suppose that $\ccat$ has an initial object. Let $\ccat'\sse \ccat$ be the simplicial subset consisting of those simplices of which all vertices are initial objects in $\ccat$ (the full $\infty$-subcategory on the initial objects). Then $\ccat'$ is a contractible Kan complex.
\end{prop}
\begin{proof}
  We show that any lifting problem
  \begin{equation}\label{24:contractible}\tag{$\ast$}
    \begin{tikzcd}
      \partial \Delta^n
      \ar{r}[above]{f}
      \ar[hookrightarrow]{d}
      &
      \ccat'
      \ar{d}
      \\
      \Delta^n
      \ar[dashed]{ur}
      \ar{r}
      &
      \Delta^0
    \end{tikzcd}
    \end{equation}
    can be solved, i.e. that the map $\ccat'\to \Delta^n$ is a trivial Kan fibration. The case $n=0$ corresponds to $(\ccat')_0\neq \emptyset$, which is true. In the case $n>0$, \eqref{24:contractible} is equivalent to the lifting problem
    \[
    \begin{tikzcd}
      \partial\Delta^{\lset 1,\ldots,n\rset}
      \ar{r}
      \ar{d}
      &
      \ccat\un{x}
      \ar{d}[right]{\pi}
      \\
      \Delta^{\lset 1,\ldots,n\rset}
      \ar[dashed]{ur}
      \ar{r}
      &
      \ccat
    \end{tikzcd},
    \]
    where $x = \restrict{f}{\Delta^0}$. But this can be solved, since $x$ is an initial object, which means that $\pi$ is a trivial Kan fibration.
\end{proof}
\begin{defn}
  Let $\ccat$ be an $\infty$-category, $K$ a simplicial set and $p\mc K\to \ccat$ a $K$-diagram.
  \begin{enumerate}
    \item A \emph{colimit cone for $p$} is an initial object in $\ccat\un{p}$.
    \item Dually, a \emph{limit cone for $p$} is a final object in $\ccat\ov{p}$.
  \end{enumerate}
\end{defn}
\begin{rem}
  By \cref{24:initialcontractible}, limit and colimit cones are unique up to contractible choice, \emph{once they exist}. Establishing their existence requires substantial work.
\end{rem}

\section{Left fibrations}
\begin{defn}
  We introduce the following types of fibrations:
  \begin{itemize}
    \item \emph{Kan fibrations}: $\lset \horn{i}{n}\hookrightarrow \Delta^n\ssp 0\leq i\leq n\rset\rlp$;
    \item \emph{left fibrations}: $\lset \horn{i}{n}\hookrightarrow \Delta^n\ssp 0\leq i< n\rset\rlp$;
    \item \emph{right fibrations}: $\lset \horn{i}{n}\hookrightarrow \Delta^n\ssp 0< i\leq n\rset\rlp$;
    \item \emph{inner fibrations}: $\lset \horn{i}{n}\hookrightarrow \Delta^n\ssp 0< i< n\rset\rlp$.
  \end{itemize}
  and \emph{anodyne}/\emph{left anodyne}/\emph{right anodyne}/\emph{inner anodyne} to be those morphisms with the left lifting property agains the Kan/left/right/inner fibrations, respectivley.
\end{defn}
We will begin by analyzing left fibrations.
\begin{defn}
  Let $F\mc \ccat\to \dcat$ be a functor of ordinary categories. We say that \emph{$F$ exhibits $\ccat$ as cofibered in groupoids over $\dcat$} if:
  \begin{enumerate}
    \item For every $c\in \ccat$ and every morphism $\eta\mc F(c)\to d$ in $\dcat$, there exists a lift $\wtilde{\eta}\mc c\to\wtilde{d}$ such that $F(\wtilde{\eta}) = \eta$. This corresponds to a solution in the lifting problem:
    \[
    \begin{tikzcd}
      \Delta^{0} = \horn{0}{1}
      \ar{r}[above]{c}
      \ar[hookrightarrow]{d}
      &
      \nerve(\ccat)
      \ar{d}[right]{\nerve(F)}
      \\
      \Delta^1
      \ar[dashed]{ur}
      \ar{r}[below]{\eta}
      &
      \nerve(\dcat)
    \end{tikzcd}
    \]
    \item For every morphism $\eta\mc c\to c'$ in $\ccat$ and every object $c''\in \ccat$, the map
    \[
    \ccat(c',c'')\to
    \ccat(c,c'')\underset{\times}{\dcat(F(c),F(c''))}
    \dcat(F(c'),F(c''))
    \]
    is bijective. This amounts to saying that every lifting problem of the form
    \[
    \begin{tikzcd}
      \horn{0}{2}
      \ar{r}
      \ar[hookrightarrow]{d}
      &
      \nerve(\ccat)
      \ar{d}
      \\
      \Delta^2
      \ar[dashed]{ur}
      \ar{r}
      &
      \nerve(\dcat)
    \end{tikzcd}
    \]
    has a solution.
  \end{enumerate}
\end{defn}
\begin{example}
  Let $\dcat$ be a category with pushouts. We define the category $\dcat_{\#}$ to be the category with objects given by morphisms $x\to y$ in $\dcat$; morphisms between $x\to y$ and $x'\to y'$ are given by pushout-squares of the form
  \[
  \begin{tikzcd}
    x
    \ar{r}
    \ar{d}
    \ar[phantom]{rd}{\ulcorner}
    &
    x'
    \ar{d}
    \\
    y
    \ar{r}
    &
    y'
  \end{tikzcd}
  \]
  We have a canonical forgetful functor
  \[
  F\mc \dcat_{\#} \to \dcat,~\left(x\to y\right)\mapsto x.
  \]
  We claim that $F$ exhibits $\dcat_{\#}$ as cofibered in groupoids:
  \begin{enumerate}
    \item Given an object $x\to y$ in $\dcat_{\#}$ and a morphism $x\xrightarrow{\eta}x'$ in $\dcat$, we need to find a ``lift''
    \[
    \wtilde{\eta}\mc
    \begin{tikzcd}
      x
      \ar{r}
      \ar{d}
      \ar[phantom]{rd}{\ulcorner}
      &
      x'
      \ar{d}
      \\
      y
      \ar{r}
      &
      y'
    \end{tikzcd},
    \]
    which is equivalent to the existence of pushouts in $\dcat$.
    \item Given a pushout-diagram of the form
    \[
    \begin{tikzcd}
      x
      \ar{r}
      \ar{d}
      \ar[phantom]{rd}{\ulcorner}
      &
      x'
      \ar{r}
      \ar{d}
      &
      x''
      \\
      y
      \ar{r}
      &
      y'
    \end{tikzcd}
    \]
    and a morphism $x''\to y''$ in $\dcat$, we want that pushout-squares of the form
    \[
    \begin{tikzcd}
    x'
    \ar{r}
    \ar{d}
    \ar[phantom]{rd}
    &
    x''
    \ar{d}
    \\
    y'
    \ar{r}
    &
    y''
    \end{tikzcd}
    \]
    are in bijection with ``big''-pushout-squares of the form
    \[
    \begin{tikzcd}
      x
      \ar{r}
      \ar{d}
      \ar[phantom]{rrd}
      &
      x'
      \ar{r}
      &
      x''
      \ar{d}
      \\
      y
      \ar{r}
      &
      y'
      \ar{r}
      &
      y''
      \end{tikzcd},
      \]
      which is true.
  \end{enumerate}
\end{example}
\begin{example}[Grothendieck construction]
  Let
  \[
  \chi\mc \dcat\to \grpdcat
  \]
  be a functor into the category $\grpdcat$ of groupoids. We defined a category $\ccat_{\chi}$ called the \emph{Grothendieck construction} as follows:
  \begin{itemize}
    \item Objects are given by pairs $(d,x)$, where $d\in \dcat$ and $x\in \chi(d)$.
    \item A morphism between a pair $(d,x)$ and $(d',x')$ is a pair $(f,\alpha)$ where $f\mc d\to d'$ is a morphism in $\dcat$ and
    \[
    \alpha\mc \chi(f)(x)\to x'.
    \]
    Note that $\alpha$ is automatically an isomorphism, since it is a morphism in the groupoid $\chi(d')$.
    \item For the composition of morphisms $(d,x)\xrightarrow{(f,\alpha)}(d',x')$ and $(d',x')\xrightarrow{(g,\beta)}(d'',x'')$ we define the morphism $d\to d''$ to be the composition $g\circ f$ and the morphism
    \[
    \chi(g\circ f)(x) \to x''
    \]
    to be the composition
    \[
    \begin{tikzcd}
      \chi(g\circ f)(x)
      \ar[equal]{d}
      \ar[bend left = 20, dashed]{rrd}
      &
      &
      \\
      \chi(g)\left(\chi(f)(x)\right)
      \ar{r}[below]{\chi(g)(\alpha)}
      &
      \chi(g)(x')
      \ar{r}[below]{\beta}
      &
      x''
    \end{tikzcd}
    \]
  \end{itemize}
  We have a canonical forgetful functor
  \[
  F\mc \ccat_{\chi}\to \dcat,~(d,x)\mapsto d,
  \]
  which exhibits $\ccat_{\chi}$ as cofibered in groupoids.
\end{example}
\begin{construction}
  We now want to reverse the above construction:
  Given a functor $F\mc \ccat\to\dcat$ be cofibered in groupoids, we want to construct a suitable $\chi\mc \dcat\to \grpdcat$.
  On objects, we define, for an object $d\in \dcat$, $\chi(d)$ to be the fibre of $f$ above $d$, i.e. the pullback
  \[
   \ccat\underset{\dcat}{\times}\lset d\rset \eqqcolon \chi(d).
  \]
  This is indeed a groupoid: By the second property of a functor cofibered in groupoids, every morphism $\eta$ in $\chi(d)$ has a left inverse:
  \begin{equation}\label{24:leftinverse}\tag{$\ast$}
  \eta'\circ \eta = \id
  \end{equation}
  But then, we may for the left inverse of $\eta'$, i.e. there is a $\eta''$ such that $\eta''\circ \eta' = \id$. Applying $\eta''\circ -$ to \eqref{24:leftinverse} yields $\eta=\eta''$.\par
  Given a morphism $f\mc d\to d'$, we now need to define a functor
  \[
  \chi(f)\mc \chi(d)\to\chi(d').
  \]
\end{construction}
