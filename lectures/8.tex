\begin{numtext}\label{8:weakkane}
  \itodo{add remark about weak kan extensions}
  As a special case, we consider the case that $\pphi\mc I\hookrightarrow J$ is the inclusion, $G\mc J\to \ccat$ is given and $F=\restrict{G}{I}$ is the restriction.
  It is immediatley clear that the choice
 \[
 \eta\defined \id\mc \restrict{G}{I}=F
 \]
 makes the pair $(G,\eta)$ into an extension of $F$ along $\pphi$, as the diagram
 \[
 \begin{tikzcd}
   I
   \ar[hookrightarrow]{r}[above]{\pphi}
   \ar{rd}[below left]{\restrict{G}{I} = F}
   &
   J
   \ar{d}[right]{G}
   \\
   &
   \ccat
 \end{tikzcd}
 \]
 literally commutes. We now observe that for every $i\in I$, the condition \kancond is already satisfied: We need to show that the cone
 \[
 \xi^{(i)}\mc \Delta(G(i))\nta F|(i/\pphi) = F
 \]
 is a limit cone for every $i\in I$.
 First, we see from \cref{6:coneconst} that for $(i',~i\xrightarrow{f}i')$ the component $\xi^{(i)}_{i',f}$ is given by the map
 \[
 G(i) = F(i)\xrightarrow{G(f)=F(f)}G(i') = F(i')
 \]
 Now for $\xi^{(i)}$ to be a limit cone over $F$ it suffices to find, for every other cone $(x,~\nu\mc \Delta(x)\nta F)$ over $F$ a unique morphism of cones $\Delta(x)\to \Delta(G(i))$, i.e. for every pair $(i',~i\xrightarrow{f}i')$ a unique morphism
 $x\to F(i)$ such that the diagram
 \[
 \begin{tikzcd}
   x
   \ar{rd}[above right]{\nu_{i',f}}
   \ar[dashed]{d}[left]{\exists!~?}
   &
   \\
   F(i)
   \ar{r}[below]{F(f)}
   &
   F(i')
 \end{tikzcd}
 \]
 commutes. But by naturality of $\nu$, we have that
 \[
 \begin{tikzcd}
   x
   \ar[equal]{r}
   \ar{d}[left]{\nu_{i,\id}}
   &
   x
   \ar{d}[right]{\nu_{i',f}}
   \\
   F(i)
   \ar{r}[below]{F(f)}
   &
   F(i')
 \end{tikzcd}
\] commutes, and hence the unique choice is given by $\nu_{i,\id}$.
\itodo{this should be \emph{way} more easy!}
\end{numtext}

\begin{example}
  Let $J = [1]\times [2]$ be the category associated to the product of posets \[\lset 0\leq 1\rset \times \lset 0\leq 1\leq 2\rset\] with the product ordering. We can visualize $J$ as two consecutive commutative diagrams:
  \[
    \begin{tikzcd}
      (0,0)
      \ar{d}
      \ar{r}
      \com{rd}
      &
      (0,1)
      \ar{d}
      \ar{r}
      \com{rd}
      &
      (0,2)
      \ar{d}
      \\
      (1,0)
      \ar{r}
      &
      (1,1)
      \ar{r}
      &
      (1,2)
    \end{tikzcd}
    \]
    A functor $G\mc J\to \ccat$ corresponds to the choice of such commutative diagrams in $\ccat$. In this example, we want to understand conditions on $G$ to be a right Kan extension of its restriction to the full subcategory $I$ given by
    \[
    \begin{tikzcd}
      &
      (0,2)
      \ar{d}
      \\
      (1,1)
      \ar{r}
      &
      (1,2)
    \end{tikzcd},
    \]
    i.e. we set $F\defined \restrict{G}{I}$ and let $\pphi\mc I\hookrightarrow J$ be the inclusion of the full subcategory. Following \cref{8:weakkane}, we spell out the condition \kancond for every $j\in J\setminus I$
    \begin{itemize}
      \item For $j = (0,1)$ the slice category $j/\pphi$ is given by:
      \[
      \begin{tikzcd}
        &
        (0,1)
        \ar[dash]{dd}
        \ar[dash]{rd}
        \ar[dash]{ld}
        &
        \\
        (1,1)
        \ar{rd}
        \com{r}
        &
        {}
        \com{r}
        &
        (0,2)
        \ar{ld}
        \\
        &
        (1,2)
        &
      \end{tikzcd}
      \]
      (Note that we have omited the arrow tips for the morphisms in $J$ that arrise in the definition of $j/\pphi$ -- only morphisms in $j/\pphi$ have arrow tips). Unraveling the definition of the $\xi^{(j)}$, we find that $G(0,1)$ is the desired limit cone if and only if it is a limit of the diagram
      \[
      \begin{tikzcd}
      &
      F(0,2)
      \ar{d}
      \\
      F(1,1)
      \ar{r}
      &
      F(1,2)
      \end{tikzcd}
      \]
      in $\ccat$, i.e. iff the square
      \[
      \begin{tikzcd}
      G(0,2)
      \ar{r}
      \ar{d}
      \pullar
      &
      F(0,2)
      \ar{d}
      \\
      F(1,1)
      \ar{r}
      &
      F(1,2)
      \end{tikzcd}
      \]
      is cartesian (which we denote by the little wedge in the upper-left corner).
      \item The same holds for $(0,0)$, i.e. $G(0,0)$ satisfies \kancond if and only if the square
      \[
      \begin{tikzcd}
      G(0,0)
      \ar{r}
      \ar{d}
      &
      F(0,2)
      \ar{d}
      \\
      F(1,1)
      \ar{r}
      &
      F(1,2)
      \end{tikzcd}
      \]
      is cartesian in $\ccat$.
      \item For $j = (1,0)$ the slice category $j/\pphi$ has the form
      \[
      \begin{tikzcd}
        &
        (1,0)
        \ar[dash]{rd}
        \ar[dash]{ld}
        &\\
        (1,1)
        \ar{rr}
        &
        &
        (1,2)
      \end{tikzcd}
      \]
      Now a cone for
      \[
      \begin{tikzcd}
        F(1,1)\ar{rr}&&F(1,2)
      \end{tikzcd}
      \]
      in $\ccat$ is the same as the choice of a morphism $h\mc x\to F(1,1)$ in $\ccat$. It is universal if and only if $h$ is an isomorphism. So $G(0,1)$ satisfies $K$ if and only if in the commutative triangle
      \[
      \begin{tikzcd}
      &
      G(1,0)
      \ar{rd}[above right]{G((1,0)\leq (1,2))}
      \ar{ld}[above left]{G((1,0)\leq (1,1))}
      &
      \\
      F(1,1)
      \ar{rr}[below]{F((1,1)\leq (1,2))}
      &&
      F(1,2)
      \end{tikzcd}
      \]
      the morphism $G((1,0)\leq (1,1)))$ is an isomorphism.
    \end{itemize}
    Note that we could have made things simpler -- in light of \cref{3:composition-of-adjoints}, we factor the inclusion $\pphi\mc I\hookrightarrow J$ over the inclusion of the full subcategory $K$ given by
    \[
    \begin{tikzcd}
      (0,1)
      \ar{d}
      \ar{r}
      \com{rd}
      &
      (0,2)
      \ar{d}
      \\
      (1,1)
      \ar{r}
      &
      (1,2)
    \end{tikzcd}
    \]
    as:
    \[
    \begin{tikzcd}[column sep = small]
      I
      \ar[hookrightarrow]{r}[below]{\beta}
      \ar[bend left = 30]{rr}[above]{\pphi}
      &
      K\ar[hookrightarrow]{r}[below]{\alpha}
      & J
    \end{tikzcd}
    \]
    Then $\pphi_{\ast} = \alpha_{\ast}\circ \beta_{\ast}$. Evaluating \kancond for $\beta$ gives again that the square given by $K$ has to be mapped by $G$ to a cartesian square, and evaluating for $\alpha$ gives that the induced maps $G((0,0))\to G((0,1))$ and $G((1,0))\to G((1,1)$ have to be isomorphisms in $\ccat$. Using more diagram chasing for cartesian squares, we get that these two descriptions are indeed equivalent.
\end{example}

\begin{example}
Let $G$ and $H$ be groups and let $\pphi\mc H\to G$ be a group homomorphism.  Denote by $\bg$ respectivley $\bh$ the associated categories, and also by $\pphi\mc \bh \to \bg$ the induced functor:
\[
\begin{tikzcd}[column sep = large]
  \ast_{H}
  \ar[mapsto]{r}
  \ar[out = 240, in = 300, loop, swap]
  &
  \ast_{G}
  \ar[out = 240, in = 300, loop, swap]
  \\
  H
  \ar{r}[below]{\pphi}
  &
  G
\end{tikzcd}
\]
We want to understand how right Kan extensions of functors $F\mc \bh\to  \kvec$, i.e. $H$-representations $V$, look like. As $\kvec$ is complete, we will do this again by checking the condition \kancond.
\begin{enumerate}
  \item Consider first the case $H\sse G$ and assume further for simplicity that $G$ is finite. We already know that we can evaluate it as
  \[
  \pphi_{\ast}F(\ast_G) \cong \lim F|(\ast_G/\pphi)
  \]
  Now the objects of the slice categories are given by tuples
  \[
  \left(
  \ast_H,~f\mc\ast_G\to \pphi(\ast_H)
  \right),
  \]
  which we identify with the set $G$. Now a morphism
  \[
  \left(\ast_G,~\ast_G\xrightarrow{g}\ast_G\right)\to
  \left(\ast_G,~\ast_G\xrightarrow{g'}\ast_G\right)
  \]
  is given by an element $h\in H$ such that $hg = g'$. So we can regard $\ast_G/\pphi$ as the discrete groupoid indexed by the right-cosets of $H$, in which every two elements in the same right-coset are uniquely isomorphic. In particular, a cone $W$ over $F|(\ast_G/\pphi)$ is the same as giving, for the distinct representatives $Hg_j$ of the right-cosets, linear maps $f_{g_j}\mc W\to V$. \par
  Returning to the Kan-extension, we get that on objects
  \[
  \pphi_{\ast}F(\ast_G) = \lim F|(\ast_G/\pphi) = \bigoplus_{Hg_j}V\eqqcolon W,
  \]
  where the direct sum is again indexed by the distinct representatives of the cosets (and we also used that for finite index sets, the product is the same as the direct sum).
  All what we are left with is to do define the $G$-action on $W$, i.e. understand how this limit behaves on morphisms.
  For an element $g\in G$, the induced morphism $\ast_G/\pphi\to \ast_G/\pphi$ is given by the left-action of $G$ on the cosets, i.e. it maps the representative $g_j$ of $Hg_j$ to the representative $gg_j$ of $H(gg_j)$, and thus there is a unique $h\in H$ such that $gg_j = hg_i$ for the chosen representative $g_i$ of $H(gg_j)$.
  So the induced $G$-action on $W$ is given by
  \[
  v_{g_j} \mapsto hv_{g_i}
  \]
  where we regard $v_{g_j}$ as an element of the vector space corresponding to $Hg_i$ on the right hand site.
\end{enumerate}
\end{example}
\lec
