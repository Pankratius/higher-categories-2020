\begin{example}
\leavevmode
\begin{enumerate}

  \item
  Let $I$ be the \emph{walking arrow category}: the category associated to $\powset(\lset 0\rset)$, i.e. the category consisting of two objects and a morphism between them:
  \[
  \ast_0\xrightarrow{f}\ast_1
  \]

  Now functors $F\mc I\to \ccat$ correspond to the choice of a morphism $F(\ast_0)\to F(\ast_1)$ in $\ccat$. For two functors $F,G\mc I\to \ccat$, a natural transformation $\eta\mc F\nta G$ corresponds to two maps $\eta_i\mc F(i)\to G(i)$ for $i=0,1$ such that the diagram
  \[
  \begin{tikzcd}
    F(0)\ar{r}[above]{\eta_0}\ar{d}[left]{F(f)}&
    G(0)\ar{d}[right]{G(f)}\\
    F(1)\ar{r}[below]{\eta_1}&
    G(1)
  \end{tikzcd}
  \]
  commutes. This is nothing else than the choice of a commutative diagram in $\ccat$.
  \item For a group $H$, functors $F,G\mc \bh\to \setcat$ are choices of $H$-sets $F(\ast)$ and $G(\ast)$. Now a natural transformation is a morphism of sets $\eta\mc F(\ast)\to G(\ast)$ such that for all $g\in G$ the diagram
  \[
  \begin{tikzcd}
  F(\ast)\ar{r}[above]{\eta}\ar{d}[left]{F(g)}&
  G(\ast)\ar{d}[right]{G(g)}\ar{d}[right]{G(g)}
  \\
  F(\ast)\ar{r}[below]{\eta}&
  G(\ast)
  \end{tikzcd}
  \]
  commutes, which is the same as giving a $H$-equivariant map from $F(\ast)$ to $G(\ast)$.
\end{enumerate}
\end{example}

\begin{example}[Functor Categories and 2-Categories]
  \leavevmode
  \begin{enumerate}
    \item Let $\ccat$ and $\dcat$ be categories. Then the functors from $\ccat$ to $\dcat$ define a category $\fun(\ccat,\dcat)$ as follows: As objects, we take the set of all functors from $\ccat$ to $\dcat$. Given two functors $F,G$ we further define $\fun(\ccat,\dcat)(F,G)$ to be the set of natural transformations from $F$ to $G$. It remains to define a compostion law -- we do this point-wise: For $\eta\mc F\nta G$ and $\gamma\mc G\nta H$ two natural transformations, we define
    \[
    \gamma\circ \eta \mc F\nta H \defined
    \lset F(x)\xrightarrow{\eta_x}G(x)\xrightarrow{\gamma_x}\rset_{x\in \obj \ccat}.
    \]
    This is also denoted as $\gamma\ast\eta$ and visualized as follows:
    \[
    \begin{tikzcd}[column sep = large]
    \ccat\ar[bend left = 40]{rr}[above]{F}[below, name = U, yshift = -.1ex]{}\ar[bend left = -40]{rr}[below]{H}[above, yshift = -.1ex, name = F]{}\ar{rr}[below,name = D]{}[above, name = s]{}[below, xshift = 2.5ex]{G}&&\dcat
      \arrow[Rightarrow,to path={(U) -- node[label=left:$\eta$] {} (s)}]{}
        \arrow[Rightarrow,to path={(D) -- node[label=left:$\gamma$] {} (F)}]{}
    \end{tikzcd}
    \leadsto
    \begin{tikzcd}[column sep = large]
    \ccat\ar[bend left = 40]{rr}[above]{F}[below, name = U, yshift = -.1ex]{}\ar[bend left = -40]{rr}[below]{H}[above, yshift = -.1ex, name = F]{}&&\dcat
      \arrow[Rightarrow,to path={(U) -- node[label=left:$\gamma\ast\eta$] {} (F)}]{}
    \end{tikzcd}
    \]
    We quickly check that $\gamma\ast\eta$ is indeed a natural transformation: Let $f\mc x\to y$ be a morphism in $\ccat$. Then by naturality of $\eta$, we have
    \[
    \eta_y\circ F(f) = G(f)\circ \eta_x
    \]
    and by naturality of $\gamma$
    \[
    \gamma_y\circ G(f) = H(f)\circ \gamma_x
    \]
    and thus
    \begin{align*}
      \gamma_y \circ \eta_y \circ F(f)&= \gamma_y\circ G(f)\circ \eta_x \\
                                      &= H(f)\circ \gamma_x \circ \eta_x.
    \end{align*}
\item There is the notion of a \emph{2-category}: A 2-category $\tccat$ consits of the following data:
\begin{itemize}
  \item A set $\obj(\tccat)$ of \emph{objects};
  \item For every pair $x,y$ of objects a category $\tccat(x,y)$ of morphisms from $x$ to $y$;
  \item For every object $x$ an object $\id_x\in \tccat(x,y)$ called the \emph{identity morphism};
  \item For every triple of objects $x,y,z$ of objects a functor
  \[
  \mu\mc \tccat(x,y)\times \tccat(y,z)\to \tccat(x,z)
  \]
  called the \emph{composition law},
\end{itemize}
subject to the following conditions:
\begin{enumerate}
  \item For every pair $x,y$ of objects, the functors
  \[
  \mu(-,\id_y)\mc \tccat(x,y)\to \tccat(x,y)
  \]
  and
  \[
  \mu(\id_x,-)\mc \tccat(x,y)\to \tccat(x,y)
  \]
  are the identity functors on $\tccat(x,y)$.
  \item For every 4-tuple $(x,y,z,w)$ of objects in $\tccat$, the diagram of functors
  \[
  \begin{tikzcd}
    \tccat(x,y)\times \tccat(y,z)\times \tccat(z,w)\ar{rr}[above]{\mu\times\id}\ar{d}[left]{\id\times\mu}&&
    \tccat(x,z)\times\tccat(z,w)\ar{d}[right]{\mu}\\
    \tccat(x,y) \times \tccat(y,z)\ar{rr}[below]{\mu}&&
    \tccat(x,w)
  \end{tikzcd}
  \]
  commutes.
\end{enumerate}
  We now want to endow the collection of small categories with the structure of a 2-category $\tcat$: For that, we set $\tcat(\ccat,\dcat)\defined \fun(\ccat,\dcat)$. All that remains is a composition law. For that, we note that given categories, functors and natural transformations as in
  \[
  \begin{tikzcd}
  \ccat
  \ar[bend left = 40]{rr}[above]{F}[below, name = U, yshift = -.1ex]{}
  \ar[bend left = -40]{rr}[below]{G}[above, yshift = -.1ex, name = F]{}&&\dcat
  \ar[bend left = 40]{rr}[above]{H}[below, name = i, yshift = -.1ex]{}
  \ar[bend left = -40]{rr}[below]{I}[above, yshift = -.1ex, name = d]{}&&
  \ecat
    \arrow[Rightarrow,to path={(U) -- node[label=left:$\alpha$] {} (F)}]{}
      \arrow[Rightarrow,to path={(i) -- node[label=left:$\beta$] {} (d)}]{}
  \end{tikzcd}
  \]
  the collection of maps
  \[
  \lset
  H(F(x)) \xrightarrow{H(\alpha_x)}H(G(x))\xrightarrow{\beta_{G(x)}}I(G(x))
  \rset_{x\in \ccat}
  \]
  defines a natural transformation $\beta\circ \alpha\mc H\circ F\nta I\circ G$.
  \end{enumerate}
\end{example}

\begin{mrem}
  \leavevmode
  \begin{enumerate}
    \item Every \qq{ordinary} category $\ccat$ can be regarded as a 2-category $\tccat$ as follows: We set $\obj \tccat  = \obj \ccat$ and for objects $x,y\in \ccat$, we define $\tccat(x,y)$ to be the discrete category on the morphisms between $x$ and $y$ (i.e. the objects of $\tccat(x,y)$ are the morphisms $x\to y$ in $\ccat$ and there are only the identity morphisms.).
    \item Let $\tccat$ be a 2-category with exactly one object $\ast$. The resulting category of morphisms $\tccat(\ast,\ast)$ is called a \emph{monoidal category}. We will probably encounter them later again.
  \end{enumerate}
\end{mrem}

\subsection{Equivalences and adjunctions}
\begin{defn}
A functor $F\mc \ccat\to \dcat$ is callend an \emph{equivalence of categories} or just an \emph{equivalence} if there is a functor $G\mc \dcat\to\ccat$ such that there are isomorphisms
\[
\alpha\mc F\circ G \nta \id_{\dcat}\text{ and }\beta\mc G\circ F\nta \id_{\ccat}
\]
in the functor category $\fun(\ccat,\dcat)$. These isomorphisms are called \emph{natural isomorphisms}.
\end{defn}
\begin{mrem}
  A natural transformation $\eta\mc F\nta G$ between functors $F,G\mc \ccat\to \dcat$ is a natural isomorphism if and only if all maps
  $\eta_x\mc F(x)\to G(x)$ are isomorphisms in $\dcat$.
\end{mrem}

\begin{defn}
  An \emph{adjunction} between two categories $\ccat$ and $\dcat$ consists of the following data:
  \begin{itemize}
    \item Functors $F\mc \ccat\to\dcat$ and $G\mc \dcat\to \ccat$ called the \emph{left} and \emph{right adjoint} respectivley;
    \item A natural transformation $\eta\mc F\circ G\nta \id_{\dcat}$ called the \emph{counit};
    \item A natural transformation $\eepsilon\mc \id_{\ccat}\nta G\circ F$ called the \emph{unit};
  \end{itemize}
  subject to the following two conditions, which are called the \emph{triangle identities}:
  \begin{enumerate}
    \item The diagram of natural transformations
    \[
    \begin{tikzcd}
      F\ar[Rightarrow]{r}[above]{F\circ \eepsilon}\ar[equal]{dr}&F\circ G\circ F\ar[Rightarrow]{d}[right]{\eta\circ F}\\
      &F
    \end{tikzcd}
    \]
    commutes.
    \item The diagram of natural transformations
    \[
    \begin{tikzcd}
      G\ar[equal]{dr}\ar[Rightarrow]{r}[above]{\eepsilon\circ G}&G\circ F\circ G\ar[Rightarrow]{d}[right]{G\circ \eta}\\
      &G
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
\end{defn}
We will typically denote adjunctions by $F\mc \ccat\leftrightarrow \dcat \mcc G$, leaving the unit and counit implicit.

\begin{prop}\label{3:composition-of-adjoints}
\itodo{composition of adjoints}
\end{prop}
\begin{defn}
  A functor $F\mc \ccat\to\dcat$ is called
  \begin{enumerate}
    \item \emph{full} respectivley \emph{faithful} if, for every pair of $x,y$ of objects in $\ccat$, the map
    \[
    \ccat(x,y)\to \dcat(F(x),F(y))\]
    is surjective respectivley injective.
    \item fully faithful if it is full and faithful.
    \item essentially surjective if, for every $y\in \dcat$, there exists a $x\in \ccat$ such that $F(x)\cong y$ in $\dcat$.
  \end{enumerate}
\end{defn}
\begin{lem}
  Let $(F,G,\eta,\eepsilon)$ be an adjunction between categories $\ccat$ and $\dcat$.
  \begin{enumerate}
    \item $F$ is fully faithful if and only if the unit is a natural isomorphism.
    \item $G$ is fully faithful if and only if the counit is a natural isomorphism.
  \end{enumerate}
\end{lem}
\begin{theorem}
  Let $F\mc \ccat\to\dcat$ be a functor. Then the following are equivalent:
  \begin{enumerate}
    \item $F$ is an equivalence.
    \item $F$ is fully faithful and essentially surjective.
    \item $F$ is part of an adjunction $(F,G,\eta,\eepsilon)$ for suitable choices of units and counits that are natural isomorphisms.
  \end{enumerate}
\end{theorem}
\lec
