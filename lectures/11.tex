\begin{prop}
  Let $\ccat$ be a small category.
  \begin{enumerate}
    \item Every inner horn $\Lambda_i^n\to \nerve(\ccat)$ can be filled in a unique way, i.e. there is a unique morphism $\Delta^n\to \nerve(\ccat)$ such that the diagram
    \[
    \begin{tikzcd}
      \Lambda_i^n
      \ar{r}
      \ar[hookrightarrow]{d}
      &
      \nerve(\ccat)
      \\
      \Delta^n
      \ar[dashed]{ur}[below right]{\exists !}
      &
      {}
    \end{tikzcd}
    \]
    \item The simplicial set $\nerve(\ccat)$ is a Kan complex if and only if $\ccat$ is a groupoid.
  \end{enumerate}
\end{prop}
\begin{proof}
  \leavevmode
  \begin{enumerate}
      \item We first consider the cases $n=2,3$: For $n=2$, a map $\Lambda_1^2$ corresponds to the choice of objects $x_0,x_1,x_2\in \ccat$ and morphisms $f\mc x_0\to x_1,g\mc x_1\to x_2$:
      \[
      \begin{tikzcd}
        &
        x_1
        \ar{rd}[above right]{g}
        &
        \\
        x_0
        \ar{ur}[above left]{f}
        &
        &
        x_2
        \end{tikzcd}
        \]
        Now an extension to $\Delta^2$ corresponds to a choice of a morphism $x_0\to x_2$ that makes the above diagram commutative -- there's indeed a unique choice, namley $g\circ f$:
        \[
        \begin{tikzcd}
          &
          x_1
          \ar{rd}[above right]{g}
          \com{d}
          &
          \\
          x_0
          \ar{ur}[above left]{f}
          \ar[dashed]{rr}[below]{g\circ f}
          &
          {}
          &
          x_2
          \end{tikzcd}
        \]
        For $n=3$, we first note that a $3$-simplex in $\nerve(\ccat)$ corresponds of a chain of composable morphisms in $\ccat$:
        \[
        x_0\to x_1\to x_2\to x_3
        \]
        Now the data of a map $\Lambda_1^3\to \nerve(\ccat)$ corresponds to commutative diagrams of the form
        \[
        \begin{tikzcd}[column sep = small]
          &
          x_1
          \ar{rd}[above right]{g}
          \com{d}
          &
          \\
          x_0
          \ar{ur}[above left]{f}
          \ar{rr}[below]{g\circ f}
          &
          {}
          &
          x_2
          \end{tikzcd},~
          \begin{tikzcd}[column sep = small]
            &
            x_2
            \ar{rd}[above right]{h}
            \com{d}
            &
            \\
            x_1
            \ar{ur}[above left]{g}
            \ar{rr}[below]{h\circ g}
            &
            {}
            &
            x_2
            \end{tikzcd},~
            \begin{tikzcd}[column sep = small]
              &
              x_1
              \ar{rd}[above right]{h\circ g}
              \com{d}
              &
              \\
              x_0
              \ar{ur}[above left]{f}
              \ar{rr}[below]{(h\circ g)\circ f}
              &
              {}
              &
              x_3
              \end{tikzcd},
        \]
        which we visualize as grey faces of the followig tetrahedron:
        \begin{figure}[h]
          \centering
          \input{lectures/11tetrahedron}
        \end{figure}\par
        Note that the lower face is not filled (we indicate by filling it in red), while the others are, to account for the commutativity of the above diagrams. Now an extension corresponds precisley to a filling of the red diagram -- but we already have this, since the diagram
        \[
        \begin{tikzcd}[column sep = small]
          &
          x_2
          \ar{rd}[above right]{h}
          \com{d}
          &
          \\
          x_0
          \ar{ur}[above left]{g\circ f}
          \ar{rr}[below]{(h\circ g)\circ f}
          &
          {}
          &
          x_3
          \end{tikzcd}
          \]
          commutes by the associtativity of the composition in $\ccat$. The same, just for interchanged bracketing, happens for the horn $\Lambda_3^2$.\par
          Now the idea for $n>3$ is that there are higher-dimensional coherence constraints -- we would always just check commutativity of certain triangles, which always amounts to a different bracketing of compositions, which are all the same, by associtativity. Putting this more formal, consider the inclusion
          \[
          j\mc \IDelta_{\leq 2}\hookrightarrow \IDelta
          \]
          of the full subcategory $\IDelta_{\leq 2}$ with objects $\left[0\right],\left[1\right]$ and $\left[2\right]$. This induces the restriction functor
          \begin{align*}
          \sk_2\mc \sset&\to \setcat_{\IDelta_{\leq 2}}\\
          \left(\op{\IDelta}\xrightarrow{F} \setcat\right)&
          \mapsto
          \left(\op{\IDelta_{\leq 2}}\xhookrightarrow{j}\op{\Delta}\xrightarrow{f}\setcat\right)
          \end{align*}
          We claim that there is a natural isomorphism:
          \[
          \sset(K,\nerve(\ccat)) \cong \sset(\sk_2(K),\sk_2(\nerve(\ccat)))
          \]

  \end{enumerate}
\end{proof}

\begin{defn}
  A morphism $p\mc K\to S$ is called a \emph{Kan fibration} if, for every $n>0$ and $0\leq i\leq n$, and every solid commutative diagram
  \[
  \begin{tikzcd}
    \Lambda_i^n
    \ar[hookrightarrow]{d}
    \ar{r}
    &
    K
    \ar{d}
    \\
    \Delta^n
    \ar{r}
    &
    S
  \end{tikzcd}
  \]
  there exists a morphism $\Delta^n\to K$ such that the whole diagram
  \[
  \begin{tikzcd}
    \Lambda_i^n
    \ar[hookrightarrow]{d}
    \ar{r}
    &
    K
    \ar{d}
    \\
    \Delta^n
    \ar{r}
    &
    S
  \end{tikzcd}
  \]
  is commutative.
\end{defn}
\begin{example}
  For a simplicial set $K$, the unique morphism $K\to \Delta^0$ is a Kan fibration if and only if $K$ is a Kan complex.
\end{example}

\subsection{The small object argument}
In this subsection, $\ccat$ denotes a category with all small colimits.
\begin{defn}
  A set $S$ of morphisms in $\ccat$ is called \emph{saturated} if the following conditions are satisfied:
  \begin{enumerate}
    \item[(S1)] $S$ contains all isomorphisms;
    \item[(S2)] $S$ is closed under pushout: Given a cocartesian square
    \[
    \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i}
    \ar[phantom]{dr}{\ulcorner}
    &
    A'
    \ar{d}[right]{i'}
    \\
    B
    \ar{r}
    &
    B'
    \end{tikzcd}
    \]
    with $i\in S$ then $i'\in S$ holds too.
    \item[(S3)] $S$ is closed under retracts: Given a commuative diagram
    \[
    \begin{tikzcd}
    A'
    \ar{r}
    \ar{d}[left]{i'}
    \ar[bend left = 30]{rr}[above]{\id}
    &
    A
    \ar{r}
    \ar{d}
    &
    A'
    \ar{d}[right]{i'}
    \\
    B'
    \ar{r}
    \ar[bend right = 30]{rr}[above]{\id}
    &
    B
    \ar{r}
    &
    B'
    \end{tikzcd}
    \]
    with $i\in S$ then $i'\in S$ holds to.
    \item[(S4)] $S$ is closed under countable composition: Given a chain of morphisms
    \[
    A_0\xrightarrow{i_0}A_1\xrightarrow{i_1}A_2\xrightarrow{i_2}\ldots
    \]
    in $S$, indexed by the natural numbers, then the canonical map
    \[
    A_0\to \colim_{\nn}A_i \eqcolon A_{\infty}
    \]
    is in $S$ too.
    \item[(S5)] $S$ is closed under small coproducts: Let $\lset i_j\mc A_j\to B_j\rset_{j\in J}$ be a small subset of $S$. Then the coproduct
    \[
    \coprod_{j\in J}A_j \xrightarrow{\coprod i_j}\coprod_{j\in J}B_j
    \]
    is in $S$ too.
  \end{enumerate}
\end{defn}
\begin{remdef}
  The intersection of saturated sets is saturated. For any set $M$ of morphisms in $\ccat$, we define the \emph{saturated hull of $M$} to be the intersection of all saturated sets which contain $M$, which is the smallest saturated set that contains $M$.
\end{remdef}

\begin{defn}
\leavevmode
\begin{enumerate}
  \item  Let $i\mc A\to B$ and $p\mc K\to S$ be morphisms in $\ccat$. We say that
    \begin{itemize}
      \item $i$ has the left lifting property with respect to $p$; or equvialently
      \item $p$ has the right lifting property with respect to $i$
    \end{itemize}
    if every lifting problem of the form
    \[
    \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i}
    &
    K
    \ar{d}[right]{p}
    \\
    B
    \ar[dashed]{ur}[below right]{?}
    \ar{r}
    &
    S
    \end{tikzcd}
    \]
    has a solution.
    \item Given a set $M$ of morphisms in $\ccat$, we define
    \begin{itemize}
    \item $M\rlp$ to be the set of morphisms in $\ccat$ which have the right lifting property with respect to every morphism in $M$; and analogously
    \item $\llp M$ to be the set of morphisms in $\ccat$ which have the left lifting property with respect to every morphism in $M$.
    \end{itemize}
  \end{enumerate}
\end{defn}

\begin{example}
  Let $\ccat=\sset$ and let $M$ be the set of horn inclusions. Then $M\rlp$ are by definition the Kan fibrations.
\end{example}
\begin{prop}
  Let $M$ be a set of morphisms then the set $\llp M$ is saturated. In particular,
    \[
    \overline{M} \sse \llp(M\rlp)
    \]
  holds.
\end{prop}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item[(S1)] Let $i$ be an isomorphism in $\ccat$, then $i$ has the left-lifting property with respect to any morphism in $M$, as the diagram
    \[
    \begin{tikzcd}
      A
      \ar{r}[above]{f}
      \ar{d}[left]{i}
      &
      K
      \ar{d}
      \\
      B
      \ar[dashed]{ur}[below right]{f\circ i^{-1}}
      \ar{r}
      &
      S
    \end{tikzcd}
    \]
    is commutative.
    \item[(S2)] Let
    \[
    \begin{tikzcd}
    A
    \ar{r}
    \ar{d}[left]{i}
    \ar[phantom]{dr}{\ulcorner}
    &
    A'
    \ar{d}[right]{i'}
    \\
    B
    \ar{r}
    &
    B'
    \end{tikzcd}
    \]
    be a cocartesian square, with $i\in \llp M$. We want that $i'$ also has the left lifting property with respect to $M$. So we have to solve lifting problems of the form
    \[
    \begin{tikzcd}
      A'
      \ar{r}
      \ar{d}[left]{i'}
      &
      K
      \ar{d}[right]{\in M}
      \\
      B'
      \ar[dashed]{ur}[below right]{?}
      \ar{r}
      &
      S
    \end{tikzcd}
    \]
    For that, we augment to the left by the cocartesian square and obtain a morphism $B\to K$ that makes the outer diagram commute, since $i\in \llp M$:
    \[
    \begin{tikzcd}
      A
      \ar{r}
      \ar{d}[left]{i}
      \ar[phantom]{dr}{\ulcorner}
      &
      A'
      \ar{r}
      \ar{d}[left]{i'}
      &
      K
      \ar{d}[right]{\in M}
      \\
      B
      \ar{r}
      \ar[dashed]{urr}
      &
      B'
      \ar{r}
      &
      S
    \end{tikzcd}
    \]
    Then by the univeral property of the pushout, there is a unique morphism $B'\to K$ which gives a solution to the lifting problem.
    \item[(S3)] Let $i'$ be a retract of an element $i\in \llp M$. To show that $i'$ also has the left lifting property with respect to $M$, we proceed similarly as in the above case by augmenting the lifting problem to the left by the retract diagram:
    \[
    \begin{tikzcd}
      A'
      \ar{r}
      \ar{d}
      &
      A
      \ar{r}
      \ar{d}[left]{i}
      &
      A'
      \ar{d}[left]{i'}
      \ar{r}
      &
      K
      \ar{d}
      \\
      B'
      \ar{r}[below]{g}
      &
      B
      \ar{r}[below]{f}
      &
      B'
      \ar{r}
      &
      S
    \end{tikzcd}
    \]
    Now the lifting problem
    \[
    \begin{tikzcd}
      A
      \ar{r}
      \ar{d}[left]{i}
      &
      A'
      \ar{r}
      &
      K
      \ar{d}
      \\
      B
      \ar{r}[below]{f}
      \ar[dashed]{urr}
      &
      B'
      \ar{r}
      &
      S
    \end{tikzcd}
    \]
    has a solution, since $i\in \llp M$. Precomposing with $g$ then gives the desired lift.
    \item
    Let
    \[
    A_0\xrightarrow{i_0}A_1\xrightarrow{i_1}A_2\to\ldots
    \]
    be an $\nn$-index series of morphisms $i_j\in \llp M$. We want that the morphism $i_{\infty}\mc A_0\to A_{\infty}$ is in $\llp M$ too. For that, we take the colimit of the solutions of the lifting problems, as indicated in the following diagram
    \[
    \begin{tikzcd}[column sep = huge]
    A_0
    \ar{r}
    \ar{d}
    \ar[bend right = 25]{dddd}[left]{i_{\infty}}
    &
    K
    \ar{dddd}
    \\
    A_1
    \ar[dashed]{ur}
    \ar{d}
    &
    \\
    A_2
    \ar[dashed]{uur}
    \ar{d}
    &
    \\
    \vdots
    &
    \\
    A_{\infty}
    \ar[dashed]{uuuur}
    \ar{r}
    &
    S
    \end{tikzcd}
    \]
    \item[(S5)] This is similar to the above.
  \end{enumerate}
\end{proof}

\lec
