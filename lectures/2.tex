\section{Category Theory}

\subsection{Categories}
\begin{defn}
  A \emph{category}\index{category} $\ccat$ consists of the following datum
  \begin{enumerate}
    \item a set $\obj\ccat$ called the \emph{objects}\index{category!objects} of $\ccat$,
    \item for every pair of objects $(x,y)$ of objects, a set $\ccat(x,y)$ called the \emph{morphisms from $x$ to $y$}\index{category!morphisms},
    \item for every object $x$, a morphism $\id_x\in \ccat(x,x)$ called the \emph{identity at $x$};
    \item for every triple $(x,y,z)$ of objects, a map
    \[
    \circ \mc \ccat(x,y)\times \ccat(y,z)\to \ccat(x,z),~(f,g)\mapsto g\circ f
    \]
    called the \emph{composition law};
  \end{enumerate}
  such that the following conditions are satisfied:
  \begin{itemize}
    \item \emph{Unitality}\index{unitality}: for all morphisms $f\in \ccat(x,y)$ it holds that $f\circ \id_x = \id_y\circ f = f$;
    \item \emph{Associativity:} for all $f\in \ccat(x,y)$, $g\in \ccat(y,z)$ and $h\in \ccat(z,w)$ it holds that
    \[
    h\circ (g\circ f) = (h\circ g)\circ f.
    \]
  \end{itemize}
\end{defn}
\begin{notation}
  For a morphism $f\in \ccat(x,y)$ we also write pictorially $f\mc x\to y$ or $x\xrightarrow{f}y$.
\end{notation}

\begin{mdefn}
  A morphism $f\mc x\to y$ in $\ccat$ is called an \emph{isomorphism} if there is a morphism $g\mc y\to x$ such that
  \[
  g\circ f = \id_{x}~\text{and}~f\circ g = \id_y.
  \]
\end{mdefn}

\begin{example}\label{1:categories-examples}
  \leavevmode
  \begin{enumerate}
    \item Every partially ordered set (or poset) $(P,\leq)$ may be considered as a category $\pcat$ as follows
    \begin{itemize}
      \item as objects, we set $\obj \pcat \defined P$, i.e. just the set $P$ itself;
      \item for $i,j\in P$, we set:
      \[
      \pcat(i,j)\defined \begin{cases}
        \lset \ast\rset &\text{if }i\leq j\\
        \emptyset&\text{else}
      \end{cases}
      \]
    \end{itemize}
    Now the existence of an identity element is ensured since $\leq$ is reflexive, and the properties of the composition follow from the transitivity of $\leq$. \par
    Note that we did not need the antisymmetry of $\leq$ for $\pcat$ to be a category. In general, a category is called \emph{thin} if, given any two morphisms $f,g\mc x\to y$ it follows that $f$ and $g$ are equal. So $\pcat$ is an example of a thin category. We will see later, after we have established the right formalisms for comparing categories, that in fact every thin category arrises in this way.
    \item Every group $G$ defines a category $\bg$\index{$\bg$} with a single object $\ast$ and $\bg(\ast,\ast)\defined G$, where the composition is given by the group multiplication. Note that once again we do not need a property of $G$ in order for $\bg$ to be a category -- in this case that every element in $G$ is invertible. So we can define for any monoid $M$ a category $\bm$, again with a single object $\ast$ and $\bm(\ast,\ast)\defined M$.
    \item Let $X$ be a topological space. Define the category $\pl X$ with objects given by the elements of $X$ and
    \[
    \pl X(x,y)\defined \lset \text{homotopy classes of continous paths from }x\text{ to }y
    \rset
    \]
    This is called the \emph{fundamental groupoid of $X$}.
    \item One of the most important categories in this lecture will be the simplex category $\IDelta$. The objects of $\IDelta$ are given by the set of partially ordered sets $\npcat$ for $n\geq 0$ and the morphisms are given by order preserving maps, i.e.
    \[
    \IDelta
    (\npcat,\mpcat)\defined \lset \text{order preserving maps }n\to m
    \rset
    \]
  \end{enumerate}
\end{example}

We would like to further define and study categories whose objects are given by sets with additional structure (like (abelian) groups, rings, vector spaces,...). The problem is that the notion of a \glqq set of sets\grqq{} is logically flawed, as illustrated by Russel's paradox\footnote{It should be noted at this point that the author of these notes has \emph{no} background on set theory.} We will fix this by working with an axiomatic framework for set theory called \emph{ZFCU}.
\begin{defn}
  A \emph{universe} is a non-empty set $U$ of sets with the following properties:
  \begin{enumerate}
    \item[U1] If $x\in U$ and $y\in x$ then $y\in U$.
    \item[U2] If $x,y\in U$ then $\lset x,y\rset\in U$.
    \item[U3] If $x\in U$ then $\powset(x)\in U$.
    \item[U4] If $I\in U$ and $\lset x_i\rset_{i\in I}$ is a family of elements $x_i\in U$ then $\bigcup_{i\in I}x_i\in U$.
  \end{enumerate}
\end{defn}

\begin{prop}
  Any universe $U$ also satisfies the following properties:
  \begin{enumerate}
    \item $x\in U\implies x\sse U$.
    \item If $y\in U$ and $x\sse y$, then $x\in U$.
    \item $\emptyset \in U$.
    \item If $x,y\in U$ then also $(x,y)\defined \lset x,\lset x,y\rset\rset\in U$.
    \item If $x,y\in U$ then both $x\cup y$ and $x\times y\in U$.
    \item If $I\in U$ and $\lset x_i\rset_{i\in I}$ is a collection of objects $x_i\in U$ then
    \[
    \prod_{i\in I}x_i,~\coprod_{i\in I}x_i,~\bigcap_{i\in I}x_i\in U
    \]
    \item If $x\in U$ then $x\cup \lset x\rset \in U$.
    \item $\nn\sse U$.
  \end{enumerate}
\end{prop}
\begin{rem}
  It is not automatic that $\nn\in U$. If this is the case the universe is called \emph{infinite}. In this case, $\zz,~\rationals,~\rr,~\cc\in U$ follows.
\end{rem}
It is however not possible to prove the existence of inifinte universes within ZFC. So we impose the following axiom: \emph{For every set $X$ there exists a universe $U$ such that $X\in U$}.
From now on, we fix an infinte universe $U$ and introduce the following terminology:
\begin{defn}
  A set $X$ is called:
  \begin{enumerate}
    \item \emph{small} if $X\in U$.
    \item \emph{a class} if $X\sse U$.
    \item \emph{large} if $X\ssne U$.
  \end{enumerate}
\end{defn}
\begin{example}
  Within this context, we can introduce the following categories:
  \begin{enumerate}
    \item The category $\setcat$ of small sets, with objects given by the elements of $U$ and morphisms given by maps of sets.
    \item The category $\groupcat$ of small groups, i.e. groups whose underlying set is small and group homomorphisms as morphisms.
    \item The category $\topcat$ of small topological spaces, i.e. topological spaces whose underlying set is small and morphisms given by continous maps.
  \end{enumerate}
\end{example}

\begin{example}
  Besides these above examples, there are also two important ways to obtain new categories from given ones:
  \begin{enumerate}
    \item For a category $\ccat$, we denote by $\op{\ccat}$ the category with the same objects as $\ccat$, but
    \[
    \op{\ccat}(x,y)\defined \ccat(y,x).
    \]
    This is called the \emph{opposite category of $\ccat$}\index{opposite! of a category}\index{category! opposite of}
    \item If $\ccat$ and $\dcat$ are categories, their \emph{product}\index{product! of a category}
    is defined the be the category $\ccat\times \dcat$ with
    \[
    \obj(\ccat\times \dcat)\defined \obj(\ccat)\times \obj(\dcat)
    \]
    and morphisms
    \[
    \ccat\times \dcat((x,y),(x',y'))\defined
    \ccat(x,x')\times \dcat(y,y').
    \]
  \end{enumerate}
\end{example}

\begin{mexample}
  Let $(P_1,\leq_1),(P_2,\leq_2)$ be posets, and $\pcat_1,~\pcat_2$ the associated categories as in \cref{1:categories-examples}. Then the product $\pcat_1\times \pcat$ is the same as the category associated to the set $P_1\times P_2$ with the usual product order (i.e. $(a,b)\leq (a',b')$ if and only if $a\leq a'$ and $b\leq b'$).
\end{mexample}


\subsection{Functors and Natural Transformations}
\begin{defn}
  Let $\ccat$, $\dcat$ be categories. A functor $F\mc \ccat\to \dcat$ consists of:
  \begin{enumerate}
    \item A map of sets $F\mc \obj\ccat\to\obj \dcat$;
    \item For every pair $x,y\in \ccat$, a map of sets
    \[
    F\mc \ccat(x,y)\to \dcat(F(x),F(y));
    \]
  \end{enumerate}
  such that the following conditions are satisfied
  \begin{enumerate}
  \item For every $x\in \ccat$ it holds that $F(\id_x) = \id_{F(x)}$.
  \item For every pair of morphism $x\xrightarrow{f}y\xrightarrow{g}z$ of morphisms if $\ccat$ it holds that
  \[
  F(g\circ f) = F(g)\circ F(f).
  \]
  \end{enumerate}
\end{defn}

\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Consider the power set of $\lset 0,1\rset$, $\powset(\lset 0,1\rset)$, which we regard as a partially ordered set by inclusion. Then the construction from \cref{1:categories-examples} gives a category $\pcat_{\lset 0,1\rset }$, and a functor $\pcat_{\lset 0,1\rset}\to \ccat$ amounts to giving a commutative square in $\ccat$ of the form:
    \[
    \begin{tikzcd}
      F(\emptyset)\ar{r}\ar{d}&
      F(\lset 0\rset)\ar{d}\\
      F(\lset 1\rset)\ar{r}&
      F(\lset 0,1\rset)
    \end{tikzcd}
    \]

  \item Let $M$ be a monoid and $\ccat$ a category. Then a functor
  \[
  F\mc \bm \to \ccat
  \]
  corresponds to the choice of an object $F(\ast)$ in $\ccat$ and a subset of morphisms $\ccat(x,x)$ such that $F(m_1)\circ F(m_2) = F(g_1\circ g_2)$. If $M = G$ is actually a group, then all the morphisms $F(g)$ are invertible. In this way, we get an abstract notion of a \emph{G-action} on an object $X\in \ccat$, which we define to be a functor $F\mc\bg\to \ccat$ with $F(\ast) = X$.
  \end{enumerate}
  \item One of the most important set of functors in this lecture will be functors $\op{\IDelta}\to \setcat$, which are called \emph{simplicial sets}.
\end{example}

\begin{defn}
  Let $\ccat$, $\dcat$ be categories and $F\mc \ccat\to\dcat$ be a functor. A \emph{natural transformation} $\eta\mc F\nta G$ from $F$ to $G$ consists of a collection of morphisms $\eta_x\mc F(x)\to G(x)$ in $\dcat$ such that for every morphism $f\mc x\to y$ in $\ccat$ the diagram
  \[
  \begin{tikzcd}
    F(x)\ar{r}[above]{\eta_x}\ar{d}[left]{F(f)}&
    G(x)\ar{d}[right]{G(f)}\\
    F(y)\ar{r}[below]{\eta_y}&
    G(y)
  \end{tikzcd}
  \]
  commutes.
\end{defn}
\lec
