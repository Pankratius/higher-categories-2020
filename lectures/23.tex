\begin{example}
  Let $\wtilde{\catcat_{\infty}}$ be the simplicial subcategory of $\sset$, consisting of $\infty$-categories with mapping simplicial sets given by
  \[
  \fun(\ccat,\dcat)^{\simeq} \sse
  \fun(\ccat,\dcat),
  \]
  which are animas. Then
  \[
  \catcat_{\infty}\defined \hnerve(\wtilde{\catcat_\infty})
  \]
  is called the \emph{$\infty$-category of (small) $\infty$-categories}.
\end{example}

\subsection{Overcategories and Undercategories}
\begin{construction}
  Let $\ccat$ and $\ccat'$ be ordinary categories. We introduce the \emph{join} $\ccat\ast\ccat'$ to be the category with:
  \begin{itemize}
    \item Objects given by the disjoint union $\obj \ccat \amalg \obj \ccat'$.
    \item Morphisms given by
    \[
    \ccat \amalg \ccat'(x,y)
    \defined
    \begin{cases}
      \ccat(x,y)&\text{if }x,y\in \ccat \\
      \ccat'(x,y)&\text{if }x,y\in \ccat'\\
      \lset \ast\rset& \text{if }x\in \ccat \text{ and }y\in \ccat'\\
      \emptyset&\text{if }x\in \ccat' \text{ and }y\in \ccat
    \end{cases}
    .
    \]
  \end{itemize}
  \end{construction}
  \begin{example}
    We have $\mpcat\ast\npcat = \left[m+n+1\right]$.
  \end{example}
  We now want to generalize the join-construction to $\infty$-cats.
  \begin{construction}
    Let $K,K'$ be simplicial sets. We then define a simplicial set $K\ast K'$, by setting for a nonempty finite linearly ordered set $I$:
    \[
    K\ast K'(I)
    \defined
    \coprod_{I = J\amalg J'}K(J)\times K'(J'),
    \]
    where the union runs over all partitions $I = J\amalg J'$ with $J\sse J'$ and possibly $J = \emptyset$, $J' = \emptyset$.
  \end{construction}
  \begin{prop}
    Let $\ccat, \ccat'$ be ordinary categories. Then we have:
    \[
    \nerve(\ccat)\ast\nerve(\ccat') = \nerve(\ccat\times\ccat')
    \]
    In particular,
    \[
    \Delta^n\amalg \Delta^m \cong  \Delta^{n+m+1}
    \]
  \end{prop}
  \begin{rem}
    The join-construction is functorial in both coordinates.
    Further, via the natural inclusion $K\hookrightarrow K\ast K'$, we obtain a functor
    \[
    \sset \to \left(\sset\right)_{K/},~K'\mapsto K\hookrightarrow K\ast K',
    \]
    which commutes with colimits. \par
    This statement, together with the analogous statement in the first variable and the formular $\Delta^n\ast\Delta^m=\Delta^{n+m+1}$ uniquely determines the join functor.
  \end{rem}
  \begin{prop}
    Let $\ccat,\ccat'$ be $\infty$-categories. Then the join $\ccat\ast\ccat'$ is an $\infty$-category too.
  \end{prop}

  \begin{defn}
  \leavevmode
  \begin{enumerate}
    \item Let $K$ be a simplical set. Then $K\triangleright\defined K\ast \Delta^0$ and $K\triangleleft\defined \Delta^0\ast K$ are called the \emph{right cone} and \emph{left cone} of $K$, respectivley.
    \item Let $K,S$ be simplicial sets and let $p\mc K\to S$ be a morphism. We introduce the simplicial set $S_{p/}$ via
    \[
    (S\un{p})_n \defined
    \lset
    f\mc K\ast\Delta^n\to S\ssp \restrict{f}{K} = p
    \rset
    \]
    and dually the simplicial set $S_{/p}$ via
    \[
    (S\ov{p})_n\defined
    \lset
    f\mc \Delta^n\ast K\to S\ssp
    \restrict{f}{K} = p
    \rset.
    \]
    \end{enumerate}
  \end{defn}

  \begin{prop}
    Let $K$ be a simplicial set, $\ccat$ an $\infty$-category and $p\mc K\to \ccat$ a $K$-diagram. Then the simplicial sets $\ccat\ov{p}$ and $\ccat\un{p}$ are again $\infty$-categories.\par
    In this case, we call $\ccat\un{p}$ the \emph{$\infty$-category of cones under $p$} and $\ccat\ov{p}$ the \emph{$\infty$-category of cones over $p$}.
  \end{prop}

  \section{Limits and Colimits}
  \begin{defn}
    Let $\ccat$ be an $\infty$-category and $x\in \ccat_0$ an object. We say that $x$ is an \emph{initial object in $\ccat$} if the map
    \[
    \ccat\un{x}\to \ccat,~
    \left(f\mc x\ast \Delta \to \ccat\right)
    \mapsto
    \restrict{f}{\Delta^n}
    \]
    is a trivial Kan fibration. Dually, $x\in \ccat_0$ is called a \emph{final object} if the map $\ccat\ov{x}\to \ccat$ is a trivial Kan fibration.
  \end{defn}
  \begin{mrem}
    Two clarification are in order: By $\ccat\un{x}$, we mean $\ccat\un{x}$ for the constant morphism $x\mc \Delta^0\to \ccat$, and by $\restrict{f}{\Delta^n}$ we mean the composition with the canonical inclusion: $\Delta^n\hookrightarrow x\ast \Delta^n\xrightarrow{f} \ccat$.
  \end{mrem}

  \lec
