\begin{numtext}\label{18:lhom-composition}
  Given morphisms $f,g\mc A\to B$ and a morphism $h\mc B\to C$, any left homotopy $H\mc f\lhom g$ gives rise to a left homotopy $h\circ H\mc hf\lhom hg$. However, we do not know if the same holds for precomposition, i.e. if $h'\mc A'\to A$ is another morphism, can we conclude that $fh'$ and $gh'$ are again homotopically related via $h'$? To answer this question, we introduce the dual notions to cylinder objects and left homotopies:
\end{numtext}
\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item A \emph{path object} for an object $B\in \ccat$ is an object $P\in \ccat$ together with maps $s\mc B\to P$ and $p\mc P\to B\times B$, such that:
    \begin{itemize}
      \item The morphism $s\mc B\to P$ is a trivial cofibration.
      \item The morphism $p = (p_0,p_1)\mc P\to B\times B$ is a fibration.
      \item The following diagram commutes:
      \[
      \begin{tikzcd}
        &
        P
        \ar{d}[right]{p}
        \\
        B
        \ar{r}[below]{\text{diag}}
        \ar{ur}[above left]{s}
        &
        B\times B
      \end{tikzcd}
      \]
    \end{itemize}
    \item A \emph{right homotopy} between morphisms $f,g\mc A\to B$ consists of:
    \begin{itemize}
      \item A path object $(P,s,p)$ for $B$.
      \item A morphism $H\mc A\to P$ such that the following diagram commutes:
      \[
      \begin{tikzcd}
        &
        P
        \ar{d}[right]{p}
        \\
        A
        \ar{ur}[above left]{H}
        \ar{r}[below]{(f,g)}
        &
        B\times B
      \end{tikzcd}
      \]
      If there is a right homotopy between two morphisms $f,g$, then we denote this as $f\rhom g$.
    \end{itemize}
  \end{enumerate}
\end{defn}
\begin{prop}
  \leavevmode
  \begin{enumerate}
    \item Let $B$ be a fibrant object and $(P,s,p)$ a path object for $B$. Then the morphisms $p_0$ and $p_1$ are trivial fibrations.
    \item
    Let $B$ be a fibrant object. then the relation $f\rhom g$ defines an equivalence relation on $\ccat(A,B)$.
  \end{enumerate}
\end{prop}
\begin{proof}
  This is analogous to \cref{17:lhom-equivalence}.

\end{proof}
\begin{numtext}
  We now observe that the dual statements of \cref{18:lhom-composition} hold: If $f$ and $g$ are right homotopic, then $fh'$ and $gh'$ are again right homotopic, but we cannot at the moment say anything about $hf$ and $hg$.
\end{numtext}


\begin{lem}
  Let $\ccat$ be a model category and let $A$ be a cofibrant object. Suppose $f,g\mc A\to B$ are left homotopic morphisms. Then, given any path object $P$ for $B$, then there exists a right homotopy $H'\mc A\to P$ between $f$ and $g$.
\end{lem}
\begin{proof}
  Assume that the left homotopy between $f$ and $g$ is given by
  \[
  \begin{tikzcd}
    A\amalg A
    \ar{d}[left]{(i_0,i_1)}
    \ar{r}[above]{(f,g)}
    &
    B
    \\
    C
    \ar{ur}[below right]{H}
    &
  \end{tikzcd}.
  \]
  Then the map $i_0$ is a trivial cofibration (\cref{17:lhom-equivalence}). As $p$ is a fibration, the lifting problem
  \[
  \begin{tikzcd}
  A
  \ar{r}[above]{sf}
  \ar{d}[left]{i_0}
  &
  P
  \ar{d}[right]{p}
  \\
  C
  \ar[dashed]{ur}
  \ar{r}[below]{f\sigma,H}
  &
  B\times B
  \end{tikzcd}
  \]
  has a solution $h\mc C\to P$. Then the desired right homotopy is given by $H'\defined h\circ i$.
\end{proof}
\begin{cor}
  Suppose that $f,g\mc A\to B$ are morphisms, with $A$ cofibrant and $B$ fibrant. Then the following are equivalent:
  \begin{enumerate}
    \item The morphisms $f$ and $g$ are left-homotopic.
    \item For every path object $P$ of $B$, there exists a right homotopy $H'\mc A\to P$.
    \item The morphisms $f$ and $g$ are right-homotopic.
    \item For every cylinder object $C$ of $A$, there exists a left-homotopy $H\mc C\to B$ between $f$ and $g$.
  \end{enumerate}
\end{cor}
\begin{proof}
  ...
\end{proof}
\begin{mrem}
  In particular, if $A$ and $B$ are both fibrant and cofibrant, the notions of left-homotopic and right-homotopic coincide.
\end{mrem}
\begin{theorem}[Whitehead]
  Let $\ccat$ be a model category. Let $f\mc X\to Y$ be a weak equivalence and suppose that $X$ and $Y$ are both fibrant and cofibrant objects. Then $f$ is a homotopy equivalence, i.e. there exists a morphism $g\mc Y\to X$ such that $fg\sim \id_Y$ and $gf\sim \id_X$.
\end{theorem}
\begin{proof}
\leavevmode
\begin{enumerate}
\item
  We first assume that $f$ is a trivial fibration. Then the lifting problem
  \[
  \begin{tikzcd}
  \emptyset
  \ar{r}
  \ar{d}
  &
  X
  \ar{d}[right]{f}
  \\
  Y
  \ar[equal]{r}
  \ar[dashed]{ur}
  &
  Y
  \end{tikzcd}
  \]
  has a solution $g\mc Y\to X$. Thus $fg=\id_Y$, so $fg$ is in particular homotopic to $\id_Y$. Further, let $C$ be a cylinder object for $X$. Then the lifiting problem
  \[
  \begin{tikzcd}
    X\amalg X
    \ar{r}
    \ar{d}[left]{i}
    &
    X
    \ar{d}[right]{f}
    \\
    C
    \ar[dashed]{ur}
    \ar{r}[below]{f\sigma}
    &
    Y
  \end{tikzcd}
  \]
  has a solution $H\mc C\to X$, since $i$ is a cofibration and $f$ a trivial cofibration. This $H$ then defines a homotopy between $gf$ and $\id_X$. The dual argument shows the claim in the case thaf $f$ is a trivial cofibration.
\item Consider now the case that $f$ is just a weak equivalence. We can then factor $f$:
\[
\begin{tikzcd}
  \emptyset
  \ar{r}
  &
  X
  \ar{r}[below]{b}
  \ar[bend left = 25]{rr}[above]{f}
  &
  Z
  \ar{r}[below]{a}
  &
  Y
  \ar{r}
  &
  \ast
\end{tikzcd},
\]
such that the maps $\emptyset\to X$ and $X \xrightarrow{b}Z$ are cofibrations, the map $Y\to \ast$ is a fibration and $Z\xrightarrow{a}Y$ is a trivial fibrations. By the two-out-of-three property for $f$, the map $b$ is actually a trivial cofibration. Furthermore, $Z$ is cofibrant and fibrant, since we the unique morphisms to respectivley from $Z$ are given by compositions of cofibrations respectivley fibrations:
\[
\begin{tikzcd}
  \emptyset
  \ar{r}[below]{\text{cof.}}
  \ar[bend left = 25,dashed]{rr}[above]{\exists !}
  &
  X
  \ar{r}[below]{\text{cof.}}
  &
  Z
\end{tikzcd}
\text{ respectivley }
\begin{tikzcd}
  Z
  \ar[bend left = 25, dashed]{rr}[above]{\exists !}
  \ar{r}[below]{\text{fib.}}
  &
  Y
  \ar{r}[below]{\text{fib.}}
  &
  \ast
\end{tikzcd}
\]
By i), we conclude that both $a$ and $b$ are homotopy equivalences, with homotopy inverses $a'$ and $b'$ respectivley. But then $f$ is also a homotopy equivalence with homotopy inverse $b'a'$, since
\begin{align*}
  abb'a'\sim aa'\sim \id_Y\\
  \intertext{and}
  b'a'ab\sim b'b\sim \id_X.
\end{align*}
\end{enumerate}
\end{proof}

\begin{defn}
  Let $\ccat$ be a model category. We define the \emph{homotopy category} $\ho(\ccat)$ as follows:
  \begin{itemize}
    \item The objects of $\ho(\ccat)$ are those objects of $\ccat$ which are both fibrant and cofibrant.
    \item Given objects $X,Y$ in $\ho(\ccat)$, the set of morphisms in $\hoh(\ccat)$ is given by $\ccat(X,Y)$ modulo homotopy equivalence.
    \item Composition is defined by composing representatives of homotopy classes (by the above, this is well-defined).
  \end{itemize}
\end{defn}
\begin{numtext}
  Our next goal will be to show that $\ho(\ccat)$ provides an explicit model structure for the localization $\ccat[\weake^{-1}]$. \par
  Let $\ccat$ be a model category. For every object $X$ of $\ccat$, we choose a factorization
  \[
  \begin{tikzcd}
    \emptyset
    \ar{r}
    \ar[bend left = 20]{rr}
    &
    \repq X
    \ar{r}[below]{p_X}
    &
    X
  \end{tikzcd}
  \]
  where $\repq X$ is a cofibrant object and $\repq X\xrightarrow{p_X}X$ is a trivial fibration.
  Here, we assume that if $X$ is cofibrant that we choose $p_X=\id_X$ and $\repq X = X$.
  The object $\repq X\to X$, equipped with the trivial fibration $\repq X\xrightarrow{p_X}X$, is called a \emph{cofibrant replacement of $X$}.
  \par
  Further, we choose for every $X\in \ccat$, a factorization
  \[
  \begin{tikzcd}
    \repq X
    \ar[bend left = 20]{rr}
    \ar{r}[below]{i_X}
    &
    \repr \repq X
    \ar{r}
    &
    \ast
  \end{tikzcd},
  \]
  where $\repr \repq X$ is a fibrant object and $i_X$ a trivial cofibration. Again, we assume that if $\repq X$ is fibrant that $i_X =\id_X$.\par
  %actually, the lecture already ends heren
  In any case, we arrive at the following pair of weak equivalences:
  \[
  \begin{tikzcd}
    X
    &
    \repq X
    \ar{r}[above]{i_X}
    \ar{l}[above]{p_X}
    &
    \repr \repq X
  \end{tikzcd}
  \]
  We will now start to construct a functor:
  \[
  \pi \mc \ccat\to\ho(\ccat),~X\mapsto \repr\repq X
  \]
  Given a morphism $f\mc X\to Y$, consider the lifting problem:
  \begin{equation}\label{18:lift1}\tag{$\ast$}
  \begin{tikzcd}
    \emptyset
    \ar{r}
    \ar{d}
    &
    \repq X
    \ar{d}[right]{p_Y}
    \\
    \repq X
    \ar{r}[below]{f\circ p_X}
    \ar[dashed]{ur}
    &
    Y
  \end{tikzcd}
  \end{equation}
  Since $\repq X$ is cofibrant and $p_Y$ a trivial fibration, we ge a solution $\repq f\mc \repq X\to \repq Y$.
  \par
  Consinder now the lifitng problem
  \begin{equation}\label{18:lifting2}\tag{$\ast\ast$}
    \begin{tikzcd}
      \repq X
      \ar{d}[left]{i_X}
      \ar{r}[above]{i_Y\repq f}
      &
      \repr \repq Y
      \ar{d}
      \\
      \repr \repq X
      \ar[dashed]{ur}
      \ar{r}
      &
      \ast
    \end{tikzcd},
  \end{equation}
  whose solution defines a morphism $\repr\repq X\xrightarrow{\repr\repq f}\repr\repq Y$.
\end{numtext}
\lec
