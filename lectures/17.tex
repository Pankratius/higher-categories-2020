\subsection{The homotopy category of a model category}
\begin{defn}
Let $\ccat$ be a model category. A \emph{cylinder object for an object $A\in \ccat$} is an object $C\in\ccat$ equipped with a factorization
\[
\begin{tikzcd}
  A\amalg A
  \ar{d}[left]{(i_0,i_1)}
  \ar{r}[above]{(\id,\id)}
  &
  A
  \\
  C
  \ar{ur}[below right]{\sigma}
  &
\end{tikzcd}
\]
such that $i$ is a cofibration and $\sigma$ is a weak equivalence.
\end{defn}
\begin{example}
  For the Kan model structure on $\sset$, given a simplicial set $A$, then $\Delta^1\times A$ is a cylinder object for $A$, with:
  \[
  i_0\mc\lset 0\rset\times A\hookrightarrow\Delta^1\times A\text{ and }
  i_1\mc\lset 1\rset\times A\hookrightarrow\Delta^1\times A
  \]
\end{example}
\begin{defn}
  A \emph{left homotopy} between morphisms $f,g\mc A\to B$ in a model category $\ccat$ consists of a cylinder object $C$ for A  and a map $H\mc C\to B$ such that the diagram
\[
\begin{tikzcd}
  A\amalg A
  \ar{d}[left]{i}
  \ar{r}[above]{(f,g)}
  &
  B\\
  C
  \ar{ur}[below]{H}
  &
\end{tikzcd}
\]
and
\[
\begin{tikzcd}
  A
  \ar{d}[left]{i_0}
  \ar{rd}[above right]{f}
  &
  \\
  C
  \ar{r}[above]{H}
  &
  B
  \\
  A
  \ar{u}[left]{i_1}
  \ar{ur}[below right]{g}
  &
\end{tikzcd}
\]
commute. We say that $f$ and $g$ \emph{are left homotopic} if there exists a left homotopy between $f$ and $g$, and we write $f\lhom g$.
\end{defn}
\begin{defn}
  An object $A$ of a model category $\ccat$ is called:
  \begin{enumerate}
    \item \emph{cofibrant} if the morphism from the initial object in $\ccat$ is a cofibration.
    \item \emph{fibrant} if the morphism to the final object of $\ccat$ is a fibration.
  \end{enumerate}
\end{defn}
\begin{prop}\label{17:lhom-equivalence}
  \leavevmode
  \begin{enumerate}
    \item Let $A$ be a cofibrant object and $C$ any cylinder for $A$. Then the maps $i_0$ and $i_1$ in the map
    \[
    A\amalg A\xrightarrow{(i_0,i_1)} A
    \]
    are trivial cofibrations.
    \item Let $A$ be a cofibrant object and $B$ any object. Then the relation $\lhom$ defines an equivalence relation on the set of morphisms from $A$ to $B$.
    \end{enumerate}
\end{prop}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Note that we have a cocartesian square of the form
    \[
    \begin{tikzcd}
      \emptyset
      \ar{d}
      \ar{r}
      \ar[phantom]{rd}{\ulcorner}
      &
      A
      \ar{d}[right]{j_0}
      \\
      A
      \ar{r}[below]{j_1}
      &
      A\amalg A
    \end{tikzcd}
    \]
    and hence, since the set of cofibrations is saturated, $j_0, j_1$ are cofibrations. Now the $i_0,i_1$ factor as $i_0=i\circ j_0$ and $i_1=i\circ j_1$ and are thus cofibrations too.\par
    We also have $\sigma\circ i_0=\id_A$ and $\sigma\circ i_1=\id_A$ and by the two-out-of-three property, it follows that both of them are weak equivalences.
    \item We show that the relation $\lhom$ is reflexive, symmetric and transitive:
    For reflexivity, let $f\mc A\to B$ be any morphism. Choose any cylinder object $C$ for $A$. Then the diagram
    \[
    \begin{tikzcd}
    A
    \ar{d}[left]{i_0}
    \ar{rd}[above right]{\id}
    \ar[bend left = 20]{rrd}[above right]{f}
    &
    &
    \\
    C
    \ar{r}[above]{\sigma}
    &
    A
    \ar{r}[above]{f}
    &
    B
    \\
    A
    \ar{u}[left]{i_1}
    \ar{ru}[below right]{\id}
    \ar[bend right = 20]{rru}[below right]{f}
    \end{tikzcd},
    \]
    commutes;
    the homotopy is thus given by $f\circ \sigma$.\par
    For $f,g\mc A\to B$ with $f\lhom g$ we want $g\lhom f$. For that we note that we can simply flip the diagram defining the left homotopy
    \[
    \begin{tikzcd}
      A
      \ar{d}[left]{i_0}
      \ar{rd}[above right]{f}
      &
      \\
      C
      \ar{r}[above]{H}
      &
      B
      \\
      A
      \ar{u}[left]{i_1}
      \ar{ur}[below right]{g}
      &
    \end{tikzcd}
    \leadsto
    \begin{tikzcd}
      A
      \ar{d}[left]{i_1}
      \ar{rd}[above right]{g}
      &
      \\
      C
      \ar{r}[above]{H}
      &
      B
      \\
      A
      \ar{u}[left]{i_0}
      \ar{ur}[below right]{f}
      &
    \end{tikzcd}
    \]
    Then this defines indeed again a left-homotopy, since the new morphism in the definition of the cylinder $(i_1,i_0)$ factors as:
    \[
    A\amalg A \xrightarrow{\simeq}A\amalg A \xrightarrow{(i_0,i_1)}C
    \]\par
    For transitivity, assume $H$ is a homotopy between $f$ and $g$ and $H'$ is a homotopy between $g$ and $h$, so we have commutative diagrams of the form
    \[
    \begin{tikzcd}
    A\amalg A
    \ar{r}[above]{(f,g)}
    \ar{d}[left]{(i_0,i_1)}
    &
    B
    \\
    C
    \ar{ur}[below right]{H}
    &
    \end{tikzcd}
    ,~
    \begin{tikzcd}
    A\amalg A
    \ar{r}[above]{(g,h)}
    \ar{d}[left]{(i'_0,i'_1)}
    &
    B
    \\
    C
    \ar{ur}[below right]{H}
    &
    \end{tikzcd}
    \]
    Then we consider the pushout $C\coprod_A C'$ along $i_1$ and $i'_0$, which gives rise to the following commutative diagram
    \[
    \begin{tikzcd}[column sep = large]
      A
      \ar[bend left = 20]{rd}[above]{f}
      \ar{d}[left]{i_0}
      &
      \\
      \displaystyle C\coprod_A C'
      \ar{r}[above]{(H,H')}
      &
      B
      \\
      A
      \ar{u}[left]{i_1'}
      \ar[bend right = 20]{ur}[below right]{h}
      &
    \end{tikzcd}
      \]
      If we can show that
      \[
      \begin{tikzcd}
        A\amalg A
        \ar{d}[left]{(i_0,i_1')}
        \ar{r}[above]{(\id,\id)}
        &
        A
        \\
        \displaystyle C\coprod_A C'
        \ar{ur}[below right]{(\sigma,\sigma')}
        &
      \end{tikzcd}
      \]
      exhibits $A\amalg A \xrightarrow{(i_0,i_1')}C\coprod_A C'$ as a cylinder, then we are done:\par
      By construction, we have the cocartesian square:
      \[
      \begin{tikzcd}
        A
        \ar{r}[above]{i_1}
        \ar{d}[left]{i_0'}
        &
        C
        \ar{d}[right]{j}
        \\
        C'
        \ar{r}
        &
        \displaystyle C \coprod_A C'
      \end{tikzcd}
      \]
      Note that $j$ is a trivial cofibration, since $i_0'$ is a trivial cofibration, by saturatedness. Since we also have the factorization of $\sigma$ given by
      \[
      C\xrightarrow{j}C\coprod_A C'\xrightarrow{(\sigma,\sigma')}A,
      \]
    the two-out-of-three implies that $(\sigma,\sigma')$ is a weak equivalence. By considering the pushout
    \[
    \begin{tikzcd}
    (A\amalg A)\amalg(A\amalg A)
    \ar{d}
    \ar{r}[above]{i\amalg i'}
    \ar[phantom]{rd}{\ulcorner}
    &
    C\amalg C'
    \ar{d}
    \\
    A\amalg A\amalg A
    \ar{r}[below]{k}
    &
    C\amalg_A C'
    \end{tikzcd}
    \]
    showing that $k$ is a cofibration, since $i\amalg i'$ is a cofibration, again by saturatedness. Finally, the pushout
    \[
    \begin{tikzcd}
      \emptyset
      \ar{r}
      \ar{d}
      &
      A
      \ar{d}
      \\
      A \amalg A
      \ar{r}
      &
      A\amalg A \amalg
    \end{tikzcd}
    \]
    so that $l$ is a cofibration, since $A$ is cofibrant. Thus $(i_0,i_1')$ is a cofibration, since $(i_0,i_1') = k\circ l$.
  \end{enumerate}
\end{proof}
\lec
