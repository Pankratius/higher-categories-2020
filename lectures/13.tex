\subsection{Anodyne Morphisms}
\begin{defn}
  A morphism $f\mc X\to Y$ of simplicial sets is called \emph{anodyne} if it has the left lifting property with respect to all Kan fibrations.
\end{defn}
\begin{cor}
  The set of anodyne morphisms is the saturated hull of the set
  \[
  M\defined
  \lset \lambda_i^n\hookrightarrow\Delta^n\ssp n>0,0\leq i\leq n\rset
  \]
  of horn inclusions.
\end{cor}
\begin{proof}
  We have that $M\rlp$ is given by the set of Kan fibrations, and $\llp(M\rlp)$ is given by the set of anodyne morphisms. Now since the $\Lambda_i^n$ are compact, the small object argument directly gives
  \[
  \overline{M} = \llp(M\rlp).
  \]
\end{proof}
\begin{construction}
  Let $f\mc A\to A'$ and $g\mc B\to B'$ be morphisms of simplicial sets. Then the commutative square
  \[
  \begin{tikzcd}
    A\times B
    \ar{r}[above]{f\times \id}
    \ar{d}[left]{\id\times g}
    &
    A'\times B
    \ar{d}[right]{\id\times g}
    \\
    A\times B'
    \ar{r}[below]{f\times \id}
    &
    A'\times B'
  \end{tikzcd}
  \]
  induces a map
  \[
  f\wedge g\mc \left(A'\times B\right)\coprod_{A\times B}\left(A\times B'\right)\to A'\times B'
  \]
  from the push-out, which is called the \emph{smash product} of $f$ and $g$.
\end{construction}

\begin{example}
  Let $f\mc \Delta^0\to X$ and $g\mc \Delta^0\to Y$ be pointed simplicial sets, then we get an injection
  \[
  f\wedge g\mc \left(X\times \Delta^0\right)\coprod_{\Delta^0\times\Delta^0}\left(\Delta^0\times Y\right)\hookrightarrow X\times Y
  \]

\end{example}

\begin{prop}
  Let $f,g,h$ be morphisms of simplicial sets. Then:
  \begin{enumerate}
    \item If $f$ and $g$ are monomorphisms, then so is $f\wedge g$.
    \item We have a natural isomorphism:
    \[
    f\wedge(g\wedge h) \cong (f\wedge g)\wedge h
    \]
  \end{enumerate}
\end{prop}
The following is \cite[Prop. 2.1.2.6]{highertopoi} or  \cite[Chapter I, Prop. 4.2]{goersjardine}.
\begin{lem}
  Consider the following three sets of morphisms:
  \begin{align*}
    M_1&\defined
    \lset \Lambda_i^n\hookrightarrow \Delta^n\ssp n>0,0\leq i\leq n\rset \\
    M_2&\defined
    \lset
    \begin{array}{c}
    \left(\lset v\rset\hookrightarrow \Delta^1\right)\wedge
    \left(\partial \Delta^n\hookrightarrow \Delta^n\right):\\
     \left(\lset v\rset \times \Delta^n\right) \coprod_{\lset v\rset \times \partial \Delta^n}\left(\Delta^1\times \partial\Delta^n\right)\hookrightarrow\Delta^1\times\Delta^n
     \end{array}
     \ssp n\geq 0,v\in \lset 0,1\rset \rset \\
    M_3 &\defined
    \lset
    \begin{array}{c}
    \left(\lset v\rset\hookrightarrow\Delta^1\right)\wedge\left(K\hookrightarrow S\right):\\
    \left(\lset v\rset \times S\right) \coprod_{\lset v\rset \times K}\left(\Delta^1\times K\right)\hookrightarrow\Delta^1\times S
    \end{array}
    \ssp K\hookrightarrow S\text{ mono, }v\in \lset 0,1\rset
    \rset
  \end{align*}
  Then all three have the same saturation, given by the set of anodyne morphisms.
\end{lem}

\begin{proof}
  We will do this in several steps:
  \begin{enumerate}
    \item $M_2\sse \overline{M_1}$: We are going to show that every morphism in $M_2$ can be obtained by a finite composition of pushouts along inner horn inclusions (which we visuallize as succesivley gluing-in horns). Consider first the case $n=1$, $v=0$, then we are faced with the inclusion of the square-with-one-end-removed:
    \begin{figure}[h!]
    \begin{tikzpicture}
    \begin{scope}[decoration={
      markings,
      mark = at position 0.5 with  {\arrow{>}}}
      ]
      \draw[postaction = {decorate}] (0,0) node [anchor = north] {\tiny$(0,0)$}--(0,1) node [anchor = south]{\tiny$(0,1)$};
      \draw[postaction = {decorate}] (0,1)--(1,1) node [anchor = south]{\tiny $(1,1)$};
      \draw[postaction = {decorate}] (0,0)-- (1,0)node [anchor = north] {\tiny$(1,0)$};

      \draw[postaction = {decorate}](2.5,0) node [anchor = north] {\tiny$(0,0)$} -- (3.5,0) node [anchor = north]{\tiny$(1,0)$};
      \draw[postaction = {decorate}](2.5,0) -- (2.5,1) node [anchor = south]{\tiny$(0,1)$};
      \draw[postaction = {decorate}](2.5,0) -- (3.5,1) node[anchor = south]{\tiny$(1,1)$};
      \draw[postaction = {decorate}](2.5,1) -- (3.5,1);
      \draw[postaction = {decorate}](3.5,0) -- (3.5,1);
    \end{scope}
    \draw[right hook ->](1.25,.5) -- (2.25,.5);
    \path[fill = gray, fill opacity = .3] (2.5,0) -- (3.5,1) -- (2.5,1) -- (2.5,0);
    \path[fill = gray, fill opacity = .3] (2.5,0) -- (3.5,0) -- (3.5,1) -- (2.5,0);

    \end{tikzpicture}
    \end{figure}
    \par
    We first fill (indicated by the gray filling) the horn on the upper-left side, which then gives rise to a lower-right horn, which we can fill again:
    \begin{figure}[h!]
    \begin{tikzpicture}
    \begin{scope}[decoration={
      markings,
      mark = at position 0.5 with  {\arrow{>}}}
      ]
      \draw[postaction = {decorate}] (0,0)--(0,1);
      \draw[postaction = {decorate}] (0,1)--(1,1);
      \draw[postaction = {decorate}] (0,0)--(1,0);

      \draw[postaction = {decorate}](2.5,0) -- (3.5,0);
      \draw[postaction = {decorate}](2.5,0) -- (2.5,1);
      \draw[postaction = {decorate}](2.5,0) -- (3.5,1);
      \draw[postaction = {decorate}](2.5,1) -- (3.5,1);

      \draw[postaction = {decorate}](5,0)--(6,0);
      \draw[postaction = {decorate}](5,0)--(5,1);
      \draw[postaction = {decorate}](5,0)--(6,1);
      \draw[postaction = {decorate}](5,1)--(6,1);
      \draw[postaction = {decorate}](6,0)--(6,1);

    \end{scope}





      \node (onehorn) at (0.5,-1.5) {$\Lambda_1^2$};
      \node (firstsimplex) at (2.75,-1.5) {$\Delta^2$};
      \node (zerohorn) at (3.25,-1.5){$\Lambda_0^2$};
      \node (secondsimplex) at (5.5,-1.5){$\Delta^2$};

      \draw[right hook ->] (1.25,.5) -- (2.25,.5);
      \draw[right hook ->] (3.75,.5) -- (4.75,.5);

      \draw[->] (onehorn) -- (0.5,-.2);
      \draw[right hook ->] (onehorn) -- (firstsimplex);
      \draw[->] (zerohorn) -- (3.25,-.2);
      \draw[->] (firstsimplex) -- (2.75,-.2);
      \draw[->] (secondsimplex) -- (5.5,-.2);
      \draw[right hook ->] (zerohorn) -- (secondsimplex);

      \path[fill = gray, fill opacity = .3] (2.5,0) -- (3.5,1) -- (2.5,1) -- (2.5,0);
      \path[fill = gray, fill opacity = .3] (5,0) -- (6,0) -- (6,1) -- (5,0);

      \node at (2.25,-.25) {$\llcorner$};
      \node at (4.75,-.25) {$\llcorner$};
    \end{tikzpicture}

    \end{figure}
    \par
    Note that the order in which we filled the horns was important, because the lower-right horn did not even exist in the original diagram.\par
    This will be our strategy for a general $n$: Glue in a $n+1$-simplices along an appropriate horn inclusion, such that in each step the arrising new horn can be filled in the next step. To formalize this, we first note that an $(n+1)$-simplex of $\Delta^1\times\Delta^n$ corresponds to a map $\left[n+1\right]\to \left[1\right]\times\left[n\right]$, and the non-degenerate $(n+1)$-simplices of $\Delta^1\times \Delta^n$ are given as follows: If we visualize the order on $\left[1\right]\times\left[n\right]$ as
    \[
    \begin{tikzcd}[column sep = small, row sep = small]
    (0,n)
    \ar{r}
    &
    (1,n)
    \\
    \vdots
    \ar{u}
    &
    \vdots
    \ar{u}
    \\
    (0,2)
    \ar{r}
    \ar{u}
    &
    (1,2)
    \ar{u}
    \\
    (0,1)
    \ar{r}
    \ar{u}
    &
    (1,1)
    \ar{u}
    \\
    (0,0)
    \ar{u}
    \ar{r}
    &
    (1,0)
     \ar{u}
  \end{tikzcd}
    \]
    then we define the map $h_j$ to be the chain
    \[
    (0,0)\to (0,1) \to \ldots \to (0,j)\to (1,j) \to \ldots\to(1,n)
    \]
    which we visualize as
    \[
    \begin{tikzcd}[column sep = small]
      n+1
      \\
      \vdots
      \ar[rightsquigarrow]{u}
      \\
      j+1
      \ar[rightsquigarrow]{u}
      \\
      \ar[rightsquigarrow]{u}
      j
      \\
      \vdots
      \ar[rightsquigarrow]{u}
      \\
      0
      \ar[rightsquigarrow]{u}
    \end{tikzcd}
  \mapsto
  \begin{tikzcd}[column sep = small]
  (0,n)
  \ar{r}
  &
  (1,n)
  \\
  \vdots
  \ar{u}
  &
  \vdots
  \ar[rightsquigarrow]{u}
  \\
  (0,j+1)
  \ar{r}
  \ar{u}
  &
  (1,j+1)
  \ar[rightsquigarrow]{u}
  \\
  (0,j)
  \ar[rightsquigarrow]{r}
  \ar{u}
  &
  (1,j)
  \ar[rightsquigarrow]{u}
  \\
  \vdots
  \ar[rightsquigarrow]{u}
  &
  \vdots
  \ar{u}
  \\
  (0,0)
  \ar[rightsquigarrow]{u}
  \ar{r}
  &
  (1,0)
   \ar{u}
\end{tikzcd}
  \]
  Furthermore, the $h_j$ satisfy the following coherence conditions:
  \begin{itemize}
    \item The diagrams
    \[
    \begin{tikzcd}
      \Delta^n
      \ar{r}[above]{d^0}
      &
      \Delta^{n+1}
      \ar{d}[right]{h_0}
      \\
      \Delta^n\times \lset 1\rset
      \ar[hookrightarrow]{r}
      \ar{u}[left]{\cong}
      &
      \Delta^n\times\Delta^1
    \end{tikzcd}
    \text{ and }
    \begin{tikzcd}
      \Delta^n
      \ar{r}[above]{d^{n+1}}
      &
      \Delta^{n+1}
      \ar{d}[right]{h_n}
      \\
      \Delta^n\times \lset 0\rset
      \ar[hookrightarrow]{r}
      \ar{u}[left]{\cong}
      &
      \Delta^n\times\Delta^1
    \end{tikzcd}
    \]
    commute, because the zero/$n$ where the ``switch'' happens is cut off by the degeneracy map.
    \item The diagrams
    \[
    \begin{tikzcd}
      \Delta^n
      \ar{r}[above]{d^{j+1}}
      \ar{d}[left]{d^{j+1}}
      &
      \Delta^{n+1}
      \ar{d}[right]{h_{j+1}}
      \\
      \Delta^{n+1}
      \ar{r}[below]{h_j}
      &
      \Delta^n\times \Delta^1
    \end{tikzcd}
    \text{ and }
    \begin{cases}
    \begin{tikzcd}
      \Delta^n
      \ar{r}[above]{d^i}
      \ar{d}[left]{h_{j-1}}
      &
      \Delta^n
      \ar{d}[right]{h_j}
      \\
      \Delta^{n-1}\times\Delta^1
      \ar{r}[below]{d^i\times 1}
      &
      \Delta^n\times \Delta^1
    \end{tikzcd}
    &
    \text{if }i<j\\
    \begin{tikzcd}
      \Delta^n
      \ar{r}[above]{d^i}
      \ar{d}[left]{h_{j}}
      &
      \Delta^{n+1}
      \ar{d}[right]{h_j}
      \\
      \Delta^{n-1}\times\Delta^1
      \ar{r}[below]{d^{i-1}\times 1}
      &
      \Delta^n\times \Delta^1
    \end{tikzcd}
    &\text{if }i>j+1
    \end{cases}
    \]
    commute -- this can again be checked on the level of ordered sets.
    \item For the $(n+1)$-simplex corresponding to $h_j$ in $\Delta^n\times\Delta^1$, we have $d_{j+1}h_j\notin\partial\Delta^n\times\Delta^1$, since it projects to the $n$-simplex of $\Delta^n$ corresponding to the identity of $\left[n\right]$ under the projection $\Delta^n\times\Delta^1\twoheadrightarrow\Delta^n$.
    \item The $n$-simplex $d_{j+1}h_j$ is not a face of $h_i$ for $j\geq i+1$, since it contains the vertex $(0,j)$.
  \end{itemize}
  Having all that formalism established, we return to the original aim of obtaining $\Delta^n\times \Delta^1$ by glueing in the missing (non-degenerate) $(n+1)$-simplices. Motivated by the case $n=1$, we set $A^{n+1}\defined \Delta^1\times \Delta^n$ and
  \[
  \left(\lset 0\rset \times \Delta^n\right) \coprod_{\lset 0\rset \times \partial \Delta^n}\left(\Delta^1\times\partial\Delta^n\right)
  \]
  and establish a factorisation of the following form:
  \[
  A^0\xhookrightarrow{\substack{\tiny\text{attach}\\h_n}}A^1\xhookrightarrow{\substack{\tiny\text{attach}\\h_{n-1}}}A^2\hookrightarrow\ldots\xhookrightarrow{\substack{\tiny\text{attach}\\h_{0}}}A^{n+1}
  \]
  such that each attachment is a pushout along a horn inclusion. Concretely, we define $A^{i+1}$ to be the pushout
  \[
  \begin{tikzcd}
    \Lambda^{n+1}_{n-i}
    \ar{r}
    \ar[hookrightarrow]{d}
    \ar[phantom]{dr}{\ulcorner}
    &
    A^i
    \ar{d}
    \\
    \Delta^n
    \ar{r}[below]{h_{n-i}}
    &
    A^{i+1}
  \end{tikzcd}
  \]
  where the map $\horn{n-i}{i+1}\to A^i$ is given by the tuple:
  \[
  \left(d_0h_{n-i},\ldots, \widehat{d_{n-i}h_{n-i}},\ldots, d_{n+1}h_{n-i+1}\right)
  \]
  The case $v=1$ follows by passing to the opposite simplicial set.
  \item To show $M_1\sse\overline{M_3}$, we will exhibit every horn inclusion $\horn{i}{n}\hookrightarrow\Lambda^n$ as a retract of a morphism in $M_2\sse M_3$:\par
  For $k<n$, consider maps
  \[
  \npcat\xrightarrow{\iota}\left[1\right]\times\npcat\xrightarrow{p}\npcat
  \]
  where $\iota(j)=(1,j)$ and
  \[
  p((i,j)) =
  \begin{cases}
      j&\text{if }i =1\\
      j&\text{if }i=0\text{ and }j\leq k\\
      k&\text{else}
  \end{cases}
  \]
  which we visualize as
  \[
  \begin{tikzcd}
  (0,k)
  \ar{r}
  &
  (1,n)
  \\
  \vdots
  \ar{u}
  &
  \vdots
  \ar{u}
  \\
  (0,k)
  \ar{r}
  \ar{u}
  &
  (1,k+1)
  \ar{u}
  \\
  (0,k)
  \ar{r}
  \ar{u}
  &
  (1,k)
  \ar{u}
  \\
  \vdots
  \ar{u}
  &
  \vdots
  \ar{u}
  \\
  (0,0)
  \ar{u}
  \ar{r}
  &
  (1,0)
   \ar{u}
\end{tikzcd}
\]
Then we have $p\circ \iota = \id$, and they induce a retract diagram of the form
\[
\begin{tikzcd}[column sep = large]
  \horn{k}{n}
  \ar[hookrightarrow]{d}
  \ar{r}
  &
  \left(\lset 0\rset \times \Delta^n\right)\displaystyle\coprod_{\lset 0\rset \times \horn{k}{n}}\left(\Delta^1\times \horn{n}{k}\right)
  \ar[hookrightarrow]{d}
  \ar{r}
  &
  \horn{k}{n}
  \ar[hookrightarrow]{d}
  \\
  \Delta^n
  \ar[hookrightarrow]{r}[below]{\iota}
  &
  \Delta^1\times\Delta^n
  \ar{r}[below]{p}
  &
  \Delta^n
\end{tikzcd}
\]
Passing to the opposite simplex, we also obtain the case $n=k$.
 \end{enumerate}
\end{proof}
