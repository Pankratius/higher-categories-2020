These are my (probably extremely erroneous and also very provisional) lecture notes for the course [_Introduction to Higher Categories_][1] taught by Prof. Tobias Dyckerhoff at Hamburg University.
A compiled version can be found [here][2].

Please note that at the moment, Prof. Dyckerhoff does not know about the existence of these lecture notes, and I would like to keep it that way (at least until I find some time to proof-read the notes) - so please be responsible (and also cautios - there are still so many mistakes) with what you find here!


[1]: https://www.math.uni-hamburg.de/home/dyckerhoff/inftycat2021/index.html
[2]: https://pankratius.gitlab.io/higher-categories-2020/highercats.pdf
